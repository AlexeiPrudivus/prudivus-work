<?php
/*
Write a procedure SortIndex(A, N, I) that creates an index array I for an array A of N real numbers.
The index array contains order numbers of elements of array A
so that they correspond to array elements in ascending order of their values (the array A remains unchanged).
The index array I is an output parameter. Using this procedure,
create index arrays for three given arrays A, B, C of size NA, NB, NC respectively.
*/
$A = array(1 => 5,0,7,1,0,9,3,0);
$I = array();
$U = 1;

for($i = 1; $i <= count($A); $i++) {
    $I[$i] = $A[$i];
}

/*for($i = 1; $i <= count($A); $i++) {
    $I[$i] = $i;
}*/

for($i = 1; $i <= count($A); $i++) {
    echo $A[$i]." ";
}

echo "\n";
for($i = 1; $i <= count($I); $i++) {
    echo $I[$i]." ";
}

for ($i = 1; $i <= count($A); $i++) {
    for ($j = 1; $j <= count($A); $j++) {
        if ($A[$i] < $A[$j]) {
            $Z = $A[$i];
            $A[$i] = $A[$j];
            $A[$j] = $Z;
        }
    }
}

echo "\n";
for($i = 1; $i <= count($A); $i++) {
    echo $A[$i]." ";
}


for ($i = 1; $i <= count($A); $i++) {
    for ($j = 1; $j <= count($A); $j++) {
        if ($I[$i] == $A[$j]) {
            $I[$i] = $j;
        }
    }
}

echo "\n";
for($i = 1; $i <= count($I); $i++) {
    echo $I[$i]." ";
}
?>