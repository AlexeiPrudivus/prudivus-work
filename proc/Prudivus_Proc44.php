<?php
/*   Описать функцию Arctg1(x, ε) вещественного типа (параметры x, ε —
 вещественные, |x|<1, ε>0), находящую приближенное значение функции arctg(x):
        arctg(x) = x - x3/3 + x5/5 - ... + (-1)n·x2·n+1/(2·n+1) + ... .
  В сумме учитывать все слагаемые, модуль которых больше ε. С помощью Arctg1
    найти приближенное значение arctg(x) для данного x при шести данных ε.
 */

function  Arctg1($x, $e) {

    $Arctg = array();

    $S = 0;
    $stop = 1;
    $f = 1;

    for ($i = 0; $i < $stop; $i++) {

        $Arctg[$i] = round(pow(-1,$i) * pow($x, 2*$i+1) / (2*$i+1), 7); //function

        if (abs($Arctg[$i]) > $e) { //stop condition
            $stop++;
            echo $Arctg[$i]." ";
            $S = $S + $Arctg[$i]; //sum
        }
    }

    echo "\n".$S;
    return $S;
}

    Arctg1(0.7518115, 0.0056663);
?>