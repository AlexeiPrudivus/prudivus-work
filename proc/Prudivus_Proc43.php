<?php
/*   Описать функцию Ln1(x, ε) вещественного типа (параметры x, ε —
  вещественные, |x|<1, ε>0), находящую приближенное значение функции ln(1+x):
          ln(1+x) = x - x2/2 + x3/3 - ... + (-1)n·xn+1/(n+1) + ... .
    В сумме учитывать все слагаемые, модуль которых больше ε. С помощью Ln1
    найти приближенное значение ln(1 + x) для данного x при шести данных ε.
 */

function Ln1($x, $e) {

    $Ln = array();

    $S = 0;
    $stop = 2;
    $f = 1;

    for ($i = 0; $i < $stop; $i++) {

        $Ln[$i] = pow(-1,$i) * pow($x, $i+1) / ($i+1); //function

        if (abs($Ln[$i]) > $e) { //stop condition
            $stop++;
            echo $Ln[$i]." ";
            $S = $S + round($Ln[$i], 7); //sum
        }
    }

    echo "\n".$S;
    return $S;
}

    Ln1(-0.5105941, 0.0004753);
?>