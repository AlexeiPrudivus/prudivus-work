<?php
/*  Описать функцию IsPowerN(K, N) логического типа, возвращающую true, если
   целый параметр K (>0) является степенью числа N (>1), и false в противном
 случае. Дано число N (>1) и набор из 10 целых положительных чисел. С помощью
      функции IsPowerN найти количество степеней числа N в данном наборе.
 */
function IsPowerN($K, $N)
{
    $b = log($K, $N) - floor(log($K, $N)); //находим дробную часть
    //echo round($b,2)." ";

    if (round($b, 2) == 0.00) {
        return true;
    } else {
        return false;
    }
}
/*
    $M = array();

    $N = rand(1,10);
    echo "N=".$N."\n";

    for ($i = 0; $i < 10; $i++) {
        $M[$i] = rand (1,1000);
        echo $M[$i]." ";
    }

    echo "\n";
    $s = 0;

    for ($i = 0; $i < count($M); $i++) {
        if (IsPowerN($M[$i], $N) == true) {
            $s++;
        }
    }
    echo $s;*/
?>