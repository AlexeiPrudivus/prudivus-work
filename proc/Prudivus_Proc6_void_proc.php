<?php
/* Описать процедуру DigitCountSum(K, C, S), находящую количество C
            цифр целого положительного числа K, а также их сумму S
            (K — входной, C и S — выходные параметры целого типа).
            С помощью этой процедуры найти количество и сумму цифр
                    для каждого из пяти данных целых чисел.
 */
function DigitCountSum($K, &$C, &$S)
{
    $C = 0;
    $S = 0;

    while ($K != 0) {
        $S += $K % 10;
        $C ++;
        $K = intval($K / 10);
    }
}
/*
    $K = rand(0, 10000);// echo $K." ".intval($K / 10);
    echo "number=".$K."\n";

    DigitCountSum($K, $C, $S);
    echo 'number of digits=' . $C . "\nsum of digits=" . $S."\n";
*/
?>