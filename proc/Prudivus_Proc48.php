<?php
/*  Наименьшее общее кратное (least common multiple) двух целых положительных чисел A и B равно A·(B/НОД(A, B)),
    где НОД(A, B) — наибольший общий делитель A и B. Используя функцию GCD2 (см. Proc46), описать функцию LCM2(A, B) целого типа,
    находящую наименьшее общее кратное чисел A и B.
    С помощью LCM2 найти наименьшие общие кратные пар (A, B), (A, C), (A, D), если даны числа A, B, C, D.
*/
require_once("Prudivus_Proc46.php");

function  LCM2($A, $B) {
    return $A*($B/GCD2($A, $B));
}
/*
    $a = rand(1, 100); echo $a." ";
    $b = rand(1, 100); echo $b." ";
    $c = rand(1, 100); echo $c." ";
    $d = rand(1, 100); echo $d."\n";

    echo $a." ".$b." LCM="; echo LCM2($a, $b)."\n";
    echo $a." ".$c." LCM="; echo LCM2($a, $c)."\n";
    echo $a." ".$d." LCM="; echo LCM2($a, $d)."\n";*/
?>