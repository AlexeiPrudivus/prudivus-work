<?php

function PowerA234($A, &$B, &$C, &$D)
{
	$B = $A*$A;
    $C = $B*$A;
    $D = $C*$A;

    return array($B, $C, $D);
}

for($i=0; $i<5; $i++)
{
	$A = rand() % 10;
	echo ' number = '.$A;

    PowerA234($A, $B, $C, $D);
	echo ' A^2= '.$B;
    echo ' A^3= '.$C;
    echo ' A^4= '.$D."\n";
}
?>