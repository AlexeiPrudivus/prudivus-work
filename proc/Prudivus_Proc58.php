<?php
/*   Используя функции Leng и Perim из заданий Proc56 и Proc57, описать функцию Area(xA, yA, xB, yB, xC, yC) вещественного типа,
    находящую площадь треугольника ABC по формуле SABC = (p·(p−|AB|)·(p−|AC|)·(p−|BC|))1/2,
    где p — полупериметр. С помощью этой функции найти площади треугольников ABC, ABD, ACD, если даны координаты точек A, B, C, D.
*/
require_once("Prudivus_Proc57.php");

function Area($xA, $yA, $xB, $yB, $xC, $yC) {
    $p = Perim($xA, $yA, $xB, $yB, $xC, $yC)/2;
    return round(pow(($p * ($p - Leng($xA, $yA, $xB, $yB)) * ($p - Leng($xA, $yA, $xC, $yC)) * ($p - Leng($xB, $yB, $xC, $yC))), 1/2), 2);
}
/*
    $xA= rand(-10,10);
    $yA= rand(-10,10);
    $xB= rand(-10,10);
    $yB= rand(-10,10);
    $xC= rand(-10,10);
    $yC= rand(-10,10);

    echo "xA=".$xA." yA=".$yA."\n";
    echo "xB=".$xB." yB=".$yB."\n";
    echo "xC=".$xC." yC=".$yC."\n";

    echo Area($xA, $yA, $xB, $yB, $xC, $yC);*/
?>