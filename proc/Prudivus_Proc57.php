<?php
/*  Используя функцию Leng из задания Proc56, описать функцию Perim(xA, yA, xB, yB, xC, yC) вещественного типа,
    находящую периметр треугольника ABC по координатам его вершин (xA, yA, xB, yB, xC, yC — вещественные параметры).
    С помощью этой функции найти периметры треугольников ABC, ABD, ACD, если даны координаты точек A, B, C, D.
*/
require_once("Prudivus_Proc56.php");

function Perim($xA, $yA, $xB, $yB, $xC, $yC) {
    return Leng($xA, $yA, $xB, $yB) + Leng($xA, $yA, $xC, $yC) + Leng($xB, $yB, $xC, $yC);
}
/*
    $xA= 1;//rand(-10,10);
    $yA= 0;//rand(-10,10);
    $xB= 0;//rand(-10,10);
    $yB= 1;//rand(-10,10);
    $xC= 1;//rand(-10,10);
    $yC= 1;//rand(-10,10);

    echo "xA=".$xA." yA=".$yA."\n";
    echo "xB=".$xB." yB=".$yB."\n";
    echo "xC=".$xC." yC=".$yC."\n";

    echo Perim($xA, $yA, $xB, $yB, $xC, $yC);*/
?>