<?php
/*   Описать функцию Cos1(x, ε) вещественного типа (параметры x, ε —
     вещественные, ε > 0), находящую приближенное значение функции cos(x):
      cos(x) = 1 - x2/(2!) + x4/(4!) - ... + (-1)n·x2·n/((2·n)!) + ... .
   В сумме учитывать все слагаемые, модуль которых больше ε. С помощью Cos1
    найти приближенное значение косинуса для данного x при шести данных ε.
 */

function Cos1($x, $e) {

    $Cos = array();

    $S = 0;
    $stop = 1;
    $f = 1;

    for ($i = 0; $i < $stop; $i++)
        if ($i == 0) { //factorial
            $f = 1;
        } else {
            $f *= (2*$i);
        }

        $Cos[$i] = pow(-1,$i) * pow($x, 2*$i) / $f; //function

        if (abs($Cos[$i]) > $e) { //stop condition
            $stop++;
            echo $Cos[$i]." ";
            $S = $S + round($Cos[$i], 7); //sum
        }

    echo "\n".$S;
    return $S;
}

    Cos1(-3.2829266, 0.0026385);
?>