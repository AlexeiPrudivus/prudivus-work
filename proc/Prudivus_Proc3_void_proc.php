<?php
/* Описать процедуру Mean(X, Y, AMean, GMean), вычисляющую среднее арифметическое AMean = (X+Y)/2 и среднее геометрическое
   GMean = (X·Y)1/2 двух положительных чисел X и Y (X и Y — входные, AMean и GMean — выходные параметры вещественного типа).
   С помощью этой процедуры найти среднее арифметическое и среднее геометрическое для пар (A, B), (A, C), (A, D), если даны A, B, C, D.
*/
function Mean($X, $Y, &$AMean, &$GMean)
{

	$AMean = round(($X+$Y)/2, 3);
    $GMean = round(sqrt($X*$Y), 3);

    //return array($AMean, round($GMean, 3));
}
/*
	$A = rand(1, 10);
    echo "A=".$A." ";

    $B = rand(1, 10);
    echo "B=".$B." ";

    $C = rand(1, 10);
    echo "C=".$C." ";

    $D = rand(1, 10);
    echo "D=".$D."\n";

    Mean($A, $B, $AMean, $GMean);
    echo 'A+B/2= '.$AMean."  sqrt(A*B)= ".round($GMean, 3)."\n";

    Mean($A, $C, $AMean, $GMean);
    echo 'A+C/2= '.$AMean."  sqrt(A*C)= ".round($GMean, 3)."\n";

    Mean($A, $D, $AMean, $GMean);
    echo 'A+D/2= '.$AMean."  sqrt(A*D)= ".round($GMean, 3)."\n";*/
?>