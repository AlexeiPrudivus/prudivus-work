<?php
/*   Используя функции Leng и Area из заданий Proc56 и Proc58, описать функцию Dist(xP, yP, xA, yA, xB, yB) вещественного типа,
    находящую расстояние D(P, AB) от точки P до прямой AB по формуле
    D(P, AB) = 2·SPAB/|AB|, где SPAB — площадь треугольника PAB.
    С помощью этой функции найти расстояния от точки P до прямых AB, AC, BC, если даны координаты точек P, A, B, C.
*/
require_once("Prudivus_Proc58.php");

function Dist($xP, $yP, $xA, $yA, $xB, $yB) {
    return round(2 * Area($xP, $yP, $xA, $yA, $xB, $yB) / abs(Leng($xA, $yA, $xB, $yB)), 2);
}
/*
    $xP= rand(-10,10);
    $yP= rand(-10,10);
    $xA= rand(-10,10);
    $yA= rand(-10,10);
    $xB= rand(-10,10);
    $yB= rand(-10,10);

    echo "xP=".$xP." yP=".$yP."\n";
    echo "xA=".$xA." yA=".$yA."\n";
    echo "xB=".$xB." yB=".$yB."\n";

    echo Dist($xP, $yP, $xA, $yA, $xB, $yB);*/
?>