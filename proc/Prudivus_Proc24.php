<?php
/*  Описать функцию Even(K) логического типа, возвращающую true,
      если целый параметр K является четным, и false в противном случае.
    С ее помощью найти количество четных чисел в наборе из 10 целых чисел.
 */
function Even($K)
{
    return ($K % 2 == 0);
   /* if ($K % 2 == 0) {
        return true;
    } else {
        return false;
    }*/
}
/*
    $M = array();

    for ($i = 0; $i < 10; $i++) {
        $M[$i] = rand (-10,10);
        echo $M[$i]." ";
    }

    echo "\n";
    $s = 0;

    for ($i = 0; $i < count($M); $i++) {
        if (Even($M[$i]) == true) {
            $s++;
        }
    }
    echo $s;*/
?>