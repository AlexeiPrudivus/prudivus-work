<?php
/*  Описать функцию IsPower5(K) логического типа, возвращающую true, если
 целый параметр K (>0) является степенью числа 5, и false в противном случае.
                С ее помощью найти количество степеней числа 5
                   в наборе из 10 целых положительных чисел.
 */
function IsPower5($K)
{
    $b = log($K, 5) - floor(log($K, 5)); //находим дробную часть
    //echo round($b,2)." ";

    if (round($b, 2) == 0.00) {
        return true;
    } else {
        return false;
    }
}

$M = array();

    for ($i = 0; $i < 10; $i++) {
        $M[$i] = rand (1,1000);
        echo $M[$i]." ";
    }

    echo "\n";
    $s = 0;

    for ($i = 0; $i < count($M); $i++) {
        if (IsPower5($M[$i]) == true) {
            $s++;
        }
    }
    echo $s;
?>