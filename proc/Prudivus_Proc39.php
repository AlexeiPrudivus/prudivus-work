<?php
/*      Используя функции Power1 и Power2 из Proc37 и Proc38, описать функцию
   Power3(A, B) вещественного типа с вещественными параметрами, находящую AB
     следующим образом: если B имеет нулевую дробную часть, то вызывается
Power2(A, N), где N — переменная целого типа, равная числу B; иначе вызывается
 Power1(A, B). С помощью Power3 найти AP, BP, CP, если даны числа P, A, B, C.
 */
require_once("Prudivus_Proc37.php");
require_once("Prudivus_Proc38.php");

function Power3($A, $B) {

    if (is_int($B) == true) {
        return Power2($A, $B);
    } else {
        return Power1($A, $B);
    }
}

    echo Power3(2, 10)."\n";
    echo Power3(7, 3.5)."\n";
?>