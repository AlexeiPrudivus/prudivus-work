<?php
/*  Описать функцию RootCount(A, B, C) целого типа, определяющую количество корней квадратного уравнения
    A·x2 + B·x + C = 0 (A, B, C — вещественные параметры, A ≠ 0).
    С ее помощью найти количество корней для каждого из трех квадратных уравнений с данными коэффициентами.
    Количество корней определять по значению дискриминанта:
    D = B2 − 4·A·C.
 */
function RootCount($A, $B, $C)
{
    $D = $B*$B - 4*$A*$C;

    if ($D == 0) {
        return 1;
    } else
        if ($D > 0) {
            return 2;
        } else {
            return 0;
        }
}
/*
    $A = 1; echo $A." ";
    $B = 0; echo $B." ";
    $C = 0; echo $C." ";

    RootCount($A, $B, $C);

    $A1 = rand (1,10); echo "\n".$A1." ";
    $B1 = rand (0,10); echo $B1." ";
    $C1 = rand (0,10); echo $C1." ";

    RootCount($A1, $B1, $C1);

    $A2 = rand (1,10); echo "\n".$A2." ";
    $B2 = rand (0,10); echo $B2." ";
    $C2 = rand (0,10); echo $C2." ";

    RootCount($A2, $B2, $C2);*/
?>