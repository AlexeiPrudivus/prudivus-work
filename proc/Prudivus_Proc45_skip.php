<?php
/*   Описать функцию Power4(x, a, ε) вещественного типа (параметры x, a, ε —
  вещественные, |x|<1;a,ε>0), находящую приближенное значение функции (1+x)a:
  (1+x)a = 1 + a·x + a·(a-1)·x2/(2!) +...+ a·(a-1)·...·(a-n+1)·xn/(n!) +... .
  В сумме учитывать все слагаемые, модуль которых больше ε. С помощью Power4
    найти приближенное значение (1+x)a для данных x и a при шести данных ε.
 */

function  Power4($x, $a, $e) {

    $Power = array();

    $S = 0;
    $stop = 1;
    $f = 1;
    $sa = 1;

    for ($i = 0; $i < $stop; $i++) {
        if ($i == 0) { //factorial
            $f = 1;
        } else {
            $f *= $i;
        }

        $sa *= $a - $i + 1;//a

        if ($i == 0) {
            $Power[$i] = 1;
        } else if ($i == 1) {
            $Power[$i] = $a*$x;
        } else {
            $Power[$i] = $sa * pow($x, $i) / $f; //function
        }

        if (abs($Power[$i]) > $e) { //stop condition
            $stop++;
            echo $Power[$i]." ";
            $S = $S + round($Power[$i], 7); //sum
        }
    }

    echo "\n".$S;
    return $S;
}

    Power4(-0.0959620, 4.0583011, 0.1629514);
?>