<?php
/*  Используя функцию IsLeapYear из задания Proc52, описать функцию MonthDays(M, Y) целого типа,
    которая возвращает количество дней для M-го месяца года Y (1 ≤ M ≤ 12, Y > 0 — целые числа).
    Вывести значение функции MonthDays для данного года Y и месяцев M1, M2, M3.
*/
require_once("Prudivus_Proc52.php");

function  MonthDays1($M,$Y) {
    return cal_days_in_month(CAL_GREGORIAN, $M, $Y);
}

function  MonthDays($M,$Y) {
    switch ($M) {

        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12: {
            $M = 31;
            break;
        }

        case 4:
        case 6:
        case 9:
        case 11: {
            $M = 30;
            break;
        }

        case 2: {
            if (IsLeapYear($Y) == true) {
                $M = 29;
                break;
            } else {
                $M = 28;
                break;
            }
        }
    }
    return $M;
}
/*
    $M = rand(1,12);
    $Y = rand(0, 3000);

    echo $M."m ".$Y."y\n";
    echo  MonthDays($M,$Y);*/
?>