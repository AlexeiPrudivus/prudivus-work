<?php
/*   Описать процедуру InvDigits(K), меняющую порядок следования цифр
     целого положительного числа K на обратный (K — параметр целого типа,
     являющийся одновременно входным и выходным).
 */
require_once("../Prudivus_Proc7_void_proc.php");
class InvDigitsTest extends PHPUnit_Framework_TestCase {
//   last element of array is not 0
    public function testDigitNTest1(){
        $K = 6345;
        InvDigits($K);
        $this -> assertEquals($K, 5436);
    }
//   last element of array is 0
    public function testDigitNTest2(){
        $K = 1230;
        InvDigits($K);
        $this -> assertEquals($K, 321);
    }
}