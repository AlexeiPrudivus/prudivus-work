<?php
/*  Описать функцию DegToRad(D) вещественного типа, находящую величину угла
   в радианах, если дана его величина D в градусах (D — вещественное число,
   0 ≤ D < 360). Воспользоваться следующим соотношением: 180° = π радианов.
      В качестве значения π использовать 3.14. С помощью функции DegToRad
              перевести из градусов в радианы пять данных углов.
 */
require_once("../Prudivus_Proc32.php");
class DegtoRadTest extends PHPUnit_Framework_TestCase {
//    0 ≤ R < 2·π
    public function testDigitNTest1(){
        $this -> assertEquals(DegToRad(12),0.21);
        $this -> assertEquals(DegToRad(240),4.19);
        $this -> assertEquals(DegToRad(71),1.24);
    }
//    R > 0 || R > 2·π
    public function testDigitNTest12(){
        $this -> assertEquals(DegToRad(-1),false);
        $this -> assertEquals(DegToRad(361),false);
    }
}