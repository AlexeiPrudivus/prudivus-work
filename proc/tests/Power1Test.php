<?php
/*   Описать функцию Power1(A, B) вещественного типа, находящую величину AB
        по формуле AB = exp(B·ln(A)) (параметры A и B — вещественные).
    В случае нулевого или отрицательного параметра A функция возвращает 0.
*/

require_once("../Prudivus_Proc37.php");
class Power1 extends PHPUnit_Framework_TestCase {
//    length of A > 0
    public function testDigitNTest1(){
        $this -> assertEquals(Power1(10,2),100);
        $this -> assertEquals(Power1(5,2),25);
        $this -> assertEquals(Power1(3,3),27);
    }
//    length of A <= 0
    public function testDigitNTest2(){
        $this -> assertEquals(Power1(0,2),0);
        $this -> assertEquals(Power1(-5,2),0);
    }
}