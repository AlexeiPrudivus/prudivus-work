<?php
/* Описать функцию TriangleP(a, h), находящую периметр равнобедренного
 треугольника по его основанию a и высоте h, проведенной к основанию (a и h —
   вещественные). Для нахождения боковой стороны b
    треугольника использовать теорему Пифагора:  b2 = (a/2)2 + h2.
 */
require_once("../Prudivus_Proc20.php");
class TrianglePTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(TriangleP(4, 1), 14);
        $this -> assertEquals(TriangleP(8, 2), 48);
        $this -> assertEquals(TriangleP(3, 10), 207.5);
    }
}