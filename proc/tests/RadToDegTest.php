<?php
/*  Описать функцию RadToDeg(R) вещественного типа, находящую величину угла
   в градусах, если дана его величина R в радианах (R — вещественное число,
   0 ≤ R < 2·π).
 */
require_once("../Prudivus_Proc33.php");
class RadToDegTest extends PHPUnit_Framework_TestCase {
//    0 ≤ R < 2·π
    public function testDigitNTest1(){
        $this -> assertEquals(RadToDeg(4),229.3);
        $this -> assertEquals(RadToDeg(1),57.32);
        $this -> assertEquals(RadToDeg(2),114.65);
    }
//    R > 0 || R > 2·π
    public function testDigitNTest12(){
        $this -> assertEquals(RadToDeg(-1),false);
        $this -> assertEquals(RadToDeg(8),false);
    }
}