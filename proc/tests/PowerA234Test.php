<?php
/* Описать процедуру PowerA234(A, B, C, D), вычисляющую вторую, третью и четвертую степень числа A и возвращающую эти степени соответственно в
   переменных B, C и D (A — входной, B, C, D — выходные параметры; все параметры являются вещественными).
 */
require_once("../Prudivus_Proc2.php");
class PowerA234Test extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(PowerA234(2, $B, $C, $D), array(4, 8, 16));
        $this -> assertEquals(PowerA234(3, $B, $C, $D), array(9, 27, 81));
    }
//   for N 0 0
    public function testDigitNTest2(){
        $this -> assertEquals(PowerA234(0, $B, $C, $D), array(0, 0, 0));
    }
//   for N < 0
    public function testDigitNTest3(){
        $this -> assertEquals(PowerA234(-3, $B, $C, $D), array(9, -27, 81));
        $this -> assertEquals(PowerA234(-2, $B, $C, $D), array(4, -8, 16));
    }
}