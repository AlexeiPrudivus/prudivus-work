<?php
/* Используя функцию IsLeapYear из задания Proc52, описать функцию MonthDays(M, Y) целого типа,
    которая возвращает количество дней для M-го месяца года Y (1 ≤ M ≤ 12, Y > 0 — целые числа).
 */
require_once("../Prudivus_Proc53.php");
class MonthDaysTest extends PHPUnit_Framework_TestCase {
//   N is a leap year
    public function testDigitNTest1(){
        $this -> assertEquals(MonthDays(2, 2004), 29);
        $this -> assertEquals(MonthDays(5, 2008), 31);
    }
//   N is not a leap year
    public function testDigitNTest2(){
        $this -> assertEquals(MonthDays(2, 2017), 28);
        $this -> assertEquals(MonthDays(9, 2011), 30);
    }
}