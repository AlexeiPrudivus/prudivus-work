<?php
/*  Описать функцию Exp1(x, ε) вещественного типа (параметры x, ε —
     вещественные, ε > 0), находящую приближенное значение функции exp(x):
           exp(x) = 1 + x + x2/(2!) + x3/(3!) + ... + xn/(n!) + ...
 (n! = 1·2·...·n). В сумме учитывать все слагаемые, большие ε.
 */
require_once("../Prudivus_Proc40.php");
class Exp1Test extends PHPUnit_Framework_TestCase {
//  ε > 0
    public function testDigitNTest1(){
        $this -> assertEquals(Exp1(1.2463101, 0.7348752), 3.0229545);
        $this -> assertEquals(Exp1(-1.3464601, 0.898712), 0.5600173);
        $this -> assertEquals(Exp1(0.3464601, 0.0046743), 1.4134086);
    }

}