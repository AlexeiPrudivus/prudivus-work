<?php
/*  Описать функцию GCD2(A, B) целого типа, находящую наибольший общий делитель
    (НОД, greatest common divisor) двух целых положительных чисел A и B, используя
     алгоритм Евклида:   НОД(A, B) = НОД(B, A mod B), если B ≠ 0;   НОД(A, 0) = A,
     где «mod» обозначает операцию взятия остатка от деления.
 */
require_once("../Prudivus_Proc46.php");
class GCD2Test extends PHPUnit_Framework_TestCase {
//   NOD exists
    public function testDigitNTest1(){
        $this -> assertEquals(GCD2(4, 8), 4);
        $this -> assertEquals(GCD2(5, 630), 5);
    }
//   NOD does not exist
    public function testDigitNTest3(){
        $this -> assertEquals(GCD2(5, 3), 1);
        $this -> assertEquals(GCD2(700, 11), 1);
    }
}