<?php
/*  Описать функцию Arctg1(x, ε) вещественного типа (параметры x, ε —
 вещественные, |x|<1, ε>0), находящую приближенное значение функции arctg(x):
        arctg(x) = x - x3/3 + x5/5 - ... + (-1)n·x2·n+1/(2·n+1) + ... .
  В сумме учитывать все слагаемые, модуль которых больше ε.
 */
require_once("../Prudivus_Proc44.php");
class Arctg1Test extends PHPUnit_Framework_TestCase {
//  ε > 0
    public function testDigitNTest1(){
        $this -> assertEquals(Arctg1(-0.6937082, 0.0027126), -0.60765);
        $this -> assertEquals(Arctg1(-0.5003782, 0.0129265), -0.4586169);
        $this -> assertEquals(Arctg1(0.7518115, 0.0056663), 0.647334);
    }
}