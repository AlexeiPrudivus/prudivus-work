<?php
/*  Описать функцию DigitCount(K) целого типа, находящую количество цифр
  целого положительного числа K.
 */
require_once("../Prudivus_Proc29.php");
class DigitCountTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(DigitCount(18761), 5);
        $this -> assertEquals(DigitCount(17867857), 8);
        $this -> assertEquals(DigitCount(89), 2);
    }

}