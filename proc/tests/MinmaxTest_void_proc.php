<?php
/* Описать процедуру Minmax(X, Y), записывающую в переменную X минимальное
      из значений X и Y, а в переменную Y — максимальное из этих значений
       (X и Y — вещественные параметры, являющиеся одновременно входными
             и выходными).
 */
require_once("../Prudivus_Proc11_void_proc.php");
class MinmaxTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $X = 6345;
        $Y = 1;
        Minmax($X, $Y);
        $this -> assertEquals($X, 1);
        $this -> assertEquals($Y, 6345);

        $X = 3;
        $Y = 2;
        Minmax($X, $Y);
        $this -> assertEquals($X, 2);
        $this -> assertEquals($Y, 3);
    }
//   for N 0 0
    public function testDigitNTest2(){
        $X = 0;
        $Y = 0;
        Minmax($X, $Y);
        $this -> assertEquals($X, 0);
        $this -> assertEquals($Y, 0);
    }
//   for N < 0
    public function testDigitNTest3(){
        $X = -15;
        $Y = -8;
        Minmax($X, $Y);
        $this -> assertEquals($X, -15);
        $this -> assertEquals($Y, -8);
    }
}