<?php
/* Описать процедуру Swap(X, Y), меняющую содержимое переменных X и Y
       (X и Y — вещественные параметры, являющиеся одновременно входными
          и выходными).
 */
require_once("../Prudivus_Proc10_void_proc.php");
class SwapTest extends PHPUnit_Framework_TestCase {
//   for N > 0

    public function testDigitNTest1(){
        $X = 6345;
        $Y = 1;
        Swap($X, $Y);
        $this -> assertEquals($X, 63451);
        $this -> assertEquals($Y, 1);

        $X = 5;
        $Y = 5467;
        Swap($X, $Y);
        $this -> assertEquals($X, 5467);
        $this -> assertEquals($Y, 5);
    }
//   for N 0 0
    public function testDigitNTest2(){
        $X = 0;
        $Y = 0;
        Swap($X, $Y);
        $this -> assertEquals($X, 0);
        $this -> assertEquals($Y, 0);
    }
//   for N < 0
    public function testDigitNTest3(){
        $X = -5;
        $Y = -2;
        Swap($X, $Y);
        $this -> assertEquals($X, -2);
        $this -> assertEquals($Y, -5);
    }
}