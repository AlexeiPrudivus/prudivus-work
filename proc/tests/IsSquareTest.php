<?php
/*  Описать функцию IsSquare(K) логического типа, возвращающую true,
    если целый параметр K (>0) является квадратом некоторого целого числа,
      и false в противном случае.
 */
require_once("../Prudivus_Proc25.php");
class IsSquareTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(IsSquare(4), true);
        $this -> assertEquals(IsSquare(9), true);
        $this -> assertEquals(IsSquare(25), true);
        $this -> assertEquals(IsSquare(49), true);

    }
}