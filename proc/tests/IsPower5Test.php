<?php
/*  Описать функцию IsPower5(K) логического типа, возвращающую true, если
 целый параметр K (>0) является степенью числа 5, и false в противном случае.
 */
require_once("../Prudivus_Proc26.php");
class IsPower5Test extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(IsPower5(25), true);
        $this -> assertEquals(IsPower5(125), true);
        $this -> assertEquals(IsPower5(625), true);
    }
}