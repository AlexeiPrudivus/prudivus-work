<?php
/* Описать процедуру ShiftRight3(A, B, C), выполняющую правый циклический сдвиг:
         значение A переходит в B, значение B — в C, значение C — в A
 (A, B, C — вещественные параметры, являющиеся одновременно входными и выходными).
 */
require_once("../Prudivus_Proc14_void_proc.php");
class ShiftRight3Test extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $A = 1;
        $B = 2;
        $C = 3;

        ShiftRight3($A, $B, $C);
        $this -> assertEquals($A, 3);
        $this -> assertEquals($B, 1);
        $this -> assertEquals($C, 2);
    }
//   for N 0 0
    public function testDigitNTest2(){
        $A = 0;
        $B = 0;
        $C = 0;

        ShiftRight3($A, $B, $C);
        $this -> assertEquals($A, 0);
        $this -> assertEquals($B, 0);
        $this -> assertEquals($C, 0);
    }
//   for N < 0
    public function testDigitNTest3(){
        $A = -1;
        $B = -2;
        $C = -3;

        ShiftRight3($A, $B, $C);
        $this -> assertEquals($A, -3);
        $this -> assertEquals($B, -1);
        $this -> assertEquals($C, -2);
    }
}