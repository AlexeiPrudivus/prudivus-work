<?php
/*  Описать функцию IsPowerN(K, N) логического типа, возвращающую true, если
   целый параметр K (>0) является степенью числа N (>1), и false в противном
 случае.
 */
require_once("../Prudivus_Proc27.php");
class IsPowerNTest extends PHPUnit_Framework_TestCase {
//   for K > 0; N > 1;
    public function testDigitNTest1(){
        $this -> assertEquals(IsPowerN(25, 5), true);
        $this -> assertEquals(IsPowerN(16, 4), true);
        $this -> assertEquals(IsPowerN(625, 5), true);
    }
}