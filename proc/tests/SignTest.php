<?php
/* Описать процедуру Mean(X, Y, AMean, GMean), вычисляющую среднее арифметическое AMean = (X+Y)/2 и среднее геометрическое
   GMean = (X·Y)1/2 двух положительных чисел X и Y (X и Y — входные, AMean и GMean — выходные параметры вещественного типа).
 */
require_once("../Prudivus_Proc16.php");
class SignTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(Sign(4), 1);
        $this -> assertEquals(Sign(1), 1);
    }
//   for N 0 0
    public function testDigitNTest2(){
        $this -> assertEquals(Sign(0), 0);
    }
//   for N < 0
    public function testDigitNTest3(){
        $this -> assertEquals(Sign(-1), -1);
        $this -> assertEquals(Sign(-2), -1);
    }
}