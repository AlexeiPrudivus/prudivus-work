<?php
/* Описать процедуру SortInc3(A, B, C), меняющую содержимое переменных A, B, C
   таким образом, чтобы их значения оказались упорядоченными по возрастанию
      (A, B, C — вещественные параметры, являющиеся одновременно входными
       и выходными).
 */
require_once("../Prudivus_Proc12_void_proc.php");
class SortInc3Test extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $A = 6345;
        $B = 67;
        $C = 1;

        SortInc3($A, $B, $C);
        $this -> assertEquals($A, 1);
        $this -> assertEquals($B, 67);
        $this -> assertEquals($B, 6345);
    }
//   for N 0 0
    public function testDigitNTest2(){
        $A = 0;
        $B = 0;
        $C = 0;

        SortInc3($A, $B, $C);
        $this -> assertEquals($A, 0);
        $this -> assertEquals($B, 0);
        $this -> assertEquals($B, 0);
    }
//   for N < 0
    public function testDigitNTest3(){
        $A = -6345;
        $B = -67;
        $C = -1;

        SortInc3($A, $B, $C);
        $this -> assertEquals($A, -6345);
        $this -> assertEquals($B, -67);
        $this -> assertEquals($B, -1);
    }
}