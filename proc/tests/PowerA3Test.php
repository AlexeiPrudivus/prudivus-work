<?php
/* Описать процедуру PowerA3(A, B), вычисляющую третью степень числа A и возвращающую ее в переменной B
 * (A — входной, B — выходной параметр; оба параметра являются вещественными).
 */
require_once("../Prudivus_Proc1.php");
class PowerA3Test extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(PowerA3(3, $B),27);
    }
//   for N 0 0
    public function testDigitNTest2(){
        $this -> assertEquals(PowerA3(0, $B),0);
    }
//   for N < 0
    public function testDigitNTest3(){
        $this -> assertEquals(PowerA3(-3, $B),-27);
    }
}