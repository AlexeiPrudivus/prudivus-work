<?php
/* Используя функцию Dist из задания Proc59, описать процедуру Altitudes(xA, yA, xB, yB, xC, yC, hA, hB, hC),
    находящую высоты hA, hB, hC треугольника ABC (выходные параметры),
    проведенные соответственно из вершин A, B, C (их координаты являются входными параметрами).
 */
require_once("../Prudivus_Proc60.php");
class AltitudesTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $this -> assertEquals(Altitudes(7, 6, 4, 4, -10, -10, $hA, $hB, $hC), array(0.71, 0.6, 3.91));
        $this -> assertEquals(Altitudes(-1, -8, -8, 1, 2, 0, $hA, $hB, $hC), array(8.25, 9.71, 7.28));
        $this -> assertEquals(Altitudes(1, 0, 0, 1, 1, 1, $hA, $hB, $hC), array(1, 1, 0.71));
        $this -> assertEquals(Altitudes(1, 0, 0, 1, -1, -1, $hA, $hB, $hC), array(1.34, 1.34, 2.13));
    }
}