<?php
/* Описать процедуру Mean(X, Y, AMean, GMean), вычисляющую среднее арифметическое AMean = (X+Y)/2 и среднее геометрическое
   GMean = (X·Y)1/2 двух положительных чисел X и Y (X и Y — входные, AMean и GMean — выходные параметры вещественного типа).
 */
require_once("../Prudivus_Proc3_void_proc.php");
class MeanTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        Mean(4, 4, $AMean, $GMean);
        $this -> assertEquals(Mean(4, 4, $AMean, $GMean), null);
        $this -> assertEquals($AMean, 4); $this -> assertEquals($GMean, 4);

        Mean(1, 5, $AMean, $GMean);
        $this -> assertEquals(Mean(1, 5, $AMean, $GMean), null);
        $this -> assertEquals($AMean, 3); $this -> assertEquals($GMean, 2.236);
    }
//   for N 0 0
    public function testDigitNTest2(){
        Mean(0, 0, $AMean, $GMean);
        $this -> assertEquals(Mean(0, 0, $AMean, $GMean), null);
        $this -> assertEquals($AMean, 0); $this -> assertEquals($GMean, 0);
    }
//   for N < 0
    public function testDigitNTest3(){
        //$this -> assertEquals(Mean(-1, -1, $AMean, $GMean), -1, 1);
        Mean(-1, -1, $AMean, $GMean);
        $this -> assertEquals(Mean(-1, -1, $AMean, $GMean), null);
        $this -> assertEquals($AMean, -1); $this -> assertEquals($GMean, 1);
        //$this -> assertEquals(Mean(-2, -2, $AMean, $GMean), -2, 2);
        Mean(-2, -2, $AMean, $GMean);
        $this -> assertEquals(Mean(-2, -2, $AMean, $GMean), null);
        $this -> assertEquals($AMean, -2); $this -> assertEquals($GMean, 2);
    }
}