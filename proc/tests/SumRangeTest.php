<?php
/* Описать функцию SumRange(A, B) целого типа, находящую сумму всех целых чисел
 от A до B включительно (A и B — целые). Если A > B, то функция возвращает 0.
        С помощью этой функции найти суммы чисел от A до B и от B до C,
                           если даны числа A, B, C.
 */
require_once("../Prudivus_Proc21.php");
class SumRangeTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(SumRange(0, 6), 21);
        $this -> assertEquals(SumRange(6, 8), 21);
        $this -> assertEquals(SumRange(1, 3), 6);
    }
//   for N = 0
    public function testDigitNTest2(){
        $this -> assertEquals(SumRange(0, 0), 0);
    }
//   for A > B
    public function testDigitNTest3(){
        $this -> assertEquals(SumRange(5, 4), 0);
    }
}