<?php
/*  Используя функцию GCD2 (см. Proc46), описать процедуру Frac1(a, b, p, q),
  преобразующую дробь a/b к несократимому виду p/q (все параметры процедуры —
целого типа, a и b — входные, p и q — выходные). Знак результирующей дроби p/q
   приписывается числителю (т.е. q > 0).
 */
require_once("../Prudivus_Proc48.php");
class Frac1Test extends PHPUnit_Framework_TestCase {
//  +a/+b
    public function testDigitNTest1(){
        $this -> assertEquals(Frac1(4, 8, $p, $q), array(1,2));
        $this -> assertEquals(Frac1(65, 5, $p, $q), array(13,1));
    }
//  +a/-b
    public function testDigitNTest2(){
        $this -> assertEquals(Frac1(4, -8, $p, $q), array(-1,2));
        $this -> assertEquals(Frac1(65, -5, $p, $q), array(-13,1));
    }
//  -a/+b
    public function testDigitNTest3(){
        $this -> assertEquals(Frac1(-4, 8, $p, $q), array(-1,2));
        $this -> assertEquals(Frac1(-65, 5, $p, $q), array(-13,1));
    }
//  -a/-b
    public function testDigitNTest4(){
        $this -> assertEquals(Frac1(-4, -8, $p, $q), array(1,2));
        $this -> assertEquals(Frac1(-65, -5, $p, $q), array(13,1));
    }
}