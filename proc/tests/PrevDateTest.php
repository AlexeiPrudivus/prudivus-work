<?php
/* Описать функцию IsLeapYear(Y) логического типа, которая возвращает True, если год Y (целое положительное число) является високосным,
    и False в противном случае. Вывести значение функции IsLeapYear для пяти данных значений параметра Y.
 */
require_once("../Prudivus_Proc54.php");
class PrevDateTest extends PHPUnit_Framework_TestCase {
//   same year
    public function testDigitNTest1(){
        $this -> assertEquals(PrevDate(1, 3, 2004), array(29,2,2004));
        $this -> assertEquals(PrevDate(31,4, 2005), array(30,3,2005));
        $this -> assertEquals(PrevDate(24,7, 2005), array(23,7,2005));
    }
//   changing years
    public function testDigitNTest2(){
        $this -> assertEquals(PrevDate(1, 1, 2001), array(31,12,2000));
        $this -> assertEquals(PrevDate(1, 1, 2000), array(31,12,1999));
    }
}