<?php
/*  Учитывая соотношение НОД(A, B, C) = НОД(НОД(A, B), C) и используя функцию GCD2 (см. Proc46),
    описать функцию GCD3(A, B, C) целого типа, находящую наибольший общий делитель трех целых положительных чисел A, B, C.
 */
require_once("../Prudivus_Proc49.php");
class GCD3Test extends PHPUnit_Framework_TestCase {
//   NOD exists
    public function testDigitNTest1(){
        $this -> assertEquals(GCD3(4, 8, 100), 4);
        $this -> assertEquals(GCD3(5, 630, 125), 5);
    }
//   NOD does not exist
    public function testDigitNTest3(){
        $this -> assertEquals(GCD2(5, 3, 7), 1);
        $this -> assertEquals(GCD2(700, 11, 91), 1);
    }
}