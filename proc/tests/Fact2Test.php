<?php
/*  Описать функцию Fact2(N) вещественного типа, вычисляющую двойной факториал:
   N!! = 1·3·5·...·N, если N — нечетное; N!! = 2·4·6·...·N, если N — четное
(N > 0 — параметр целого типа; вещественное возвращаемое значение используется
для того, чтобы избежать целочисленного переполнения при больших значениях N).
*/

require_once("../Prudivus_Proc35.php");
class Fact2Test extends PHPUnit_Framework_TestCase {
//    length of N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(Fact2(7),105);
        $this -> assertEquals(Fact2(5),15);
        $this -> assertEquals(Fact2(6),48);
        $this -> assertEquals(Fact2(2),2);
    }
}