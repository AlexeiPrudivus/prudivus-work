<?php
/*  Описать функцию Fact2(N) вещественного типа, вычисляющую двойной факториал:
   N!! = 1·3·5·...·N, если N — нечетное; N!! = 2·4·6·...·N, если N — четное
(N > 0 — параметр целого типа; вещественное возвращаемое значение используется
для того, чтобы избежать целочисленного переполнения при больших значениях N).
*/

require_once("../Prudivus_Proc36.php");
class FibTest extends PHPUnit_Framework_TestCase {
//    length of N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(Fib(10),55);
        $this -> assertEquals(Fib(5),5);
        $this -> assertEquals(Fib(6),8);
        $this -> assertEquals(Fib(2),1);
        $this -> assertEquals(Fib(1),1);
        $this -> assertEquals(Fib(0),0);
    }
}