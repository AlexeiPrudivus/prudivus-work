<?php
/* /* Описать процедуру RectPS(x1,y1,x2,y2,P,S), вычисляющую периметр P и площадь S
   прямоугольника со сторонами, параллельными осям координат, по координатам
  (x1,y1), (x2,y2) его противоположных вершин (x1,y1,x2,y2 — входные, P и S —
    выходные параметры вещественного типа).
 */
require_once("../Prudivus_Proc5_void_proc.php");
class RectPSTestTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        RectPS(1, 0, 0, 1, $P, $S);
        $this -> assertEquals($P, 4);
        $this -> assertEquals($S, 1);

        //$this -> assertEquals(RectPS(2, 4, 4, 2, $P, $S), array(8, 4));
    }
//   for N 0 0
    public function testDigitNTest2(){
        RectPS(0, 0, 0, 0, $P, $S);
        $this -> assertEquals($P, 0);
        $this -> assertEquals($S, 0);
    }
//   for N < 0
    public function testDigitNTest3(){
        RectPS(-1, 0, 0, -1, $P, $S);
        $this -> assertEquals($P, 4);
        $this -> assertEquals($S, 1);

        //$this -> assertEquals(RectPS(-3, 6, 6, -4, $P, $S), array(38, 90));
    }
}