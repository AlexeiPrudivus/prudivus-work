<?php
/* Описать функцию Leng(xA, yA, xB, yB) вещественного типа, находящую длину отрезка AB на плоскости по координатам его концов:
    |AB| = ((xA − xB)2 + (yA − yB)2)1/2 (xA, yA, xB, yB — вещественные параметры).
    С помощью этой функции найти длины отрезков AB, AC, AD, если даны координаты точек A, B, C, D.
 */
require_once("../Prudivus_Proc56.php");
class LengTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $this -> assertEquals(Leng(64, -19, 3, -48), 67.54);
        $this -> assertEquals(Leng(1, 0, 0, 1), 1.41);
        $this -> assertEquals(Leng(21, -47, 60, -20), 47.43);
        $this -> assertEquals(Leng(-20, -53, -95, -13), 85);
    }
}