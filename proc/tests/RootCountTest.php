<?php
/* Описать процедуру PowerA234(A, B, C, D), вычисляющую вторую, третью и четвертую степень числа A и возвращающую эти степени соответственно в
   переменных B, C и D (A — входной, B, C, D — выходные параметры; все параметры являются вещественными).
 */
require_once("../Prudivus_Proc17.php");
class RootCountTest extends PHPUnit_Framework_TestCase {
//   for D > 0
    public function testDigitNTest1(){
        $this -> assertEquals(RootCount(1, 10, 4), 2);
        $this -> assertEquals(RootCount(2, 10, 5), 2);
    }
//   for D = 0
    public function testDigitNTest2(){
        $this -> assertEquals(RootCount(1, 2, 1), 1);
        $this -> assertEquals(RootCount(1, 0, 0), 1);
    }
//   for D < 0
    public function testDigitNTest3(){
        $this -> assertEquals(RootCount(9, 1, 1), 0);
        $this -> assertEquals(RootCount(1, 0, 0), 0);
    }
}