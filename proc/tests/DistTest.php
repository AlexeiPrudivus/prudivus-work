<?php
/* Используя функции Leng и Area из заданий Proc56 и Proc58, описать функцию Dist(xP, yP, xA, yA, xB, yB) вещественного типа,
    находящую расстояние D(P, AB) от точки P до прямой AB по формуле
    D(P, AB) = 2·SPAB/|AB|, где SPAB — площадь треугольника PAB.
 */
require_once("../Prudivus_Proc59.php");
class DistTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $this -> assertEquals(Dist(-10, -6, -5, -4, 2, 7), 3.16);
        $this -> assertEquals(Dist(3, -8, 0, -5, 10, 6), 4.24);
        $this -> assertEquals(Dist(1, 0, 0, 1, 1, 1), 1);
        $this -> assertEquals(Dist(1, 0, 0, 1, -1, -1), 1.34);
    }
}