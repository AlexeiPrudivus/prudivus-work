<?php
/* Описать процедуру DigitCountSum(K, C, S), находящую количество C
            цифр целого положительного числа K, а также их сумму S
            (K — входной, C и S — выходные параметры целого типа).
 */
require_once("../Prudivus_Proc6_void_proc.php");
class DigitCountSumTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        DigitCountSum(6345, $C, $S);
        $this -> assertEquals($C, 4);
        $this -> assertEquals($S, 18);

        //$this -> assertEquals(DigitCountSum(5555, $C, $S), array(4, 20));
        //$this -> assertEquals(DigitCountSum(10, $C, $S), array(2, 1));
        //$this -> assertEquals(DigitCountSum(184, $C, $S), array(3, 13));
    }
}