<?php
/* Описать процедуру AddLeftDigit(D, K), добавляющую к целому положительному
 числу K слева цифру D (D — входной параметр целого типа, лежащий в диапазоне
  1–9, K — параметр целого типа, являющийся одновременно входным и выходным).
 */
require_once("../Prudivus_Proc9_void_proc.php");
class AddLeftDigitTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $K = 6;
        AddLeftDigit(9, $K);
        $this -> assertEquals($K, 96);

        $K = 500;
        AddLeftDigit(1, $K);
        $this -> assertEquals($K, 1500);
    }
//   for N 0 0
    public function testDigitNTest2(){
        $K = 0;
        AddLeftDigit(0, $K);
        $this -> assertEquals($K, 0);
    }
//   for N1=0 N2<>0
    public function testDigitNTest3(){
        $K = 0;
        AddLeftDigit(1, $K);
        $this -> assertEquals($K, 1);
    }
}