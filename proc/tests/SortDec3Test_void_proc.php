<?php
/* Описать процедуру SortDec3(A, B, C), меняющую содержимое переменных A, B, C
     таким образом, чтобы их значения оказались упорядоченными по убыванию
      (A, B, C — вещественные параметры, являющиеся одновременно входными
        и выходными).
 */
require_once("../Prudivus_Proc13_void_proc.php");
class SortDec3Test extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $A = 6345;
        $B = 67;
        $C = 1;

        SortDec3($A, $B, $C);
        $this -> assertEquals($A, 6345);
        $this -> assertEquals($B, 67);
        $this -> assertEquals($B, 1);
    }
//   for N 0 0
    public function testDigitNTest2(){
        $A = 0;
        $B = 0;
        $C = 0;

        SortDec3($A, $B, $C);
        $this -> assertEquals($A, 0);
        $this -> assertEquals($B, 0);
        $this -> assertEquals($B, 0);
    }
//   for N < 0
    public function testDigitNTest3(){
        $A = -6;
        $B = -1;
        $C = -9;

        SortDec3($A, $B, $C);
        $this -> assertEquals($A, -1);
        $this -> assertEquals($B, -6);
        $this -> assertEquals($B, -9);
    }
}