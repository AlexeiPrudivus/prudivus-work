<?php
/*  Описать функцию DigitN(K, N) целого типа, возвращающую N-ю цифру целого
       положительного числа K (цифры в числе нумеруются справа налево).
      Если количество цифр в числе K меньше N, то функция возвращает -1.
 */
require_once("../Prudivus_Proc30.php");
class DigitNTest extends PHPUnit_Framework_TestCase {
//    length of N < length of K
    public function testDigitNTest1(){
        $this -> assertEquals(DigitN(2252,2),5);
        $this -> assertEquals(DigitN(465879,4),5);
        $this -> assertEquals(DigitN(27892,1),2);
        $this -> assertEquals(DigitN(225,1),5);
    }
//    length of N > length of K
    public function testDigitNTest2(){
        $this -> assertEquals(DigitN(2252,10),-1);
        $this -> assertEquals(DigitN(465879,60),-1);
        $this -> assertEquals(DigitN(27892,15),-1);
        $this -> assertEquals(DigitN(225,10000),-1);
    }

}