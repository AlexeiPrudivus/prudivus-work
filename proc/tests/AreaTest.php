<?php
/* Используя функцию Leng из задания Proc56, описать функцию Perim(xA, yA, xB, yB, xC, yC) вещественного типа,
    находящую периметр треугольника ABC по координатам его вершин (xA, yA, xB, yB, xC, yC — вещественные параметры).
 */
require_once("../Prudivus_Proc58.php");
class AreaTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $this -> assertEquals(Area(-10, 9, 3, 1, 2, 2), 2.43);
        $this -> assertEquals(Area(10, 7, 5, -5, -2, -6), 39.5);
        $this -> assertEquals(Area(1, 0, 0, 1, 1, 1), 0.5);
        $this -> assertEquals(Area(1, 0, 0, 1, -1, -1), 1.5);
    }
}