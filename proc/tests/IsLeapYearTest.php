<?php
/* Описать функцию IsLeapYear(Y) логического типа, которая возвращает True, если год Y (целое положительное число) является високосным,
    и False в противном случае. Вывести значение функции IsLeapYear для пяти данных значений параметра Y.
 */
require_once("../Prudivus_Proc52.php");
class IsLeapYearTest extends PHPUnit_Framework_TestCase {
//   N is a leap year
    public function testDigitNTest1(){
        $this -> assertEquals(IsLeapYear(800), true);
        $this -> assertEquals(IsLeapYear(2116), true);
    }
//   N is not a leap year
    public function testDigitNTest2(){
        $this -> assertEquals(IsLeapYear(900), false);
        $this -> assertEquals(IsLeapYear(205), false);
    }
}