<?php
/*  Описать функцию Exp1(x, ε) вещественного типа (параметры x, ε —
     вещественные, ε > 0), находящую приближенное значение функции exp(x):
           exp(x) = 1 + x + x2/(2!) + x3/(3!) + ... + xn/(n!) + ...
 (n! = 1·2·...·n). В сумме учитывать все слагаемые, большие ε.
 */
require_once("../Prudivus_Proc43.php");
class Ln1Test extends PHPUnit_Framework_TestCase {
//  ε > 0
    public function testDigitNTest1(){
        $this -> assertEquals(Ln1(-0.1301259, 0.0022495), -0.1385923);
        $this -> assertEquals(Ln1(0.6301059, 0.0908688), 0.4315892);
        $this -> assertEquals(Ln1(-0.5105941, 0.0004753), -0.714075);
    }

}