<?php
/* Описать функцию CircleS(R) вещественного типа, находящую площадь круга
      радиуса R (R — вещественное).  Площадь круга радиуса R вычисляется
         по формуле S = π·R2. В качестве значения π использовать 3.14.
 */
require_once("../Prudivus_Proc18.php");
class CircleSTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(CircleS(4), 50.24);
        $this -> assertEquals(CircleS(1), 3.14);
    }
//   for N = 0
    public function testDigitNTest2(){
        $this -> assertEquals(CircleS(0), 0);
    }
}