<?php
/* Описать функцию Calc(A, B, Op) вещественного типа, выполняющую над ненулевыми
  вещественными числами A и B одну из арифметических операций и возвращающую
  ее результат. Вид операции определяется целым параметром Op: 1 — вычитание,
   2 — умножение, 3 — деление, остальные значения — сложение.
 */
require_once("../Prudivus_Proc22.php");
class CalcSTest extends PHPUnit_Framework_TestCase {
//   for Op = 1
    public function testDigitNTest1(){
        $this -> assertEquals(Calc(4,2,1), 2);
        $this -> assertEquals(Calc(-5, 2, 1), -7);
    }
//   for Op = 2
    public function testDigitNTest2(){
        $this -> assertEquals(Calc(5,5,2), 25);
        $this -> assertEquals(Calc(6,3,2), 18);
    }
//   for Op = 3
    public function testDigitNTest3(){
        $this -> assertEquals(Calc(5,5,3), 1);
        $this -> assertEquals(Calc(6,3,3), 2);
    }

//   for Op = other
    public function testDigitNTest4(){
        $this -> assertEquals(Calc(5,5,4), 10);
        $this -> assertEquals(Calc(6,9,0), 15);
    }
//   for A = 0
    public function testDigitNTest5(){
        $this -> assertEquals(Calc(0,5,4), 'error');
        $this -> assertEquals(Calc(0,9,0), 'error');
    }
//   for B = 0
    public function testDigitNTest6(){
        $this -> assertEquals(Calc(5,0,4), 'error');
        $this -> assertEquals(Calc(6,0,0), 'error');
    }
}