<?php
/*  Описать функцию IsPrime(N) логического типа, возвращающую true, если целый
  параметр N (>1) является простым числом, и false в противном случае (число,
   большее 1, называется простым, если оно не имеет положительных делителей,
        кроме 1 и самого себя).
 */
require_once("../Prudivus_Proc28.php");
class IsPrimeTest extends PHPUnit_Framework_TestCase {
//   for N prime
    public function testDigitNTest1(){
        $this -> assertEquals(IsPrime(11), true);
        $this -> assertEquals(IsPrime(17), true);
        $this -> assertEquals(IsPrime(89), true);
    }
//   for N not prime
    public function testDigitNTest2(){
        $this -> assertEquals(IsPrime(25), false);
        $this -> assertEquals(IsPrime(16), false);
        $this -> assertEquals(IsPrime(56), false);
    }
}