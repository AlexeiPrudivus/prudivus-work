<?php
/*  Используя функции Power1 и Power2 из Proc37 и Proc38, описать функцию
   Power3(A, B) вещественного типа с вещественными параметрами, находящую AB
     следующим образом: если B имеет нулевую дробную часть, то вызывается
Power2(A, N), где N — переменная целого типа, равная числу B; иначе вызывается
 Power1(A, B).
*/

require_once("../Prudivus_Proc39.php");
class Power3Test extends PHPUnit_Framework_TestCase {
//    B is int
    public function testDigitNTest1(){
        $this -> assertEquals(Power3(10,2),100);
        $this -> assertEquals(Power3(5,2),25);
        $this -> assertEquals(Power3(3,3),27);
    }
//    B is real
    public function testDigitNTest2(){
        $this -> assertEquals(Power3(2,1.5),2.83);
        $this -> assertEquals(Power3(7,3.5),907.49);
    }
}