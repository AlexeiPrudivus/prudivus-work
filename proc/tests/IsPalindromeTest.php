<?php
/*  Описать функцию IsPalindrome(K), возвращающую true,
    если целый параметр K (>0) является палиндромом
    (т.е. его запись читается одинаково слева направо и справа налево),
    и false в противном случае.
 */
require_once("../Prudivus_Proc31.php");
class IsPalindromeTest extends PHPUnit_Framework_TestCase {
//    K is a palindrome
    public function testDigitNTest1(){
        $this -> assertEquals(IsPalindrome(22522),true);
        $this -> assertEquals(IsPalindrome(11),true);
        $this -> assertEquals(IsPalindrome(747),true);
        $this -> assertEquals(IsPalindrome(666),true);
    }
//    K is not a palindrome
    public function testDigitNTest2(){
        $this->assertEquals(IsPalindrome(2252), false);
        $this->assertEquals(IsPalindrome(465879), false);
        $this->assertEquals(IsPalindrome(27892), false);
        $this->assertEquals(IsPalindrome(225), false);
    }

}