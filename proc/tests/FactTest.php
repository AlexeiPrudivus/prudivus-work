<?php
/*  Описать функцию Fact(N) вещественного типа, вычисляющую
       значение факториала N! = 1·2·...·N (N > 0 — параметр целого типа;
   вещественное возвращаемое значение используется для того, чтобы избежать
             целочисленного переполнения при больших значениях N).
       С помощью этой функции найти факториалы пяти данных целых чисел.
 */
require_once("../Prudivus_Proc33.php");
class FactTest extends PHPUnit_Framework_TestCase {
//    N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(Fact(8),40320);
        $this -> assertEquals(Fact(3),6);
        $this -> assertEquals(Fact(4),24);
    }
}