<?php
/* Описать функцию Quarter(x, y) целого типа, определяющую номер
         координатной четверти, в которой находится точка с ненулевыми
        вещественными координатами (x, y). С помощью этой функции найти
                 номера координатных четвертей для трех точек
                      с данными ненулевыми координатами.
 */
require_once("../Prudivus_Proc23.php");
class QuarterTest extends PHPUnit_Framework_TestCase {
//   for x > 0; y > 0;
    public function testDigitNTest1(){
        $this -> assertEquals(Quarter(4,2), 1);
        $this -> assertEquals(Quarter(5, 7), 1);
    }
//   for x < 0; y > 0;
    public function testDigitNTest2(){
        $this -> assertEquals(Quarter(-5,5), 2);
        $this -> assertEquals(Quarter(-6,3), 2);
    }
//   for x < 0; y < 0;
    public function testDigitNTest3(){
        $this -> assertEquals(Quarter(-5,-3), 3);
        $this -> assertEquals(Quarter(-3,-3), 3);
    }

//   for x > 0; y < 0;
    public function testDigitNTest4(){
        $this -> assertEquals(Quarter(5,-4), 4);
        $this -> assertEquals(Quarter(6,-9), 4);
    }
}