<?php
/*  Описать процедуру IncTime(H, M, S, T), которая увеличивает на T секунд время, заданное в часах H, минутах M и секундах S
    (H, M и S — входные и выходные параметры, T — входной параметр; все параметры — целые положительные).
    Дано время (в часах H, минутах M, секундах S) и целое число T.
 */
require_once("../Prudivus_Proc51_void_proc.php");
class IncTimeTest extends PHPUnit_Framework_TestCase {
//   N >= 0
    public function testDigitNTest1(){
        $H = 0;
        $M = 1;
        $S = 5;
        $T = 5;

        IncTime($H, $M, $S, $T);
        $this -> assertEquals($H, 0);
        $this -> assertEquals($M, 1);
        $this -> assertEquals($S, 10);

        $H = 0;
        $M = 0;
        $S = 0;
        $T = 0;

        IncTime($H, $M, $S, $T);
        $this -> assertEquals($H, 0);
        $this -> assertEquals($M, 0);
        $this -> assertEquals($S, 0);

        $H = 0;
        $M = 20;
        $S = 50;
        $T = 15;

        IncTime($H, $M, $S, $T);
        $this -> assertEquals($H, 0);
        $this -> assertEquals($M, 21);
        $this -> assertEquals($S, 5);

        $H = 6;
        $M = 32;
        $S = 44;
        $T = 7761;

        IncTime($H, $M, $S, $T);
        $this -> assertEquals($H, 8);
        $this -> assertEquals($M, 42);
        $this -> assertEquals($S, 5);
    }
}