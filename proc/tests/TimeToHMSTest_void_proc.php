<?php
/*  Описать процедуру TimeToHMS(T, H, M, S),
    определяющую по времени T (в секундах) содержащееся в нем количество часов H, минут M и секунд S
    (T — входной, H, M и S — выходные параметры целого типа).
 */
require_once("../Prudivus_Proc50_void_proc.php");
class TimeToHMSTest extends PHPUnit_Framework_TestCase {
//   N >= 0
    public function testDigitNTest1(){
        $A = 7142;

        TimeToHMS($A, $H, $M, $S);
        $this -> assertEquals($H, 1);
        $this -> assertEquals($M, 59);
        $this -> assertEquals($S, 2);

        $A = 0;

        TimeToHMS($A, $H, $M, $S);
        $this -> assertEquals($H, 0);
        $this -> assertEquals($M, 0);
        $this -> assertEquals($S, 0);

        /*
        $this -> assertEquals(TimeToHMS(5, $H, $M, $S), array(0, 0, 5));
        $this -> assertEquals(TimeToHMS(0, $H, $M, $S), array(0, 0, 0));
        $this -> assertEquals(TimeToHMS(65, $H, $M, $S), array(0, 1, 5));*/
    }
}