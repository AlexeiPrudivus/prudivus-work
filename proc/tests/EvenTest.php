<?php
/*  Описать функцию Even(K) логического типа, возвращающую true,
      если целый параметр K является четным, и false в противном случае.
 */
require_once("../Prudivus_Proc24.php");
class EvenTest extends PHPUnit_Framework_TestCase {
//   for N = even
    public function testDigitNTest1(){
        $this -> assertEquals(Even(4), true);
        $this -> assertEquals(Even(2), true);
    }
//   for N = 0
    public function testDigitNTest2(){
        $this -> assertEquals(Even(0), true);
    }
//   for N = odd
    public function testDigitNTest3(){
        $this -> assertEquals(Even(5), false);
        $this -> assertEquals(Even(7), false);
    }
}