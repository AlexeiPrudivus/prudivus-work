<?php
/*  Наименьшее общее кратное (least common multiple) двух целых положительных чисел A и B равно A·(B/НОД(A, B)),
    где НОД(A, B) — наибольший общий делитель A и B. Используя функцию GCD2 (см. Proc46), описать функцию LCM2(A, B) целого типа,
    находящую наименьшее общее кратное чисел A и B.
 */
require_once("../Prudivus_Proc48.php");
class LCM2Test extends PHPUnit_Framework_TestCase {
//   A >0; B > 0;
    public function testDigitNTest1(){
        $this -> assertEquals(LCM2(4, 3), 12);
        $this -> assertEquals(LCM2(3, 29), 87);
        $this -> assertEquals(LCM2(22, 23), 506);
        $this -> assertEquals(LCM2(26, 5), 130);
    }
}