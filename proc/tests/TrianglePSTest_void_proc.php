<?php
/* Описать процедуру PowerA234(A, B, C, D), вычисляющую вторую, третью и четвертую степень числа A и возвращающую эти степени соответственно в
   переменных B, C и D (A — входной, B, C, D — выходные параметры; все параметры являются вещественными).
 */
require_once("../Prudivus_Proc4_void_proc.php");
class TrianglePSTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        TrianglePS(10, $P, $S);
        $this -> assertEquals($P, 30);
        $this -> assertEquals($S, 43.301);

        //$this -> assertEquals(TrianglePS(3, $P, $S), array(9, 3.897));
        //$this -> assertEquals(TrianglePS(1, $P, $S), array(3, 0.433));
    }
//   for N = 0
    public function testDigitNTest2(){
        TrianglePS(0, $P, $S);
        $this -> assertEquals($P, 0);
        $this -> assertEquals($S, 0);
    }
//   for N < 0
    public function testDigitNTest3(){
        TrianglePS(-5, $P, $S);
        $this -> assertEquals($P, -15);
        $this -> assertEquals($S, 10.825);
        //$this -> assertEquals(TrianglePS(-6, $P, $S), array(-18, 15.588));
    }
}