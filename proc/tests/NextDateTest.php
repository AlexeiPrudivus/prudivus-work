<?php
/* Используя функцию MonthDays из задания Proc53, описать процедуру NextDate(D, M, Y), которая по информации о правильной дате,
    включающей день D, номер месяца M и год Y, определяет следующую дату (параметры целого типа D, M, Y являются одновременно входными и выходными).
 */
require_once("../Prudivus_Proc55.php");
class NextDateTest extends PHPUnit_Framework_TestCase {
//   same year
    public function testDigitNTest1(){
        $this -> assertEquals(NextDate(31,4, 2005), array(1,5,2005));
        $this -> assertEquals(NextDate(24,5, 2005), array(25,5,2005));
    }
//   changing years
    public function testDigitNTest2(){
        $this -> assertEquals(NextDate(31, 12, 2004), array(1,1,2005));
        $this -> assertEquals(NextDate(31, 12, 2000), array(1,1,2001));
    }
}