<?php
/* Описать процедуру PowerA234(A, B, C, D), вычисляющую вторую, третью и четвертую степень числа A и возвращающую эти степени соответственно в
   переменных B, C и D (A — входной, B, C, D — выходные параметры; все параметры являются вещественными).
 */
require_once("../Prudivus_Proc8_void_proc.php");
class AddRightDigitTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $K = 6345;
        AddRightDigit(1, $K);
        $this -> assertEquals($K, 63451);

        $K = 1009017;
        AddRightDigit(9, $K);
        $this -> assertEquals($K, 10090179);
    }
//   for N 0 0
    public function testDigitNTest2(){
        $K = 0;
        AddRightDigit(0, $K);
        $this -> assertEquals($K, 0);
    }
//   for N1=0 N2<>0
    public function testDigitNTest3(){
        $K = 0;
        AddRightDigit(4, $K);
        $this -> assertEquals($K, 4);
    }
}