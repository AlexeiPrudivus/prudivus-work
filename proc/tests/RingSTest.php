<?php
/* Описать функцию CircleS(R) вещественного типа, находящую площадь круга
      радиуса R (R — вещественное).  Площадь круга радиуса R вычисляется
         по формуле S = π·R2. В качестве значения π использовать 3.14.
 */
require_once("../Prudivus_Proc19.php");
class RingSTest extends PHPUnit_Framework_TestCase {
//   for N > 0
    public function testDigitNTest1(){
        $this -> assertEquals(RingS(2, 1), 9.42);
        $this -> assertEquals(RingS(5, 4), 28.26);
        $this -> assertEquals(RingS(9, 5), 175.84);
    }
}