<?php
/* Используя функцию Leng из задания Proc56, описать функцию Perim(xA, yA, xB, yB, xC, yC) вещественного типа,
    находящую периметр треугольника ABC по координатам его вершин (xA, yA, xB, yB, xC, yC — вещественные параметры).
 */
require_once("../Prudivus_Proc57.php");
class PerimTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $this -> assertEquals(Perim(9, -6, 1, 2, -4, -4), 32.27);
        $this -> assertEquals(Perim(2, 10, -7, 6, 7, -3), 40.42);
        $this -> assertEquals(Perim(1, 0, 0, 1, 1, 1), 3.41);
        $this -> assertEquals(Perim(-1, 0, 0, -1, 1, 1), 5.89);
    }
}