<?php
/*  Описать функцию Power2(A, N) вещественного типа, находящую величину AN
         (A — вещественный, N — целый параметр) по следующим формулам:
          A0 = 1;    AN = A·A·...·A  (N сомножителей), если N > 0;
          AN = 1/(A·A·...·A)  (|N| сомножителей), если N < 0.
*/

require_once("../Prudivus_Proc38.php");
class Power2Test extends PHPUnit_Framework_TestCase {
//    length of A > 0
    public function testDigitNTest1(){
        $this -> assertEquals(Power2(10,2),100);
        $this -> assertEquals(Power2(5,2),25);
        $this -> assertEquals(Power2(3,3),27);
    }
//    length of A <= 0
    public function testDigitNTest2(){
        $this -> assertEquals(Power2(0,2),0);
        $this -> assertEquals(Power2(-5,2),0);
    }
}