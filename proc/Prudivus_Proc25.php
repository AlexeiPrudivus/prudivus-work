<?php
/*  Описать функцию IsSquare(K) логического типа, возвращающую true,
    если целый параметр K (>0) является квадратом некоторого целого числа,
      и false в противном случае. С ее помощью найти количество квадратов
                   в наборе из 10 целых положительных чисел.
 */
function IsSquare($K)
{
    $b = sqrt($K) - floor(sqrt($K)); //находим дробную часть

    if (round($b, 2) == 0.00) {
        return true;
    } else {
        return false;
    }
}
/*
    $M = array();

    for ($i = 0; $i < 10; $i++) {
        $M[$i] = rand (1,1000);
        echo $M[$i]." ";
    }

    echo "\n";
    $s = 0;

    for ($i = 0; $i < count($M); $i++) {
        if (IsSquare($M[$i]) == true) {
            $s++;
        }
    }
    echo $s;*/
?>