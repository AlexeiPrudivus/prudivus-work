<?php
/*  Описать функцию SumRange(A, B) целого типа, находящую сумму всех целых чисел
 от A до B включительно (A и B — целые). Если A > B, то функция возвращает 0.
        С помощью этой функции найти суммы чисел от A до B и от B до C,
                           если даны числа A, B, C.
 */
function SumRange($A, $B)
{
    $s = 0;

    if ($A > $B) {
        return 0;
    } else
        for ($i = $A; $i <= $B; $i++) {
            $s+=$i;
            //echo $s." ";
        }

    return $s;
}
/*
    $a = rand (0,10); echo $a." ";
    $b = rand (0,10); echo $b." ";
    $c = rand (0,10); echo $c." \n";

    SumRange($a, $b); echo "\n";

    SumRange($b, $c); echo "\n";
*/
?>