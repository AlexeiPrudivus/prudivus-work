<?php
/*  Описать функцию IsPrime(N) логического типа, возвращающую true, если целый
  параметр N (>1) является простым числом, и false в противном случае (число,
   большее 1, называется простым, если оно не имеет положительных делителей,
        кроме 1 и самого себя). Дан набор из 10 целых чисел, больших 1.
   С помощью функции IsPrime найти количество простых чисел в данном наборе.
 */
function IsPrime($N) {
    if ($N == 2 || $N == 3 || $N == 5 || $N == 7) {
        //echo "prime";
        return true;
    }

    if($N % 2 == 0) {
        //echo "prime";
        return false;
    }

    for ($i = 3; $i < $N; $i = $i + 2) {
        if (round($N % $i, 2) == 0.00) {
            //echo $i . " false\n";
            return false;
        }
    }

    return true;
}
/*
    $M = array();
    $s =0;

    for ($i = 0; $i < 10; $i++) {
        $M[$i] = rand(1, 100);
        echo $M[$i]." ";

        if (IsPrime($M[$i]) == true) {
            $s++;
        }
    }

    echo "\n".$s;*/
?>