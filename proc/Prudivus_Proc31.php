<?php
/*  Описать функцию IsPalindrome(K), возвращающую true,
                если целый параметр K (>0) является палиндромом
      (т.е. его запись читается одинаково слева направо и справа налево),
     и false в противном случае. С ее помощью найти количество палиндромов
                   в наборе из 10 целых положительных чисел.
 */
function IsPalindrome($K) {
    if ($K == strrev($K)) {
            //echo 'true';
            return true;
        }
    else {
        //echo 'false';
        return false;
    }
}
/*
    $M = array();
    $s = 0;

    for ($i = 0; $i < 10; $i++) {
        $M[$i] = rand (1, 100000);
        echo $M[$i]." ";
        if (IsPalindrome($M[$i]) == true) {
            $s++;
        }
    }

    echo "\n".$s;*/
?>