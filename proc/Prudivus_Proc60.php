<?php
/*  Используя функцию Dist из задания Proc59, описать процедуру Altitudes(xA, yA, xB, yB, xC, yC, hA, hB, hC),
    находящую высоты hA, hB, hC треугольника ABC (выходные параметры),
    проведенные соответственно из вершин A, B, C (их координаты являются входными параметрами).
    С помощью этой процедуры найти высоты треугольников ABC, ABD, ACD, если даны координаты точек A, B, C, D.
*/
require_once("Prudivus_Proc59.php");

function Altitudes($xA, $yA, $xB, $yB, $xC, $yC, &$hA, &$hB, &$hC) {
    $hA = Dist($xA, $yA, $xB, $yB, $xC, $yC);
    $hB = Dist($xB, $yB, $xC, $yC, $xA, $yA);
    $hC = Dist($xC, $yC, $xA, $yA, $xB, $yB);

    return array($hA, $hB, $hC);
}
/*
    $xA= rand(-10,10);
    $yA= rand(-10,10);
    $xB= rand(-10,10);
    $yB= rand(-10,10);
    $xC= rand(-10,10);
    $yC= rand(-10,10);

    echo "xA=".$xA." yA=".$yA."\n";
    echo "xB=".$xB." yB=".$yB."\n";
    echo "xC=".$xC." yC=".$yC."\n";

    $M = array();
    $M = Altitudes($xA, $yA, $xB, $yB, $xC, $yC, $hA, $hB, $hC);
    echo "hA=".$M[0]." hb=".$M[1]." hb=".$M[2];*/
?>