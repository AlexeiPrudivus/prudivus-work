<?php
/*  Описать функцию Leng(xA, yA, xB, yB) вещественного типа, находящую длину отрезка AB на плоскости по координатам его концов:
    |AB| = ((xA − xB)2 + (yA − yB)2)1/2 (xA, yA, xB, yB — вещественные параметры).
    С помощью этой функции найти длины отрезков AB, AC, AD, если даны координаты точек A, B, C, D.
*/

function Leng($xA, $yA, $xB, $yB) {
    return round(abs(pow((pow(($xA - $xB),2) + pow(($yA - $yB),2)), 1/2)), 2);
}
/*
    $xA= rand(-100,100);
    $yA= rand(-100,100);
    $xB= rand(-100,100);
    $yB= rand(-100,100);

    echo "xA=".$xA." yA=".$yA."\n";
    echo "xB=".$xB." yB=".$yB."\n";

    echo Leng($xA, $yA, $xB, $yB);*/
?>