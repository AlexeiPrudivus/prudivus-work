<?php
/*
Write a procedure SortCols(A, M, N) that rearrange columns of an M × N matrix A of integers in ascending lexicographic order
(that is, for comparison of two columns their first distinct elements with the equal order numbers must be compared).
Two-dimensional array A is an input and output parameter. Using this procedure, sort columns of a given M × N matrix A.
*/

function SortCols(&$A, $M, $N)
{
    for ($u = 1; $u < $N; $u++) {
        for ($i = $M; $i > 1; $i--) {
            for ($j = $N; $j >= 1; $j--) {
                if ($A[$i][$j] < $A[$i - 1][$j]) {

                    $Z = $A[$i - 1][$j];
                    $A[$i - 1][$j] = $A[$i][$j];
                    $A[$i][$j] = $Z;
                }
            }
        }
    }

    for ($i = 1; $i <= $M; $i++) {
        for ($j = 1; $j <= $N; $j++) {
            echo $A[$i][$j]." ";
        }
        echo "\n";
    }
}

$A = array(     1 => array(1 => 3,1,2,0),
                2 => array(1 => 1,0,3,5),
                3 => array(1 => 2,3,0,5),
                4 => array(1 => 3,0,5,6),
);

$M = 4;
$N = 4;

SortCols($A, $M, $N);
?>