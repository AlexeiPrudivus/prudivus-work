<?php
/*
Write a procedure Transp(A, M) that transposes a real-valued square matrix A of order M
(that is, reflects its elements about the main diagonal).
The matrix A is an input and output parameter.
Using this procedure, transpose the given matrix A of order M.
*/

function Transp(&$A, $M)
{
    $K = array();

    for ($i = 1; $i <= $M; $i++) {
            for ($j = 1; $j <= $M; $j++) {
                $K[$i][$j] = $A[$j][$i];
            }
    }
/*
    for ($i = 1; $i <= $M; $i++) {
            for ($j = 1; $j <= $M; $j++) {
                echo $K[$i][$j]." ";
            }
        echo "\n";
    }*/

    $A = $K;
}
/*
$A = array(     1 => array(1 => 1,2,3),
                2 => array(1 => 4,5,6),
                3 => array(1 => 7,8,9));

Transp($A, 3);*/
?>