<?php
/*
Write a procedure Split1(A, NA, B, NB, C, NC) that copies elements of an array A of NA real numbers
to arrays B and C so that the array B contains all elements of the array A with odd order numbers (1, 3, …)
and the array C contains all elements of the array A with even order numbers (2, 4, …).
The arrays B, C and their sizes NB, NC are output parameters.
Apply this procedure to a given array A of size NA and output the size and the elements for each of the resulting arrays B and C.
 */

function Split1($A, $NA, &$B, &$NB, &$C, &$NC)
{
    if ($NA > count($A)) {
        $NA = count($A);
    }

    $NB = 0;
    $NC = 0;

    for ($i = 1; $i <= $NA; $i++) {
        if ($A[$i] % 2 == 0) {
            $NC++;
            $C[$NC] = $A[$i];
           // echo "нечётное " . $A[$i] . " ";
        } else {
            $NB++;
            $B[$NB] = $A[$i];
            //echo "чётное " . $A[$i] . " ";
        }
    }
/*
    echo "\n";
    for($i = 1; $i <= count($B); $i++) {
        echo $B[$i]." ";
    }
    echo "\n";
    for($i = 1; $i <= count($C); $i++) {
        echo $C[$i]." ";
    }*/
}
?>