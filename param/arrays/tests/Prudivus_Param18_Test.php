<?php
/*
Write a procedure Chessboard(M, N, A) that creates an M × N matrix A whose elements are integers 0 and 1,
which are arranged in "chessboard" order, and A1,1 = 0. Two-dimensional array A is an output parameter.
Having input integers M, N and using this procedure, create an M × N matrix A.
 */
require_once("../Prudivus_Param18.php");
class ChessboardTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $M = 4;
        $N = 4;
        Chessboard($M, $N, $A);
        $this->assertEquals($A, array(  1 => array(1 => 0,1,0,1),
                                        2 => array(1 => 1,0,1,0),
                                        3 => array(1 => 0,1,0,1),
                                        4 => array(1 => 1,0,1,0),
                            ));
    }
    public function testDigitNTest2(){
        $M = 3;
        $N = 3;
        Chessboard($M, $N, $A);
        $this->assertEquals($A, array(  1 => array(1 => 0,1,0),
                                        2 => array(1 => 1,0,1),
                                        3 => array(1 => 0,1,0),
                            ));
    }
}