<?php
/*
Write a procedure Split2(A, NA, B, NB, C, NC) that copies elements of an array A of NA integers to arrays B and C
so that the array B contains all elements whose values are even numbers and the array C contains all elements
whose values are odd numbers (in the same order). The arrays B, C and their sizes NB, NC are output parameters.
 */
require_once("../Prudivus_Param15.php");
class Split2Test extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $A = array(1 => 5, 9, 4, 7, 6, 2, 1, 8, 3);
        $NA = 15;
        Split2($A, $NA, $B, $NB, $C, $NC);
        $this->assertEquals($C, array(1 => 5, 9, 7, 1, 3));
        $this->assertEquals($NC, 5);
        $this->assertEquals($B, array(1 => 4, 6, 2, 8));
        $this->assertEquals($NB, 4);
    }
//  N = count($A)
        public function testDigitNTest2(){
        $A = array(1 => 5);
        $NA = 1;
        Split2($A, $NA, $B, $NB, $C, $NC);
        $this -> assertEquals($C, array(1 => 5));
        $this -> assertEquals($NC, 1);
        $this -> assertEquals($B, null);
        $this -> assertEquals($NB, 0);
    }
}