<?php
/*
Write a real-valued function SumCol(A, M, N, K) that returns the sum of elements in K-th column of an M × N matrix A of real numbers
(if K is out of the range 1 to N then the function returns 0).
Output the return value of SumCol(A, M, N, K) for a given M × N matrix A and three given integers K.
 */
require_once("../Prudivus_Param22.php");
class SumColTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 0,1,9),
                        2 => array(1 => 1,3,1),
                        3 => array(1 => 0,1,4));
        $M = 3;
        $N = 3;
        $K = 3;
        $this->assertEquals(SumCol($A, $M, $N, $K), 14);
    }
    public function testDigitNTest2(){
        $A = array(     1 => array(1 => 0,10,9),
                        2 => array(1 => 1,0,1),
                        3 => array(1 => 0,1,2));
        $M = 3;
        $N = 3;
        $K = 2;
        $this->assertEquals(SumCol($A, $M, $N, $K), 11);
    }
//       K < 0 || K > M
    public function testDigitNTest3(){
        $A = array(     1 => array(1 => 0,10,9),
            2 => array(1 => 1,0,1),
            3 => array(1 => 0,1,2));
        $M = 3;
        $N = 3;
        $K = -5;
        $this->assertEquals(SumCol($A, $M, $N, $K), 0);

        $K = 999;
        $this->assertEquals(SumCol($A, $M, $N, $K), 0);
    }

}