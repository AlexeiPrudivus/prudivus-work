<?php
/*
Write a procedure RemoveRows(A, M, N, K1, K2) that removes rows with the order numbers
in the range K1 to K2 from an M × N matrix A of real numbers
(integers K1 and K2 are assumed to satisfy the double inequality 1 < K1 ≤ K2).
If K1 > M then the matrix remains unchanged, if K2 > M then rows with numbers from K1 to M must be removed.
Two-dimensional array A and integers M, N are input and output parameters.
 */
require_once("../Prudivus_Param26.php");
class RemoveRowsTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 1,2,3),
                        2 => array(1 => 4,5,6),
                        3 => array(1 => 7,8,9));
        $M = 3;
        $N = 3;
        $K1 = 1;
        $K2 = 1;
        RemoveRows($A, $M, $N, $K1, $K2);

        $this->assertEquals($M, 2);
        $this->assertEquals($N, 3);
        $this->assertEquals($A, array(  1 => array(1 => 4,5,6),
                                        2 => array(1 => 7,8,9)));
    }
    public function testDigitNTest2(){
        $A = array(     1 => array(1 => 1,2,3,4),
                        2 => array(1 => 5,6,7,8),
                        3 => array(1 => 9,0,1,2),
                        4 => array(1 => 3,4,5,6));
        $M = 4;
        $N = 4;
        $K1 = 3;
        $K2 = 9;
        RemoveRows($A, $M, $N, $K1, $K2);

        $this->assertEquals($M, 2);
        $this->assertEquals($N, 4);
        $this->assertEquals($A, array(  1 => array(1 => 1,2,3,4),
                                        2 => array(1 => 5,6,7,8)));
    }
}