<?php
/*
Write a real-valued function SumRow(A, M, N, K) that returns the sum of elements in K-th row of an M × N matrix A of real numbers
(if K is out of the range 1 to M then the function returns 0).
Output the return value of SumRow(A, M, N, K) for a given M × N matrix A and three given integers K.
 */
require_once("../Prudivus_Param21.php");
class SumRowTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 0,1,9),
                        2 => array(1 => 1,3,1),
                        3 => array(1 => 0,1,4));
        $M = 3;
        $N = 3;
        $K = 3;
        $this->assertEquals(SumRow($A, $M, $N, $K), 5);
    }
    public function testDigitNTest2(){
        $A = array(     1 => array(1 => 0,10,9),
                        2 => array(1 => 1,0,1),
                        3 => array(1 => 0,1,2));
        $M = 3;
        $N = 3;
        $K = 2;
        $this->assertEquals(SumRow($A, $M, $N, $K), 2);
    }
//       K < 0 || K > M
    public function testDigitNTest3(){
        $A = array(     1 => array(1 => 0,10,9),
            2 => array(1 => 1,0,1),
            3 => array(1 => 0,1,2));
        $M = 3;
        $N = 3;
        $K = -5;
        $this->assertEquals(SumRow($A, $M, $N, $K), 0);

        $K = 999;
        $this->assertEquals(SumRow($A, $M, $N, $K), 0);
    }

}