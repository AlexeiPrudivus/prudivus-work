<?php
/*
Write a procedure SortCols(A, M, N) that rearrange columns of an M × N matrix A of integers in ascending lexicographic order
(that is, for comparison of two columns their first distinct elements with the equal order numbers must be compared).
Two-dimensional array A is an input and output parameter.
 */
require_once("../Prudivus_Param29_v2.php");
class SortColsTest extends PHPUnit_Framework_TestCase {
//  L < N && K < M
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 2,2,2),
                        2 => array(1 => 9,5,5),
                        3 => array(1 => 7,2,1));
        $M = 3;
        $N = 3;
        SortCols($A, $M, $N);

        $this->assertEquals($A, array(  1 => array(1 => 2,2,2),
                                        2 => array(1 => 5,5,9),
                                        3 => array(1 => 1,2,7)));

        $A = array(     1 => array(1 => 3,1,2,1),
                        2 => array(1 => 1,0,3,0),
                        3 => array(1 => 2,3,0,5),
                        4 => array(1 => 3,0,5,6));
        $M = 4;
        $N = 4;
        SortCols($A, $M, $N);

        $this->assertEquals($A, array(  1 => array(1 => 1,1,2,3),
                                        2 => array(1 => 0,0,3,1),
                                        3 => array(1 => 3,5,0,2),
                                        4 => array(1 => 0,6,5,3)));
    }
}