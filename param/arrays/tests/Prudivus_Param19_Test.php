<?php
/*
Write a real-valued function Norm1(A, M, N) that computes the norm of an M × N matrix A of real numbers using the formula
Norm1(A, M, N) = max {|A1,J| + |A2,J| + … + |AM,J|},
where the maximum is being found over J = 1, …, N. Having input an M × N matrix A, output Norm1(A, K, N), K = 1, …, M.
 */
require_once("../Prudivus_Param19.php");
class Norm1Test extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 0,1,9),
                        2 => array(1 => 1,3,1),
                        3 => array(1 => 0,1,4));
        $M = 3;
        $N = 3;
        $this->assertEquals(Norm1($A, $M, $N), 10);
    }
    public function testDigitNTest2(){
        $A = array(     1 => array(1 => 0,10,9),
                        2 => array(1 => 1,0,1),
                        3 => array(1 => 0,1,2));
        $M = 3;
        $N = 3;
        $this->assertEquals(Norm1($A, $M, $N), 19);
    }
}