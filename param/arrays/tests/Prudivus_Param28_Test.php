<?php
/*
Write a procedure RemoveRowCol(A, M, N, K, L) that removes K-th row and L-th column simultaneously from an M × N matrix A
of real numbers (integers K and L are assumed to satisfy the inequalities M > 1, N > 1).
If K > M or L > N then the matrix remains unchanged.
Two-dimensional array A and integers M, N are input and output parameters.
 */
require_once("../Prudivus_Param28.php");
class RemoveRowColTest extends PHPUnit_Framework_TestCase {
//  L < N && K < M
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 1,2,3),
                        2 => array(1 => 4,5,6),
                        3 => array(1 => 7,8,9));
        $M = 3;
        $N = 3;
        $K = 1;
        $L = 1;
        RemoveRowCol($A, $M, $N, $K, $L);

        $this->assertEquals($M, 2);
        $this->assertEquals($N, 2);
        $this->assertEquals($A, array(  1 => array(1 => 5,6),
                                        2 => array(1 => 8,9)));
    }

//  L > N || K > M
    public function testDigitNTest2(){
        $A = array(     1 => array(1 => 1,2,3,4),
                        2 => array(1 => 5,6,7,8),
                        3 => array(1 => 9,0,1,2),
                        4 => array(1 => 3,4,5,6));
        $M = 4;
        $N = 4;
        $K = 10;
        $L = 9;
        RemoveRowCol($A, $M, $N, $K, $L);

        $this->assertEquals($M, 4);
        $this->assertEquals($N, 4);
        $this->assertEquals($A, array(  1 => array(1 => 1,2,3,4),
                                        2 => array(1 => 5,6,7,8),
                                        3 => array(1 => 9,0,1,2),
                                        4 => array(1 => 3,4,5,6)));
    }
}