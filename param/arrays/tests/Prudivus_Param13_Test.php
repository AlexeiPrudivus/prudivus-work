<?php
/*
Write a procedure Hill(A, N) that changes order of elements of an array A of N real numbers as follows:
the minimal element of the array must be the first one, an element, whose value is the next to minimal value,
must be the last one, an element with the next value must be the second one,
and so on (as a result, the diagram of values of the array elements will be similar to a hill).
The array A is an input and output parameter.
 */
require_once("../Prudivus_Param13.php");
class HillTest extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $A = array(1 => 1,0,2);
        $N = 15;
        Hill($A, $N);
        $this -> assertEquals($A, array(1 => 0,2,1));
    }
//  N = count($A)
    public function testDigitNTest2(){
        $A = array(1 => 5,7,6,3,10);
        $N = 8;
        Hill($A, $N);
        $this -> assertEquals($A, array(1 => 3,6,10,7,5));
    }
//  N = count($A)
    public function testDigitNTest3(){
        $A = array(1 => 5,7,6,3);
        $N = 8;
        Hill($A, $N);
        $this -> assertEquals($A, array(1 => 3,6,7,5));
    }
}