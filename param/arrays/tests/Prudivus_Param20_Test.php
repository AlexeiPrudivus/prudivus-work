<?php
/*
Write a real-valued function Norm2(A, M, N) that computes the norm of an M × N matrix A of real numbers using the formula
Norm2(A, M, N) = max {|AI,1| + |AI,2| + … + |AI,N|},
where the maximum is being found over I = 1, …, M. Having input an M × N matrix A, output Norm2(A, K, N), K = 1, …, M.
 */
require_once("../Prudivus_Param20.php");
class Norm2Test extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 0,1,9),
                        2 => array(1 => 1,3,1),
                        3 => array(1 => 0,1,4));
        $M = 3;
        $N = 3;
        $this->assertEquals(Norm2($A, $M, $N), 14);
    }
    public function testDigitNTest2(){
        $A = array(     1 => array(1 => 0,10,9),
                        2 => array(1 => 1,0,1),
                        3 => array(1 => 0,1,2));
        $M = 3;
        $N = 3;
        $this->assertEquals(Norm2($A, $M, $N), 12);
    }
}