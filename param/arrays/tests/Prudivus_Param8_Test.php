<?php
/*
Write a procedure RemoveX(A, N, X) that removes all elements equal an integer X from an array A of N integers.
The array A and its size N are input and output parameters. Using this procedure,
 */
require_once("../Prudivus_Param8.php");
class RemoveXTest extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $A = array(1 => 1,0,2,0,3,0,4,0,5,0,0,0);
        $N = 15;
        $X = 0;
        RemoveX($A, $N, $X);
        $this -> assertEquals($A, array(1 => 1,2,3,4,5));
    }
//  N = count($A)
    public function testDigitNTest2(){
        $A = array(1 => 7,8,4,3,8,9);
        $N = 6;
        $X = 8;
        RemoveX($A, $N, $X);
        $this -> assertEquals($A, array(1 => 7,4,3,9));

        $A = array(1 => 1,1,1,1,1,1);
        $N = 6;
        $X = 1;
        RemoveX($A, $N, $X);
        $this -> assertEquals($A, array());
    }
//  N < count($A)
    public function testDigitNTest3(){
        $A = array(1 => 5,0,7,1,0,9,3,0);
        $N = 5;
        $X = 0;
        RemoveX($A, $N, $X);
        $this -> assertEquals($A, array(1 => 5,7,1,9,3));
    }
}