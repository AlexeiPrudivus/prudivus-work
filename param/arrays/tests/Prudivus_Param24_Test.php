<?php
/*
Write a procedure SwapCol(A, M, N, K1, K2) that exchanges K1-th and K2-th column of an M × N matrix A of real numbers.
The matrix A is an input and output parameter; if K1 or K2 are out of the range 1 to N then the matrix remains unchanged.
Having input an M × N matrix A and two integers K1, K2 and using this procedure, exchange K1-th and K2-th column of the matrix A.
 */
require_once("../Prudivus_Param24.php");
class SwapColTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 0,1,9),
                        2 => array(1 => 1,3,1),
                        3 => array(1 => 0,1,4));
        $M = 3;
        $N = 3;
        $K1 = 1;
        $K2 = 3;
        SwapCol($A, $M, $N, $K1, $K2);
        $this->assertEquals($A, array(  1 => array(1 => 9,1,0),
                                        2 => array(1 => 1,3,1),
                                        3 => array(1 => 4,1,0)));
    }
    public function testDigitNTest2(){
        $A = array(     1 => array(1 => 1,2,3),
                        2 => array(1 => 1,2,3),
                        3 => array(1 => 1,2,3));
        $M = 3;
        $N = 3;
        $K1 = 1;
        $K2 = 2;
        SwapCol($A, $M, $N, $K1, $K2);
        $this->assertEquals($A, array(  1 => array(1 => 2,1,3),
                                        2 => array(1 => 2,1,3),
                                        3 => array(1 => 2,1,3)));
    }
}