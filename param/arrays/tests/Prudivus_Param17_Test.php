<?php
/*
Write a procedure ArrayToMatrCol(A, K, M, N, B) that copies elements of an array A of K real numbers to an M × N matrix B (by columns).
"Superfluous" array elements must be ignored;
if the size of the array is less than the amount of matrix elements then zero value must be assigned to remaining matrix elements.
Two-dimensional array B is an output parameter.
Having input an array A of size K, integers M, N and using this procedure, create a matrix B and output its elements.
 */
require_once("../Prudivus_Param17.php");
class ArrayToMatrColTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $A = array(1 => 1,2,3,4,5,6,7,8,9,0,1,2,3,4,5);
        $K = 15;
        $M = 4;
        $N = 4;
        //$B = array();
        ArrayToMatrCol($A, $K, $M, $N, $B);
        $this->assertEquals($B, array(  1 => array(1 => 1,5,9,3),
                                        2 => array(1 => 2,6,0,4),
                                        3 => array(1 => 3,7,1,5),
                                        4 => array(1 => 4,8,2,0),
                            ));
    }
    public function testDigitNTest2(){
        $A = array(1 => 1,2,3,4,5,6,7);
        $K = 7;
        $M = 3;
        $N = 3;
        //$B = array();
        ArrayToMatrCol($A, $K, $M, $N, $B);
        $this->assertEquals($B, array(  1 => array(1 => 1,4,7),
                                        2 => array(1 => 2,5,0),
                                        3 => array(1 => 3,6,0),
                            ));
    }
}