<?php
/*
Write a procedure DoubleX(A, N, X) that doubles occurrences of all elements equal an integer X for an array A of N integers.
The array A and its size N are input and output parameters.
 */
require_once("../Prudivus_Param10.php");
class DoubleXTest extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $A = array(1 => 1,0,2,0,3,0,4,0,5,0,0,0);
        $N = 15;
        $X = 0;
        DoubleX($A, $N, $X);
        $this -> assertEquals($A, array(1 => 1,0,0,2,0,0,3,0,0,4,0,0,5,0,0,0,0,0,0));
        $this -> assertEquals($A, 19);
    }
//  N = count($A)
    public function testDigitNTest2(){
        $A = array(1 => 7,8,4,3,8,9);
        $N = 6;
        $X = 7;
        DoubleX($A, $N, $X);
        $this -> assertEquals($A, array(1 => 7,7,8,4,3,8,9));

        $A = array(1 => 1,1,1,1,1,1);
        $N = 6;
        $X = 1;
        DoubleX($A, $N, $X);
        $this -> assertEquals($A, array(1 => 1,1,1,1,1,1,1,1,1,1,1,1));
    }
//  N < count($A)
    public function testDigitNTest3(){
        $A = array(1 => 5,0,7,1,0,9,3,0);
        $N = 5;
        $X = 0;
        DoubleX($A, $N, $X);
        $this -> assertEquals($A, array(1 => 5,0,0,7,1,0,0));
    }
//  X not present in array
    public function testDigitNTest4(){
        $A = array(1 => 5,6,7,8,9);
        $N = 5;
        $X = 0;
        DoubleX($A, $N, $X);
        $this -> assertEquals($A, array(1 => 5,6,7,8,9));
    }
}