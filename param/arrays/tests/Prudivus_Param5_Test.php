<?php
/*
Описать процедуру Smooth1(A, N), выполняющую сглаживание вещественного массива A размера N следующим образом:
элемент AK заменяется на среднее арифметическое первых K исходных элементов массива A.
Массив A является входным и выходным параметром.
 */
require_once("../Prudivus_Param5.php");
class Smooth1Test extends PHPUnit_Framework_TestCase {
//  N = count($A)
    public function testDigitNTest1(){
        $A = array(1 => 7, 1 ,2, 4, 1);
        $N = 5;
        Smooth1($A, $N);
        $this -> assertEquals($A, array(1 => 7, 4, 3.33, 3.5, 3));
    }
//  N > count($A)
    public function testDigitNTest2(){
        $A = array(1 => 1,1,1,1,1);
        $N = 7;
        Smooth1($A, $N);
        $this -> assertEquals($A, array(1 => 1, 1, 1, 1, 1));
    }
//  N < count($A)
    public function testDigitNTest3(){
        $A = array(1 => 1,1,1,1,1);
        $N = 3;
        Smooth1($A, $N);
        $this -> assertEquals($A, array(1 => 1, 1, 1));
    }
}