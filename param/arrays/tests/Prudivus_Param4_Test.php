<?php
/*
Описать процедуру Inv(A, N), меняющую порядок следования элементов вещественного массива A размера N на обратный (инвертирование массива).
Массив A является входным и выходным параметром.
 */
require_once("../Prudivus_Param4.php");
class InvTest extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $A = array(1=> 1,2,3,4,5);
        $N = 60;
        Inv($A, $N);
        $this -> assertEquals($A, array(1 => 5,4,3,2,1));
    }
//  N = count($A)
    public function testDigitNTest2(){
        $A = array(1=> 1,2,3,4,5);
        $N = 5;
        Inv($A, $N);
        $this -> assertEquals($A, array(1 => 5,4,3,2,1));
    }
//  N < count($A)
    public function testDigitNTest3(){
        $A = array(1=> 1,2,3,4,5);
        $N = 4;
        Inv($A, $N);
        $this -> assertEquals($A, array(1 => 4,3,2,1));
    }
}