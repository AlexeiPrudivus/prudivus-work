<?php
/*
Write a procedure Transp(A, M) that transposes a real-valued square matrix A of order M
(that is, reflects its elements about the main diagonal).
The matrix A is an input and output parameter.
 */
require_once("../Prudivus_Param25.php");
class TranspTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 1,2,3),
                        2 => array(1 => 4,5,6),
                        3 => array(1 => 7,8,9));
        $M = 3;
        Transp($A, $M);
        $this->assertEquals($A, array(  1 => array(1 => 1,4,7),
                                        2 => array(1 => 2,5,8),
                                        3 => array(1 => 3,6,9)));
    }
    public function testDigitNTest2(){
        $A = array(     1 => array(1 => 1,2,3),
                        2 => array(1 => 1,2,3),
                        3 => array(1 => 1,2,3));
        $M = 3;
        Transp($A, $M);
        $this->assertEquals($A, array(  1 => array(1 => 1,1,1),
                                        2 => array(1 => 2,2,2),
                                        3 => array(1 => 3,3,3)));
    }
}