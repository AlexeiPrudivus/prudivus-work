<?php
/* Описать функцию MinElem(A, N) целого типа, находящую минимальный элемент целочисленного массива A размера N.
    С помощью этой функции найти минимальные элементы массивов A, B, C размера NA, NB, NC соответственно.
 */
require_once("../Prudivus_Param1.php");
class MinElemTest extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $this -> assertEquals(MinElem(array(1 => 0,7,5), 7), 0);
    }
//  N = count($A)
    public function testDigitNTest2(){
        $this -> assertEquals(MinElem(array(1 => 4,2,1,4,5,9), 6), 1);
    }
//  N < count($A)
    public function testDigitNTest3(){
        $this -> assertEquals(MinElem(array(1 => 4,7,2,5), 2), 4);
    }
}