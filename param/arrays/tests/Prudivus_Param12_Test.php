<?php
/*
Write a procedure SortArray(A, N) that sorts an array A of N real numbers in ascending order.
The array A is an input and output parameter.
 */
require_once("../Prudivus_Param12.php");
class SortIndexTest extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $A = array(1 => 1,0,2);
        $N = 15;
        SortIndex($A, $N, $I);
        $this -> assertEquals($I, array(1 => 2,1,3));
    }
//  N = count($A)
    public function testDigitNTest2(){
        $A = array(1 => 5,0,7,1,0,9,3,0);
        $N = 8;
        SortIndex($A, $N, $I);
        $this -> assertEquals($I, array(1 => 6, 1, 7, 4, 1, 8, 5, 1));
    }
}