<?php
/*
Write a procedure SortArray(A, N) that sorts an array A of N real numbers in ascending order.
The array A is an input and output parameter.
 */
require_once("../Prudivus_Param11.php");
class SortArrayTest extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $A = array(1 => 1,0,2,0,3,0,4,0,5,0,0,0);
        $N = 15;
        SortArray($A, $N);
        $this -> assertEquals($A, array(1 => 0,0,0,0,0,0,0,1,2,3,4,5));
    }
//  N = count($A)
    public function testDigitNTest2(){
        $A = array(1 => 7,8,4,3,8,9);
        $N = 6;
        SortArray($A, $N);
        $this -> assertEquals($A, array(1 => 3,4,7,8,8,9));

        $A = array(1 => 1,1,1,1,1,1);
        $N = 6;
        SortArray($A, $N);
        $this -> assertEquals($A, array(1 =>1,1,1,1,1,1));
    }
//  N < count($A)
    public function testDigitNTest3(){
        $A = array(1 => 5,0,7,1,0,9,3,0);
        $N = 5;
        SortArray($A, $N);
        $this -> assertEquals($A, array(1 => 0,0,0,1,3,5,7,9));
    }
}