<?php
/* Описать функцию MaxNum(A, N) целого типа, находящую номер максимального элемента вещественного массива A размера N.
    С помощью этой функции найти номера максимальных элементов массивов A, B, C размера NA, NB, NC соответственно.
 */
require_once("../Prudivus_Param2.php");
class MaxNumTest extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $this -> assertEquals(MaxNum(array(1 => 0,7,5), 7), 7);
    }
//  N = count($A)
    public function testDigitNTest2(){
        $this -> assertEquals(MaxNum(array(1 => 4,2,1,4,5,9), 6), 9);
    }
//  N < count($A)
    public function testDigitNTest3(){
        $this -> assertEquals(MaxNum(array(1 => 4,7,2,5), 2), 7);
    }
}