<?php
/*
Write a procedure RemoveForInc(A, N) that removes some elements from an array A of N real numbers
so that the values of elements being remained were in ascending order: the first element remains unchanged,
the second element must be removed if its value is less than the value of the first one,
the third element must be removed if its value is less than the value of the previous element being remained, and so on.
 */
require_once("../Prudivus_Param9.php");
class RemoveForIncTest extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $A = array(1 => 1,0,2,0,3,0,4,0,5,0,0,0);
        $N = 15;
        RemoveForInc($A, $N);
        $this -> assertEquals($A, array(1 => 1,2,3,4,5));
    }
//  N = count($A)
    public function testDigitNTest2(){
        $A = array(1 => 7,8,4,3,8,9);
        $N = 6;
        RemoveForInc($A, $N);
        $this -> assertEquals($A, array(1 => 7,8,9));

        $A = array(1 => 1,1,1,1,1,1);
        $N = 6;
        RemoveForInc($A, $N);
        $this -> assertEquals($A, array(1 => 1));
    }
//  N < count($A)
    public function testDigitNTest3(){
        $A = array(1 => 5,0,7,1,0,9,3,0);
        $N = 5;
        RemoveForInc($A, $N);
        $this -> assertEquals($A, array(1 => 5,7));
    }
}