<?php
/*
Write a procedure Split1(A, NA, B, NB, C, NC) that copies elements of an array A of NA real numbers
to arrays B and C so that the array B contains all elements of the array A with odd order numbers (1, 3, …)
and the array C contains all elements of the array A with even order numbers (2, 4, …).
The arrays B, C and their sizes NB, NC are output parameters.
 */
require_once("../Prudivus_Param14.php");
class Split1Test extends PHPUnit_Framework_TestCase {
//  N > count($A)
    public function testDigitNTest1(){
        $A = array(1 => 5, 9, 4, 7, 6, 2, 1, 8, 3);
        $NA = 15;
        Split1($A, $NA, $B, $NB, $C, $NC);
        $this->assertEquals($B, array(1 => 5, 9, 7, 1, 3));
        $this->assertEquals($NB, 5);
        $this->assertEquals($C, array(1 => 4, 6, 2, 8));
        $this->assertEquals($NC, 4);
    }
//  N = count($A)
        public function testDigitNTest2(){
        $A = array(1 => 5);
        $NA = 1;
        Split1($A, $NA, $B, $NB, $C, $NC);
        $this -> assertEquals($B, array(1 => 5));
        $this -> assertEquals($NB, 1);
        $this -> assertEquals($C, null);
        $this -> assertEquals($NC, 0);
    }
}