<?php
/*
Write a procedure SwapRow(A, M, N, K1, K2) that exchanges K1-th and K2-th row of an M × N matrix A of real numbers.
The matrix A is an input and output parameter; if K1 or K2 are out of the range 1 to M then the matrix remains unchanged.
Having input an M × N matrix A and two integers K1, K2 and using this procedure, exchange K1-th and K2-th row of the matrix A.
 */
require_once("../Prudivus_Param23.php");
class SwapRowTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1(){
        $A = array(     1 => array(1 => 0,1,9),
                        2 => array(1 => 1,3,1),
                        3 => array(1 => 0,1,4));
        $M = 3;
        $N = 3;
        $K1 = 1;
        $K2 = 3;
        SwapRow($A, $M, $N, $K1, $K2);
        $this->assertEquals($A, array(  1 => array(1 => 0,1,4),
                                        2 => array(1 => 1,3,1),
                                        3 => array(1 => 0,1,9)));
    }
    public function testDigitNTest2(){
        $A = array(     1 => array(1 => 1,1,1),
                        2 => array(1 => 2,2,2),
                        3 => array(1 => 3,3,3));
        $M = 3;
        $N = 3;
        $K1 = 1;
        $K2 = 2;
        SwapRow($A, $M, $N, $K1, $K2);
        $this->assertEquals($A, array(  1 => array(1 => 2,2,2),
                                        2 => array(1 => 1,1,1),
                                        3 => array(1 => 3,3,3)));
    }
}