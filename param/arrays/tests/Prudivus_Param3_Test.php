<?php
/* Описать процедуру MinmaxNum(A, N, NMin, NMax), находящую номера минимального и максимального элемента вещественного массива A размера N.
Выходные параметры целого типа: NMin (номер минимального элемента) и NMax (номер максимального элемента).
 */
require_once("../Prudivus_Param3.php");
class MinmaxNumTest extends PHPUnit_Framework_TestCase {
// N > count($A)
    public function testDigitNTest1(){
        $A = array(1 => 0,7,5);
        $N = 8;
        MinmaxNum($A, $N, $NMin, $NMax);
        $this -> assertEquals($NMin, 0);
        $this -> assertEquals($NMax, 7);

        $A = array(1 => 0,7,5,9,99);
        $N = 5;
        MinmaxNum($A, $N, $NMin, $NMax);
        $this -> assertEquals($NMin, 0);
        $this -> assertEquals($NMax, 99);
    }
// N = count($A)
    public function testDigitNTest2(){
        $A = array(1 => 0,7,5);
        $N = 3;
        MinmaxNum($A, $N, $NMin, $NMax);
        $this -> assertEquals($NMin, 0);
        $this -> assertEquals($NMax, 7);

        $A = array(1 => 0,7,5,9,99);
        $N = 5;
        MinmaxNum($A, $N, $NMin, $NMax);
        $this -> assertEquals($NMin, 0);
        $this -> assertEquals($NMax, 99);
    }
// N < count($A)
    public function testDigitNTest3(){
        $A = array(1 => 0,7,5);
        $N = 1;
        MinmaxNum($A, $N, $NMin, $NMax);
        $this -> assertEquals($NMin, 0);
        $this -> assertEquals($NMax, 0);

        $A = array(1 => 0,7,5,9,99);
        $N = 4;
        MinmaxNum($A, $N, $NMin, $NMax);
        $this -> assertEquals($NMin, 0);
        $this -> assertEquals($NMax, 9);
    }
}