<?php
/*
Write a procedure Smooth2(A, N) that performs smoothing an array A of N real numbers as follows:
an element A1 remains unchanged; elements AK (K = 2, …, N) is replaced with the average of initial values of elements AK−1 and AK.
The array A is an input and output parameter.
 */
require_once("../Prudivus_Param6.php");
class Smooth2Test extends PHPUnit_Framework_TestCase {
//  N = count($A)
    public function testDigitNTest1(){
        $A = array(1 => 1,0,1,0,1,0,1,0,1,0);
        $N = 10;
        Smooth2($A, $N);
        $this -> assertEquals($A, array(1 => 1, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5));
    }
//  N > count($A)
    public function testDigitNTest2(){
        $A = array(1 => 1,1,1,1,1);
        $N = 7;
        Smooth2($A, $N);
        $this -> assertEquals($A, array(1 => 1, 1, 1, 1, 1));
    }
//  N < count($A)
    public function testDigitNTest3(){
        $A = array(1 => 5,7,1,0,9,3);
        $N = 5;
        Smooth2($A, $N);
        $this -> assertEquals($A, array(1 => 5, 6, 4, 0.5, 4.5));
    }
}