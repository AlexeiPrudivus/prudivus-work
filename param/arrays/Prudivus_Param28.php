<?php
/*
Write a procedure RemoveRowCol(A, M, N, K, L) that removes K-th row and L-th column simultaneously from an M × N matrix A
of real numbers (integers K and L are assumed to satisfy the inequalities M > 1, N > 1).
If K > M or L > N then the matrix remains unchanged.
Two-dimensional array A and integers M, N are input and output parameters.
Having input an M × N matrix A and two integers K, L,
apply this procedure to the given matrix and output a new order and elements of the resulting matrix.
*/

function RemoveRowCol(&$A, &$M, &$N, $K, $L)
{
    if ($L <= $N && $K <= $M) {
        for ($i = 1; $i <= $M; $i++) {
            for ($j = 1; $j <= $N; $j++) {
                if ($i == $K || $j == $L) {
                    unset($A[$i][$j]);
                }
            }
        }

        $N--;
        $M--;

        if ($M != 0) {
            $A = array_filter(array_map('array_values', $A));
            $A = array_combine(range(1, $M), array_values(array_filter($A)));

            $U = array();

            for ($i = 1; $i <= $M; $i++) {
                for ($j = 0; $j < $N; $j++) {
                    $U[$i][$j + 1] = $A[$i][$j];
                }
            }

            $A = $U;
            unset($U);
        }
        /*
                for ($i = 1; $i <= $M; $i++) {
                        for ($j = 1; $j <= $N; $j++) {
                            echo $A[$i][$j]." ";
                        }
                    echo "\n";
                }*/
    }
}
/*
$A = array(     1 => array(1 => 1,2,3,4),
                2 => array(1 => 5,6,7,8),
                3 => array(1 => 9,0,1,2),
                4 => array(1 => 3,4,5,6),
);

$M = 4;
$N = 4;

RemoveRowCol($A, $M, $N, 2, 3);*/
?>