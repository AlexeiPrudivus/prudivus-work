<?php
/*
Write a procedure Hill(A, N) that changes order of elements of an array A of N real numbers as follows:
the minimal element of the array must be the first one, an element, whose value is the next to minimal value,
must be the last one, an element with the next value must be the second one,
and so on (as a result, the diagram of values of the array elements will be similar to a hill).
The array A is an input and output parameter.
Using this procedure, change three given arrays A, B, C of size NA, NB, NC respectively.
 */

function Hill(&$A, $N)
{
    if ($N > count($A)) {
        $N = count($A);
    }

    $I = array();
    $U = 0;
    $G = 2;

    for ($i = 1; $i <= $N; $i++) {
        for ($j = 1; $j <= $N; $j++) {
            if ($A[$i] < $A[$j]) {
                $Z = $A[$i];
                $A[$i] = $A[$j];
                $A[$j] = $Z;
            }
        }
    }
/*
    for($i = 1; $i <= $N; $i++) {
        echo $A[$i]." ";
    }
*/
    echo "\n";
    for($i = 1; $i <= $N; $i++) {
        $I[$i] = $A[$i];
    }

    for ($i = 2; $i <= $N; $i++) {
        if ($i % 2 == 0) {
            $I[count($I) - $U] = $A[$i];
            //echo "чётное " . $A[$i] . " ";
            $U++;
        } else {
            $I[$G] = $A[$i];
            //echo "нечётное " . $A[$i] . " ";
            $G++;
        }
    }

    $A = $I;
}
?>