<?php
/*
Write a procedure SwapRow(A, M, N, K1, K2) that exchanges K1-th and K2-th row of an M × N matrix A of real numbers.
The matrix A is an input and output parameter; if K1 or K2 are out of the range 1 to M then the matrix remains unchanged.
Having input an M × N matrix A and two integers K1, K2 and using this procedure, exchange K1-th and K2-th row of the matrix A.
*/

function SwapRow(&$A, $M, $N, $K1, $K2)
{
    for ($i = 1; $i <= $M; $i++) {
        if ($K1 == $i) {
            for ($j = 1; $j <= $N; $j++) {
                $Z = $A[$K1][$j];
                $A[$K1][$j] = $A[$K2][$j];
                $A[$K2][$j] = $Z;
            }
        }
    }
/*
    for ($i = 1; $i <= $M; $i++) {
            for ($j = 1; $j <= $N; $j++) {
                echo $A[$i][$j]." ";
            }
        echo "\n";
    }*/

}
/*
$A = array(     1 => array(1 => 6,6,6),
                2 => array(1 => 1,1,1),
                3 => array(1 => 5,5,5));

SwapRow($A, 3, 3, 1, 3);*/
?>