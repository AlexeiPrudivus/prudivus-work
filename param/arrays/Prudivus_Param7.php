<?php
/*
Write a procedure Smooth3(A, N) that performs smoothing an array A of N real numbers as follows:
each array element is replaced with the average of initial values of this element and its neighbors.
The array A is an input and output parameter. Using five calls of this procedure,
perform smoothing a given array A of N real numbers five times successively; output array elements after each smoothing.
 */

function Smooth3(&$A, $N)
{
    if ($N > count($A)) {
        $N = count($A);
    }
    $M = array(1 => 0);

    if ($N < count($A)) {
        for ($i = $N; $i <= count($A); $i++) {
            unset($A[$i + 1]);
        }
    }

    for($i = 1; $i <= count($A); $i++) {
        if ($i == 1) {
            $M[$i] = round(($A[$i] + $A[$i + 1]) / 2, 2);
        } else if ($i == count($A)) {
            $M[$i] = round(($A[$i - 1] + $A[$i]) / 2, 2);
        } else {
            $M[$i] = round(($A[$i - 1] + $A[$i] + $A[$i + 1]) / 3, 2);
        }
    }

    $A = $M;

    /*for($i = 1; $i <= count($M); $i++) {
        echo $M[$i]." ";
    }*/
}
?>