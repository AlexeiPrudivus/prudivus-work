<?php
/*
Write a procedure SwapCol(A, M, N, K1, K2) that exchanges K1-th and K2-th column of an M × N matrix A of real numbers.
The matrix A is an input and output parameter; if K1 or K2 are out of the range 1 to N then the matrix remains unchanged.
Having input an M × N matrix A and two integers K1, K2 and using this procedure, exchange K1-th and K2-th column of the matrix A.
*/

function SwapCol(&$A, $M, $N, $K1, $K2)
{
    for ($i = 1; $i <= $M; $i++) {
        if ($K1 == $i) {
            for ($j = 1; $j <= $N; $j++) {
                $Z = $A[$j][$K1];
                $A[$j][$K1] = $A[$j][$K2];
                $A[$j][$K2] = $Z;
            }
        }
    }
/*
    for ($i = 1; $i <= $M; $i++) {
            for ($j = 1; $j <= $N; $j++) {
                echo $A[$i][$j]." ";
            }
        echo "\n";
    }*/

}
/*
$A = array(     1 => array(1 => 1,5,6),
                2 => array(1 => 1,5,6),
                3 => array(1 => 1,5,6));

SwapCol($A, 3, 3, 1, 3);*/
?>