<?php
/*
Write a procedure Smooth2(A, N) that performs smoothing an array A of N real numbers as follows:
an element A1 remains unchanged; elements AK (K = 2, …, N) is replaced with the average of initial values of elements AK−1 and AK.
The array A is an input and output parameter. Using five calls of this procedure,
perform smoothing a given array A of N real numbers five times successively; output array elements after each smoothing.
 */

function Smooth2(&$A, $N)
{
    if ($N > count($A)) {
        $N = count($A);
    }
    $M = array(1 => $A[1]);

    if ($N < count($A)) {
        for ($i = $N; $i <= count($A); $i++) {
            unset($A[$i + 1]);
        }
    }

    for($i = 2; $i <= count($A); $i++) {
        $M[$i] = round(($A[$i - 1] + $A[$i]) / 2, 2);
    }

    $A = $M;

    /*for($i = 1; $i <= count($M); $i++) {
        echo $M[$i]." ";
    }*/
}
?>