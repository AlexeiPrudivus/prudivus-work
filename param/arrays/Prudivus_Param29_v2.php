<?php
/*
Write a procedure SortCols(A, M, N) that rearrange columns of an M × N matrix A of integers in ascending lexicographic order
(that is, for comparison of two columns their first distinct elements with the equal order numbers must be compared).
Two-dimensional array A is an input and output parameter. Using this procedure, sort columns of a given M × N matrix A.
*/

function SortCols(&$A, $M, $N)
{
    for ($u = 1; $u <= $N; $u++) {
        for ($i = $M; $i >= 1; $i--) {
            for ($j = $N; $j > 1; $j--) {
                if ($A[$i][$j] < $A[$i][$j - 1]) {
                    for($l = 1; $l <= $N; $l++){
                        $Z = $A[$l][$j];
                        $A[$l][$j] = $A[$l][$j - 1];
                        $A[$l][$j - 1] = $Z;
                    }
                }
            }
        }
    }

    for ($i = 1; $i <= $M; $i++) {
        for ($j = 1; $j <= $N; $j++) {
            echo $A[$i][$j]." ";
        }
        echo "\n";
    }
}

$A = array(     1 => array(1 => 9,9,1,0),
                2 => array(1 => 7,7,3,5),
                3 => array(1 => 3,3,0,5),
                4 => array(1 => 1,2,5,6),
);

$M = 4;
$N = 4;

SortCols($A, $M, $N);
?>