<?php
/*
Write a procedure DoubleX(A, N, X) that doubles occurrences of all elements equal an integer X for an array A of N integers.
The array A and its size N are input and output parameters.
Using this procedure, double occurrences of elements with given values XA, XB, XC for three given arrays A, B, C
of size NA, NB, NC respectively and output the new size and elements of each changed array.
 */

function DoubleX(&$A, &$N, $X)
{
    if ($N > count($A)) {
        $N = count($A);
    }

    $M = array();
    $U = 1;

    for ($i = 1; $i <= $N; $i++) {
        $M[$U] = $A[$i];
        $U++;

        if($A[$i] == $X) {
            $M[$U] = $A[$i];
            $U++;
        }
    }

    $A = $M;
    $N = count($A);
}
?>