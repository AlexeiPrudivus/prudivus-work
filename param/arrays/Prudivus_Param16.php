<?php
/*
Write a procedure ArrayToMatrRow(A, K, M, N, B) that copies elements of an array A of K real numbers
to an M × N matrix B (by rows). "Superfluous" array elements must be ignored;
if the size of the array is less than the amount of matrix elements then zero value must be assigned to remaining matrix elements.
Two-dimensional array B is an output parameter.
Having input an array A of size K, integers M, N and using this procedure, create a matrix B and output its elements.
 */

function ArrayToMatrRow($A, $K, $M, $N, &$B)
{
    $U = 1;

    for ($i = 1; $i <= $M; $i++)
    {
        for ($j = 1; $j <= $N; $j++) {
            if ($U <= $K) {
                $B[$i][$j] = $A[$U];
                $U++;
            }
            else {
                $B[$i][$j] = 0;
            }
            echo $B[$i][$j]." ";
        }
        echo "\n";
    }
}
/*
$A = array(1 => 1,2,3,4,5,6,7,8,9,0,1,2,3,4,5);
$K = 15;
$M = 4;
$N = 4;
$B = array(array());
ArrayToMatrRow($A, $K, $M, $N, $B);*/
?>