<?php
/*
Write a procedure RemoveX(A, N, X) that removes all elements equal an integer X from an array A of N integers.
The array A and its size N are input and output parameters. Using this procedure,
remove elements with given values XA, XB, XC from three given arrays A, B, C of size NA, NB, NC respectively
and output the new size and elements of each changed array.
 */

function RemoveX(&$A, $N, $X)
{
    if ($N > count($A)) {
        $N = count($A);
    }

    $M = array();
    $U = 1;

    for ($i = 1; $i <= count($A); $i++) {
        if ($A[$i] != $X) {
            $M[$U] = $A[$i];
            $U++;
        }
    }

    $A = $M;
}
?>