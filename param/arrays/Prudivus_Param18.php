<?php
/*
Write a procedure Chessboard(M, N, A) that creates an M × N matrix A whose elements are integers 0 and 1,
which are arranged in "chessboard" order, and A1,1 = 0. Two-dimensional array A is an output parameter.
Having input integers M, N and using this procedure, create an M × N matrix A.
 */

function Chessboard($M, $N, &$A)
{
    for ($i = 1; $i <= $M; $i++)
    {
        for ($j = 1; $j <= $N; $j++) {
            if (($i + $j) % 2 != 0) {
                $A[$i][$j] = 1;
            }
            else {
                $A[$i][$j] = 0;
            }
            //echo $A[$i][$j]." ";
        }
        //echo "\n";
    }
}

//Chessboard(10, 10, $A);
?>