<?php
/*
Write a real-valued function SumCol(A, M, N, K) that returns the sum of elements in K-th column of an M × N matrix A of real numbers
(if K is out of the range 1 to N then the function returns 0).
Output the return value of SumCol(A, M, N, K) for a given M × N matrix A and three given integers K.
*/

function SumCol($A, $M, $N, $K)
{
    $U = 0;

    if($K < 0 || $K > $M) {
        return 0;
    }

    for ($i = 1; $i <= $M; $i++) {
        if ($K == $i) {
            for ($j = 1; $j <= $N; $j++) {
                $U += $A[$j][$i];
            }
            echo $U;
        }
    }

    return $U;
}
/*
$A = array(     1 => array(1 => 0,1,9),
                2 => array(1 => 1,3,1),
                3 => array(1 => 0,1,4));

SumCol($A, 3, 3, 1);*/
?>