<?php
/*
Write a real-valued function Norm2(A, M, N) that computes the norm of an M × N matrix A of real numbers using the formula
Norm2(A, M, N) = max {|AI,1| + |AI,2| + … + |AI,N|},
where the maximum is being found over I = 1, …, M. Having input an M × N matrix A, output Norm2(A, K, N), K = 1, …, M.
 */

function Norm2($A, $M, $N)
{
    $K = array();
    for ($i = 1; $i <= $M; $i++){
        $K[$i] = 0;
    }

    for ($i = 1; $i <= $M; $i++) {
        for ($j = 1; $j <= $N; $j++) {
            $K[$i] = $K[$i] + $A[$j][$i];
        }
        //echo $K[$i]." ";
    }

    $max = $K[1];
    for ($i = 1; $i <= $M; $i++) {
        if ($max < $K[$i]) {
            $max = $K[$i];
        }
    }
    //echo "\n".$max;
    return $max;
}
/*
$A = array(     1 => array(1 => 0,1,9),
                2 => array(1 => 1,3,1),
                3 => array(1 => 0,1,4));

Norm2($A, 3, 3);*/
?>