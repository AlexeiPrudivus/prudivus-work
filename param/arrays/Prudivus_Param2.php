<?php
/*    Описать функцию MaxNum(A, N) целого типа, находящую номер максимального элемента вещественного массива A размера N.
    С помощью этой функции найти номера максимальных элементов массивов A, B, C размера NA, NB, NC соответственно.
 */

function MaxNum($A, $N) {
    $max = $A[1];

    if ($N > count($A)) {
        $N = count($A);
    }

    if ($N < count($A)) {
        for ($i = $N; $i <= count($A); $i++) {
            unset($A[$i + 1]);
        }
    }

    for ($i = 1; $i <= $N; $i++) {
        if ($A[$i] > $max) {
            $max = $A[$i];
        }
    }

    return $max;
}
    //echo MaxNum(array(1 => 0,7,5), 50);
?>