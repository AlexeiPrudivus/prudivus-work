<?php
/*
Write a procedure ArrayToMatrCol(A, K, M, N, B) that copies elements of an array A of K real numbers to an M × N matrix B (by columns).
"Superfluous" array elements must be ignored;
if the size of the array is less than the amount of matrix elements then zero value must be assigned to remaining matrix elements.
Two-dimensional array B is an output parameter.
Having input an array A of size K, integers M, N and using this procedure, create a matrix B and output its elements.
 */

function ArrayToMatrCol($A, $K, $M, $N, &$B)
{
    $U = 1;

    for ($i = 1; $i <= $M; $i++)
    {
        for ($j = 1; $j <= $N; $j++) {
            if ($U <= $K) {
                $B[$j][$i] = $A[$U];
                $U++;
            }
            else {
                $B[$j][$i] = 0;
            }
        }
    }
/*
    for ($i = 1; $i <= $M; $i++)
    {
        for ($j = 1; $j <= $N; $j++) {
            echo $B[$i][$j]." ";
        }
        echo "\n";
    }*/
}
/*
$A = array(1 => 1,2,3,4,5,6,7,8,9,0,1,2,3,4,5);
//$A = array();
$K = 15;
$M = 4;
$N = 4;
$B = array(array());
ArrayToMatrCol($A, $K, $M, $N, $B);*/
?>