<?php
/*    Описать функцию MinElem(A, N) целого типа, находящую минимальный элемент целочисленного массива A размера N.
    С помощью этой функции найти минимальные элементы массивов A, B, C размера NA, NB, NC соответственно.
 */

function MinElem($A, $N) {
    $min = $A[1];

    if ($N > count($A)) {
        $N = count($A);
    }

    if ($N < count($A)) {
        for ($i = $N; $i <= count($A); $i++) {
            unset($A[$i + 1]);
        }
    }

    for ($i = 1; $i < $N; $i++) {
        if ($A[$i] < $min) {
            $min = $A[$i];
        }
    }

    return $min;
}
    echo MinElem(array(1 => 0,7,5), 50);
?>