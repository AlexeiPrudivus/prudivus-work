<?php
/*
Write a procedure RemoveRows(A, M, N, K1, K2) that removes rows with the order numbers
in the range K1 to K2 from an M × N matrix A of real numbers
(integers K1 and K2 are assumed to satisfy the double inequality 1 < K1 ≤ K2).
If K1 > M then the matrix remains unchanged, if K2 > M then rows with numbers from K1 to M must be removed.
Two-dimensional array A and integers M, N are input and output parameters.
Having input an M × N matrix A and two integers K1, K2 and using this procedure,
remove rows with the order numbers in the range K1 to K2 from the given matrix
and output a new order and elements of the resulting matrix.
*/

function RemoveRows(&$A, &$M, &$N, $K1, $K2)
{
    if ($K2 > $M) {
        $K2 = $M;
    }

    if (($K1 > 1) && ($K1 <= $K2)) {
        for ($i = 1; $i <= $M; $i++) {
            if ($i >= $K1 && $i <= $K2) {
                for ($j = 1; $j <= $N; $j++) {
                    unset($A[$i][$j]);
                }
            }
        }

        $M = $M - ($K2 - $K1) - 1;
        if ($M != 0) {
            $A = array_combine(range(1, $M), array_values(array_filter($A)));
        }
    }
/*
    for ($i = 1; $i <= $M; $i++) {
            for ($j = 1; $j <= $N; $j++) {
                echo $A[$i][$j]." ";
            }
        echo "\n";
    }*/
}
/*
$A = array(     1 => array(1 => 1,2,3,4),
                2 => array(1 => 5,6,7,8),
                3 => array(1 => 9,0,1,2),
                4 => array(1 => 3,4,5,6),
);
$M = 4;
$N = 4;

RemoveRows($A, $M, $N, 2, 8);*/
?>