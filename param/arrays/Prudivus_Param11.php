<?php
/*
Write a procedure SortArray(A, N) that sorts an array A of N real numbers in ascending order.
The array A is an input and output parameter.
Using this procedure, sort three given arrays A, B, C of size NA, NB, NC respectively.
 */

function SortArray(&$A, $N)
{
    if ($N > count($A)) {
        $N = count($A);
    }

    for ($i = 1; $i <= $N; $i++) {
        for ($j = 1; $j <= $N; $j++) {
            if ($A[$i] < $A[$j]) {
                $Z = $A[$i];
                $A[$i] = $A[$j];
                $A[$j] = $Z;
            }
        }
    }
}
?>