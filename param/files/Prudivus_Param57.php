<?php
/*
Write a procedure EncodeText(S, K) that encrypts the contents of a text file called S
using the right cyclic shift of any Latin letter by K positions of the English alphabet (0 < K < 10).
For instance, if K = 3 then the letter "A" is encoded by the letter "D",
"a" is encoded by "d", "B" is encoded by "E", "z" is encoded by "c", and so on.
Blank characters and punctuation marks should not be changed.
Having input an integer K and using this procedure, encrypt a text file with the given name.
*/

function EncodeText(&$S, $K)
{
    if (file_exists($S)) {
        $lines = file($S);
        $s = '';
        $a = '';

        for ($i = 0; $i < count($lines); $i++) {
            for ($j = 0; $j < strlen($lines[$i]); $j++) {
                if (preg_match('/[a-zA-Z]/', $lines[$i][$j])) {
                    $a = chr(ord($lines[$i][$j]) + $K);
                    $s = $s . $a;
                    $a = '';
                } else {
                    $s = $s . $lines[$i][$j];
                }
            }
        }
        echo $s;
        file_put_contents($S, $s);

        //echo file_get_contents($S);
    }
}

$S = "files/text_file_57.txt";
$K = 2;

EncodeText($S, $K);
?>