<?php
/*
Write an integer function IntFileSize(S) that returns the amount of components in a binary file of integers called S.
If the required file does not exist then the function returns −1.
Using this function, output the amount of components for three binary files of integers with given names.
*/

function IntFileSize($S)
{
    if (file_exists($S)) {
        $handle = fopen($S, "r");
        $contents = fread($handle, filesize($S));
        fclose($handle);

        return strlen($contents);
    } else {
        return -1;
    }
}

$S = "files/text_file_48.txt";

echo IntFileSize($S);
?>