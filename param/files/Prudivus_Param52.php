<?php
/*
Write a procedure RemoveLineNumbers(S) that removes order numbers (with leading and trailing blank characters)
from the beginning of each line of a text file called S (the format of order numbers is described in Param51).
If file lines do not contain order numbers then the procedure performs no actions.
Apply this procedure to a given text file.
*/

function RemoveLineNumbers(&$S)
{
    if (file_exists($S)) {
        $lines = file($S);

        for ($i = 0; $i < count($lines); $i++) {
            $l = strlen($lines[$i]);
            for ($j = 0; $j < $l; $j++) {
                if (is_numeric($lines[$i][0]) || $lines[$i][0] == " ") {
                    $lines[$i] = substr($lines[$i], 1);
                }
            }
        }

        file_put_contents($S, $lines);
        //echo file_get_contents($S);
    }
}

$S = "files/text_file_52.txt";

RemoveLineNumbers($S);
?>