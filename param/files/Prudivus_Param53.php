<?php
/*
Write a procedure SplitIntFile(S0, K, S1, S2) that copies first K (≥ 0) components of an existing file called S0
to a new file called S1 and other components of this file to a new file called S2.
All files are assumed to be binary files of integers;
one of the resulting files may be empty.
Apply this procedure to a given file called S0 using given values of K, S1, S2.
*/

function SplitIntFile($S0, $K, &$S1, &$S2)
{
    if (file_exists($S0)) {
        $S1 = "../files/S1.txt"; //для тестирования
        $S2 = "../files/S2.txt"; //для тестирования

        $lines = file($S0);
        $preK = '';
        $afterK = '';
        $u = 0;

        for ($i = 0; $i < count($lines); $i++) {
            for ($j = 0; $j < strlen($lines[$i]); $j++) {
                if ($u < $K) {
                    $preK = $preK.$lines[$i][$j];
                } else {
                    $afterK = $afterK . $lines[$i][$j];
                }

                $u++;
            }
        }

        file_put_contents($S1, $preK);
        file_put_contents($S2, $afterK);
/*
        echo file_get_contents($S0);
        echo "\n\n".file_get_contents($S1);
        echo "\n\n".file_get_contents($S2);
*/
    }
}
/*
$S0 = "files/text_file_53.txt";
$K = 7;

SplitIntFile($S0, $K, $S1, $S2);*/
?>