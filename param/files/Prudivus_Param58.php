<?php
/*
Write a procedure DecodeText(S, K) that decrypts the contents of a text file called S
provided that this file was encrypted by the method described in Param57 with using an integer parameter K.
Having input an integer K and using this procedure, decrypt a text file with the given name.
*/

function DecodeText(&$S, $K)
{
    if (file_exists($S)) {
        $lines = file($S);
        $s = '';
        $a = '';

        for ($i = 0; $i < count($lines); $i++) {
            for ($j = 0; $j < strlen($lines[$i]); $j++) {
                if (preg_match('/[a-zA-Z]/', $lines[$i][$j])) {
                    $a = chr(ord($lines[$i][$j]) - $K);
                    $s = $s . $a;
                    $a = '';
                } else {
                    $s = $s . $lines[$i][$j];
                }
            }
        }
        echo $s;
        file_put_contents($S, $s);

        //echo file_get_contents($S);
    }
}

$S = "files/text_file_58.txt";
$K = 2;

DecodeText($S, $K);
?>