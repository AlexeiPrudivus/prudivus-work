<?php
/*
Write a procedure StringFileToText(S) that converts a binary file of strings called S to a new text file with the same name.
Using this procedure, convert two given files of strings with the names S1, S2 to text files.
*/

function StringFileToText($S)
{
    if (file_exists($S)) {
        $S5 = "../files/S5.txt"; //для тестирования

        $lines = file($S);
        $s = '';

        for ($i = 0; $i < count($lines); $i++) {
            for ($j = 0; $j < strlen($lines[$i]); $j = $j + 4) {
                $s = $s . chr($lines[$i][$j] . $lines[$i][$j + 1] . $lines[$i][$j + 2]);
            }
        }

        //echo $s."\n";
        //echo file_get_contents($S);

        file_put_contents($S5, $s);

       // echo file_get_contents($S5);
    }
}
/*
$S = "files/text_file_55";

StringFileToText($S);*/
?>