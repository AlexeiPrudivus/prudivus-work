<?php
/*
Write an integer function LineCount(S) that returns the amount of lines in a text file called S.
If the required file does not exist then the function returns −1.
Using this function, output the amount of lines for three text files with given names.
*/

function LineCount($S)
{
    if (file_exists($S)) {
        $LineCount = count(file($S));

        return $LineCount;
    } else {
        return -1;
    }
}

$S = "files/text_file_49.txt";

echo LineCount($S);
?>