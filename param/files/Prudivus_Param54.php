<?php
/*
Write a procedure SplitText(S0, K, S1, S2) that copies first K (≥ 0) lines of an existing text file called S0
to a new text file called S1 and other lines of this file to a new text file called S2 (one of the resulting files may be empty).
Apply this procedure to a given file called S0 using given values of K, S1 S2.
*/

function SplitText($S0, $K, &$S3, &$S4)
{
    if (file_exists($S0)) {
        $S3 = "../files/S3.txt"; //для тестирования
        $S4 = "../files/S4.txt"; //для тестирования

        $lines = file($S0);
        $preK = '';
        $afterK = '';
        $u = 0;

        for ($i = 0; $i < count($lines); $i++) {
            if ($u < $K) {
                $preK = $preK.$lines[$i];
            } else {
                $afterK = $afterK . $lines[$i];
            }
            $u++;
        }

        file_put_contents($S3, $preK);
        file_put_contents($S4, $afterK);
/*
        echo file_get_contents($S0);
        echo "\n\n".file_get_contents($S3);
        echo "\n\n".file_get_contents($S4);
*/
    }
}
/*
$S0 = "files/text_file_54.txt";
$K = 4;

SplitText($S0, $K, $S3, $S4);*/
?>