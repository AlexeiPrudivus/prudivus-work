<?php
/*
Write a procedure AddLineNumbers(S, N, K, L) that adds the number of each line of a text file called S to the beginning of this line;
the first line receives the number N, the second line receives the number N + 1, and so on.
Any number is right-aligned within K first character positions of a line and is separated from the following text by L blank characters (K > 0, L > 0).
If a line is empty then its number should not contain trailing blank characters.
Apply this procedure to a given text file using given values of parameters N, K, L.
*/

function AddLineNumbers(&$S, $N, $K, $L)
{
    if (file_exists($S)) {
        $lines = file($S);

        for ($i = 0; $i < count($lines); $i++) {
            $e = $lines[$i];
            if ($e != PHP_EOL) {
                for ($u = 0; $u < $L; $u++) {
                    $lines[$i] = " " . $lines[$i];
                }
            }

            $lines[$i] = ($i+$N).$lines[$i];

            for ($j = 0; $j < $K; $j++) {
                $lines[$i] = " " . $lines[$i];
            }
            //echo $lines[$i];
        }

        file_put_contents($S, $lines);
        //echo file_get_contents($S);
    }
}

$S = "files/text_file_51.txt";
$N = 5;
$K = 1;
$L = 1;

AddLineNumbers($S, $N, $K, $L);
?>