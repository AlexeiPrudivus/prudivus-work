<?php
/*
Write a procedure SplitText(S0, K, S1, S2) that copies first K (≥ 0) lines of an existing text file called S0
to a new text file called S1 and other lines of this file to a new text file called S2 (one of the resulting files may be empty).
 */
require_once("../Prudivus_Param54.php");
class SplitTextTest extends PHPUnit_Framework_TestCase {
// file exists
    public function testDigitNTest1() {
        $S0 = "../files/text_file_54.txt";
/*
first line
second line
third line
fourth line
fifth line
sixth line
*/
        $K = 4;

        SplitText($S0, $K, $S1, $S2);
        $this->assertEquals(file_get_contents($S1),
'first line
second line
third line
fourth line
'
        );
        $this->assertEquals(file_get_contents($S2),
'fifth line
sixth line'
        );
    }
// file does not exist
    public function testDigitNTest2() {
        $S0 = "../files/text_file_40.txt";
        $K = 3;
        SplitText($S0, $K, $S1, $S2);
    }
}