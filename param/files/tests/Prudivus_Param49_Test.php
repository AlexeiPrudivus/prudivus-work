<?php
/*
Write an integer function LineCount(S) that returns the amount of lines in a text file called S.
If the required file does not exist then the function returns −1.
 */
require_once("../Prudivus_Param49.php");
class LineCountTest extends PHPUnit_Framework_TestCase {
// file exists
    public function testDigitNTest1() {
        $S = "../files/text_file_49.txt";
        $this->assertEquals(LineCount($S), 5);
    }
// file does not exist
    public function testDigitNTest2() {
        $S = "../files/text_file_40.txt";
        $this->assertEquals(LineCount($S), -1);
    }
}