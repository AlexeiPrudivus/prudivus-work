<?php
/*
Write a procedure DecodeText(S, K) that decrypts the contents of a text file called S
provided that this file was encrypted by the method described in Param57 with using an integer parameter K.
 */
require_once("../Prudivus_Param58.php");
class DecodeTextTest extends PHPUnit_Framework_TestCase {
// text file
    public function testDigitNTest1() {
        $S = "../files/text_file_58.txt"; //ncvkp, vg5zv%
        $K = 2;

        DecodeText($S, $K);
        $this->assertEquals(file_get_contents("../files/text_file_58.txt"), 'latin, te5xt%');
    }
// file does not exist
    public function testDigitNTest2() {
        $S = "../files/text_file_40.txt";
        $K = 2;

        DecodeText($S, $K);
    }
}