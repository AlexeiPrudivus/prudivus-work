<?php
/*
Write a procedure EncodeText(S, K) that encrypts the contents of a text file called S
using the right cyclic shift of any Latin letter by K positions of the English alphabet (0 < K < 10).
For instance, if K = 3 then the letter "A" is encoded by the letter "D",
"a" is encoded by "d", "B" is encoded by "E", "z" is encoded by "c", and so on.
Blank characters and punctuation marks should not be changed.
 */
require_once("../Prudivus_Param57.php");
class EncodeTextTest extends PHPUnit_Framework_TestCase {
// text file
    public function testDigitNTest1() {
        $S = "../files/text_file_57.txt"; //latin, te5xt%
        $K = 2;

        EncodeText($S, $K);
        $this->assertEquals(file_get_contents("../files/text_file_57.txt"), 'ncvkp, vg5zv%');
    }
// file does not exist
    public function testDigitNTest2() {
        $S = "../files/text_file_40.txt";
        $K = 2;

        EncodeText($S, $K);
    }
}