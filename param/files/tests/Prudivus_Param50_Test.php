<?php
/*
Write a procedure InvIntFile(S) that changes the order of components of a binary file of integers called S to inverse one.
If the required file does not exist then the procedure performs no actions.
 */
require_once("../Prudivus_Param50.php");
class InvIntFileTest extends PHPUnit_Framework_TestCase {
// file exists
    public function testDigitNTest1() {
        $S = "../files/text_file_50.txt"; //56743222222222222222222222222227
        InvIntFile($S);
        $this->assertEquals(file_get_contents($S), 72222222222222222222222222234765);
    }
// file does not exist
    public function testDigitNTest2() {
        $S = "../files/text_file_40.txt";
        InvIntFile($S);
    }
}