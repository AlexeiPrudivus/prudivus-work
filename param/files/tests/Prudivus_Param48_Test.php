<?php
/*
Write an integer function IntFileSize(S) that returns the amount of components in a binary file of integers called S.
If the required file does not exist then the function returns −1.
 */
require_once("../Prudivus_Param48.php");
class IntFileSizeTest extends PHPUnit_Framework_TestCase {
// file exists
    public function testDigitNTest1() {
        $S = "../files/text_file_48.txt";
        $this->assertEquals(IntFileSize($S), 52);
    }
// file does not exist
    public function testDigitNTest2() {
        $S = "../files/text_file_40.txt";
        $this->assertEquals(IntFileSize($S), -1);
    }
}