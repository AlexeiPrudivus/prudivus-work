<?php
/*
Write a procedure RemoveLineNumbers(S) that removes order numbers (with leading and trailing blank characters)
from the beginning of each line of a text file called S (the format of order numbers is described in Param51).
If file lines do not contain order numbers then the procedure performs no actions.
 */
require_once("../Prudivus_Param52.php");
class RemoveLineNumbersTest extends PHPUnit_Framework_TestCase {
// file exists
    public function testDigitNTest1() {
        $S = "../files/text_file_52.txt";
/*
  6  line 1 text
  7  line 2 text
  8  line 3 text
  9
  10  line 5 text
  11  line 6 text
  12
  13  line 8 text
*/
        RemoveLineNumbers($S);
        $this->assertEquals(file_get_contents($S),
'line 1 text
line 2 text
line 3 text

line 5 text
line 6 text

line 8 text'
        );
    }
// file does not exist
    public function testDigitNTest2() {
        $S = "../files/text_file_40.txt";
        RemoveLineNumbers($S);
    }
}