<?php
/*
Write a procedure TextToStringFile(S) that converts a text file called S to a new binary file of strings with the same name.
 */
require_once("../Prudivus_Param56.php");
class TextToStringFileTest extends PHPUnit_Framework_TestCase {
// text file
    public function testDigitNTest1() {
        $S = "../files/text_file_56.txt"; //this is a text file

        TextToStringFile($S);
        $this->assertEquals(file_get_contents("../files/S6.txt"), '116 104 105 115 32 105 115 32 97 32 116 101 120 116 32 102 105 108 101 '); //ascii
    }
// file does not exist
    public function testDigitNTest2() {
        $S = "../files/text_file_40.txt";

        TextToStringFile($S);
    }
}