<?php
/*
Write a procedure SplitIntFile(S0, K, S1, S2) that copies first K (≥ 0) components of an existing file called S0
to a new file called S1 and other components of this file to a new file called S2.
All files are assumed to be binary files of integers;
one of the resulting files may be empty.
 */
require_once("../Prudivus_Param53.php");
class RemoveLineNumbersTest extends PHPUnit_Framework_TestCase {
// file exists
    public function testDigitNTest1() {
        $S0 = "../files/text_file_53.txt"; //1234567890
        $K = 3;

        SplitIntFile($S0, $K, $S1, $S2);
        $this->assertEquals(file_get_contents($S1), 123);
        $this->assertEquals(file_get_contents($S2), 4567890);
    }
// file does not exist
    public function testDigitNTest2() {
        $S0 = "../files/text_file_40.txt";
        $K = 3;
        SplitIntFile($S0, $K, $S1, $S2);
    }
}