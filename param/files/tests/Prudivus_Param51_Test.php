<?php
/*
Write a procedure AddLineNumbers(S, N, K, L) that adds the number of each line of a text file called S to the beginning of this line;
the first line receives the number N, the second line receives the number N + 1, and so on.
Any number is right-aligned within K first character positions of a line and is separated from the following text by L blank characters (K > 0, L > 0).
If a line is empty then its number should not contain trailing blank characters.
 */
require_once("../Prudivus_Param51.php");
class AddLineNumbersTest extends PHPUnit_Framework_TestCase {
// file exists
    public function testDigitNTest1() {
        $S = "../files/text_file_51.txt";
/*
line 1 text
line 2 text
line 3 text

line 5 text
*/
        $N = 5;
        $K = 1;
        $L = 1;

        AddLineNumbers($S, $N, $K, $L);
        $this->assertEquals(file_get_contents($S),
' 5 line 1 text
 6 line 2 text
 7 line 3 text
 8
 9 line 5 text'
        );
    }
// file does not exist
    public function testDigitNTest2() {
        $S = "../files/text_file_40.txt";
        $N = 5;
        $K = 1;
        $L = 1;
        AddLineNumbers($S, $N, $K, $L);
    }
}