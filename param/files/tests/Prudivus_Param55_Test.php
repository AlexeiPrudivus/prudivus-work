<?php
/*
Write a procedure StringFileToText(S) that converts a binary file of strings called S to a new text file with the same name.
Using this procedure, convert two given files of strings with the names S1, S2 to text files.
 */
require_once("../Prudivus_Param55.php");
class StringFileToTextTest extends PHPUnit_Framework_TestCase {
// file with ASCII code
    public function testDigitNTest1() {
        $S = "../files/text_file_55"; //116 104 105 115 032 105 115 032 097 032 115 116 114 105 110 103

        StringFileToText($S);
        $this->assertEquals(file_get_contents("../files/S5.txt"), 'this is a string');
    }
// file does not exist
    public function testDigitNTest2() {
        $S = "../files/text_file_40.txt";

        StringFileToText($S);
    }
}