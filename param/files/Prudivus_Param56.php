<?php
/*
Write a procedure TextToStringFile(S) that converts a text file called S to a new binary file of strings with the same name.
Using this procedure, convert two given text files with the names S1, S2 to binary files of strings.
*/

function TextToStringFile($S)
{
    if (file_exists($S)) {
        $S6 = "../files/S6.txt"; //для тестирования

        $lines = file($S);
        $s = '';

        for ($i = 0; $i < count($lines); $i++) {
            for ($j = 0; $j < strlen($lines[$i]); $j++) {
                $s = $s . ord($lines[$i][$j])." ";
            }
        }

        //echo $s."\n";
        //echo file_get_contents($S);

        file_put_contents($S6, $s);

        echo file_get_contents($S6);
    }
}

$S = "files/text_file_56.txt";

TextToStringFile($S);
?>