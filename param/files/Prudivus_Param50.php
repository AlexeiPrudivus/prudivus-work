<?php
/*
Write a procedure InvIntFile(S) that changes the order of components of a binary file of integers called S to inverse one.
If the required file does not exist then the procedure performs no actions.
Using this procedure, process three binary files of integers with given names.
*/

function InvIntFile(&$S)
{
    if (file_exists($S)) {
        $handle = fopen($S, "r");
        $contents = fread($handle, filesize($S));
        $contents = strrev($contents);
/*
        echo file_get_contents($S)."\n";
        file_put_contents($S, $contents);
        echo file_get_contents($S);
        */
        fclose($handle);
    }
}

$S = "files/text_file_50.txt";

InvIntFile($S);
?>