<?php
/*
Using the TPoint type and the Leng function (see Param64),
define a new type called TTriangle that is a record with three fields A, B, C (triangle vertices) of TPoint type,
and write a real-valued function Perim(T) that returns the perimeter of a triangle T (T is an input parameter of TTriangle type).
Using this function, find perimeters of triangles ABC, ABD, ACD provided that the coordinates of points A, B, C, D are given.
*/
require_once("Prudivus_Param64.php");

class TTriangle {
    function TTriangle (TPoint $A, TPoint $B, TPoint $C){
            $this->a = $A;
            $this->b = $B;
            $this->c = $C;
    }
}

function Perim(TTriangle $T) {
    return Leng($T->a, $T->b) + Leng($T->b, $T->c) + Leng($T->a, $T->c);
}
/*
$A = new TPoint(5,1);
$B = new TPoint(7,0);
$C = new TPoint(1,2);

$T = new TTriangle($A, $B, $C);

echo Perim($T);*/
?>