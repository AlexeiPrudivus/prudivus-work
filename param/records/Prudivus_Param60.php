<?php
/*
Using the TDate type and the LeapYear function (see Param59),
write an integer function DaysInMonth(D) with a parameter D of TDate type.
The function returns the amount of days for the month of date D.
Output the return values of this function for five given dates (all dates are assumed to be correct).
*/
require_once("Prudivus_Param59.php");

function  DaysInMonth(TDate $D) {
    switch ($D->month) {

        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12: {
            return  31;
            break;
        }

        case 4:
        case 6:
        case 9:
        case 11: {
            return 30;
            break;
        }

        case 2: {
            if (LeapYear($D) == true) {
                return 29;
                break;
            } else {
                return 28;
                break;
            }
        }

        default: {
            return 0;
            break;
        }
    }
}
/*
$date = new TDate(15, 02, 2005);

echo DaysInMonth($date);
*/
?>