<?php
/*
Using the TPoint and TTriangle types and the Dist function (see Param64, Param65, Param67),
write a procedure Alts(T, h1, h2, h3) that evaluates the altitudes h1, h2, h3
drawn from the vertices T.A, T.B, T.C of a triangle T
(T is an input parameter of TTriangle type, h1, h2, h3 are output real-valued parameters).
Using this procedure, evaluate the altitudes of each of triangles ABC, ABD, ACD provided that the coordinates of points A, B, C, D are given.
*/
require_once("Prudivus_Param67.php");

function Alts(TTriangle $T, &$h1, &$h2, &$h3) {
    $A = new TPoint($T->a->x, $T->a->y);
    $B = new TPoint($T->b->x, $T->b->y);
    $C = new TPoint($T->c->x, $T->c->y);

    $h1 = Dist($T);

    $T = new TTriangle($B, $A, $C);
    $h2 = Dist($T);

    $T = new TTriangle($C, $A, $B);
    $h3 = Dist($T);

    echo $h1." ";
    echo $h2." ";
    echo $h3." ";
}
/*
$A = new TPoint(0,1);
$B = new TPoint(1,0);
$C = new TPoint(1,1);

$T = new TTriangle($A, $B, $C);

Alts($T, $h1, $h2, $h3);*/
?>