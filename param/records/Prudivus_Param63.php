<?php
/*
Using the TDate type and the DaysInMonth and CheckDate functions (see Param59−Param61),
write a procedure NextDate(D) that changes a correct date D (of TDate type) to the next one;
if D contains an invalid date then it remains unchanged.
The parameter D is an input and output parameter.
Apply this procedure to five given dates.
*/
require_once("Prudivus_Param61.php");

function NextDate(TDate &$D) {
    if (CheckDateD($D) == 0) {
        if ($D->day == DaysInMonth($D) && $D->month == 12) {
            $D->day = 1;
            $D->month = 1;
            $D->year += 1;
        } else if ($D->day == DaysInMonth($D)) {
            $D->month += 1;
            $D->day = 1;
        } else {
            $D->day += 1;
        }
    }
}
/*
$date = new TDate(31, 12, 2005);

NextDate($date);

echo $date->day." ";
echo $date->month." ";
echo $date->year." ";
*/
?>