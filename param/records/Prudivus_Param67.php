<?php
/*
Using the TPoint and TTriangle types and the Leng and Area functions (see Param64–Param66),
write a real-valued function Dist(P, A, B) that returns the distance D(P, AB) between a point P and a line AB:
D(P, AB) = 2·SPAB/|AB|,
where SPAB is the area of the triangle PAB (parameters P, A, B are input parameters of TPoint type).
Using this function, find the distance between a point P and each of lines AB, AC, BC provided that the coordinates of points P, A, B, C are given.
*/
require_once("Prudivus_Param66.php");

function Dist(TTriangle $T) {
    return round(2 * Area($T) / Leng($T->b, $T->c), 2);
}
/*
$A = new TPoint(0,1);
$B = new TPoint(1,0);
$C = new TPoint(1,1);

$T = new TTriangle($A, $B, $C);

echo Dist($T);*/
?>