<?php
/*
Using the TDate type and the DaysInMonth function (see Param59 and Param60),
write an integer function CheckDate(D) with a parameter D of TDate type.
If the date D is a correct date then the function returns 0;
if the date D contains an invalid month number or invalid day number for the correct month then the function returns 1 or 2 respectively.
 */
require_once("../Prudivus_Param61.php");
class CheckDateDTest extends PHPUnit_Framework_TestCase {
// is a correct date
    public function testDigitNTest1() {
        $date = new TDate(15, 02, 2004);
        $this->assertEquals(CheckDateD($date), 0);

        $date = new TDate(30, 01, 2015);
        $this->assertEquals(CheckDateD($date), 0);
    }
// is not a correct date
    public function testDigitNTest2() {
        $date = new TDate(-15, 02, 2005);
        $this->assertEquals(CheckDateD($date), 2);

        $date = new TDate(150, 02, 2005);
        $this->assertEquals(CheckDateD($date), 2);

        $date = new TDate(15, -02, 2005);
        $this->assertEquals(CheckDateD($date), 1);

        $date = new TDate(15, 202, 2005);
        $this->assertEquals(CheckDateD($date), 1);
    }
}