<?php
/*
Using the TPoint and TTriangle types and the Leng and Area functions (see Param64–Param66),
write a real-valued function Dist(P, A, B) that returns the distance D(P, AB) between a point P and a line AB:
D(P, AB) = 2·SPAB/|AB|,
where SPAB is the area of the triangle PAB (parameters P, A, B are input parameters of TPoint type).
*/
require_once("../Prudivus_Param67.php");
class DistTest extends PHPUnit_Framework_TestCase {
// is a correct date
    public function testDigitNTest1() {
        $A = new TPoint(0, 1);
        $B = new TPoint(1, 0);
        $C = new TPoint(1, 1);
        $T = new TTriangle($A, $B, $C);
        $this->assertEquals(Dist($T), 1);

        $A = new TPoint(1, 0);
        $B = new TPoint(0, 1);
        $C = new TPoint(-1, -1);
        $T = new TTriangle($A, $B, $C);
        $this->assertEquals(Dist($T), 1.34);
    }
}