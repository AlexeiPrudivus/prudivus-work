<?php
/*
Using the TDate type and the DaysInMonth and CheckDate functions (see Param59−Param61),
write a procedure PrevDate(D) that changes a correct date D (of TDate type) to the previous one;
if D contains an invalid date then it remains unchanged.
The parameter D is an input and output parameter.
 */
require_once("../Prudivus_Param62.php");
class PrevDateTest extends PHPUnit_Framework_TestCase {
// is a correct date
    public function testDigitNTest1() {
        $date = new TDate(15, 02, 2004);
        PrevDate($date);
        $this->assertEquals($date->day, 14);
        $this->assertEquals($date->month, 02);
        $this->assertEquals($date->year, 2004);

        $date = new TDate(1, 02, 2004);
        PrevDate($date);
        $this->assertEquals($date->day, 31);
        $this->assertEquals($date->month, 1);
        $this->assertEquals($date->year, 2004);

        $date = new TDate(1, 1, 2004);
        PrevDate($date);
        $this->assertEquals($date->day, 31);
        $this->assertEquals($date->month, 12);
        $this->assertEquals($date->year, 2003);
    }
// is not a correct date
    public function testDigitNTest2() {
        $date = new TDate(50, 50, 2004);
        PrevDate($date);
        $this->assertEquals($date->day, 50);
        $this->assertEquals($date->month, 50);
        $this->assertEquals($date->year, 2004);
    }
}