<?php
/*
Using the TPoint type and the Leng function (see Param64),
define a new type called TTriangle that is a record with three fields A, B, C (triangle vertices) of TPoint type,
and write a real-valued function Perim(T) that returns the perimeter of a triangle T (T is an input parameter of TTriangle type).
*/
require_once("../Prudivus_Param65.php");
class PerimTest extends PHPUnit_Framework_TestCase {
// is a correct date
    public function testDigitNTest1() {
        $A = new TPoint(0, 1);
        $B = new TPoint(1, 0);
        $C = new TPoint(1, 1);
        $T = new TTriangle($A, $B, $C);
        $this->assertEquals(Perim($T), 3.41);

        $A = new TPoint(5, 1);
        $B = new TPoint(7, 0);
        $C = new TPoint(1, 2);
        $T = new TTriangle($A, $B, $C);
        $this->assertEquals(Perim($T), 12.68);
    }
}