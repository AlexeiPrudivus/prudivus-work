<?php
/*
Using the TDate type and the LeapYear function (see Param59),
write an integer function DaysInMonth(D) with a parameter D of TDate type.
The function returns the amount of days for the month of date D.
 */
require_once("../Prudivus_Param60.php");
class DaysInMonthTest extends PHPUnit_Framework_TestCase {
// is leap year
    public function testDigitNTest1() {
        $date = new TDate(15, 02, 2004);
        $this->assertEquals(DaysInMonth($date), 29);

        $date = new TDate(15, 02, 2008);
        $this->assertEquals(DaysInMonth($date), 29);

        $date = new TDate(15, 01, 2008);
        $this->assertEquals(DaysInMonth($date), 31);
    }
// is not leap year
    public function testDigitNTest2() {
        $date = new TDate(15, 02, 2005);
        $this->assertEquals(DaysInMonth($date), 28);
    }
}