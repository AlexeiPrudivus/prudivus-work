<?php
/*
Using the TPoint type and the Leng function (see Param64), write a real-valued function PerimN(P, N)
that returns the perimeter of a polygon with N (> 2) vertices.
The polygon vertices have the TPoint type; an array P contains all vertices in order of walk.
*/
require_once("../Prudivus_Param69.php");
class PerimNTest extends PHPUnit_Framework_TestCase {
// N > 2 && = count($P)
    public function testDigitNTest1() {
        $A = new TPoint(0,1);
        $B = new TPoint(1,0);
        $C = new TPoint(1,1);
        $D = new TPoint(-1,-1);
        $E = new TPoint(1,5);

        $P = array(1 => $A, $B, $C);
        $N = 3;
        $this->assertEquals(PerimN($P, $N), 3.41);

        $P = array(1 => $A, $B, $C, $D, $E);
        $N = 5;
        $this->assertEquals(PerimN($P, $N), 15.68);
    }
// N <= 2 || = count($P)
    public function testDigitNTest2() {
        $A = new TPoint(0,1);
        $B = new TPoint(1,0);
        $C = new TPoint(1,1);
        $D = new TPoint(-1,-1);
        $E = new TPoint(1,5);

        $P = array(1 => $A, $B, $C, $D, $E);
        $N = 50;
        $this->assertEquals(PerimN($P, $N), 'error');

        $N = 1;
        $this->assertEquals(PerimN($P, $N), 'error');
    }
}