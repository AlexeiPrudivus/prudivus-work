<?php
/*
Define a new type called TDate that is a record with three integer fields:
Day (a day number), Month (a month number), Year (a year number).
Also write a logical function LeapYear(D) with a parameter D of TDate type.
The function returns True if a year of date D is a leap year, and False otherwise.
Note that a year is a leap year if it is divisible by 4 except for years that are divisible by 100 and are not divisible by 400.
 */
require_once("../Prudivus_Param59.php");
class LeapYearTest extends PHPUnit_Framework_TestCase {
// is leap year
    public function testDigitNTest1() {
        $date = new TDate(31, 02, 2004);
        $this->assertEquals(LeapYear($date), true);

        $date = new TDate(31, 02, 2012);
        $this->assertEquals(LeapYear($date), true);
    }
// is not leap year
    public function testDigitNTest2() {
        $date = new TDate(31, 02, 205);
        $this->assertEquals(LeapYear($date), false);

        $date = new TDate(31, 02, 700);
        $this->assertEquals(LeapYear($date), false);
    }
}