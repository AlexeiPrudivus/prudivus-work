<?php
/*
Define a new type called TPoint that is a record with two real-valued fields: X (an x-coordinate), Y (an y-coordinate).
Also write a real-valued function Leng(A, B) that returns the length of a segment AB (A and B are input parameters of TPoint type):
|AB| = ((A.X − B.X)2 + (A.Y − B.Y)2)1/2.
 */
require_once("../Prudivus_Param64.php");
class LengTest extends PHPUnit_Framework_TestCase {
// is a correct date
    public function testDigitNTest1() {
        $A = new TPoint(0, 1);
        $B = new TPoint(1, 0);
        $this->assertEquals(Leng($A, $B), 1.41);

        $A = new TPoint(1, 0);
        $B = new TPoint(0, 1);
        $this->assertEquals(Leng($A, $B), 1.41);

        $A = new TPoint(0, 3);
        $B = new TPoint(2, 1);
        $this->assertEquals(Leng($A, $B), 2.83);
    }
}