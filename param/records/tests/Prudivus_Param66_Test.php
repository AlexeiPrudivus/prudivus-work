<?php
/*
Using the TPoint and TTriangle types and the Leng and Perim functions (see Param64 and Param65),
write a real-valued function Area(T) that returns the area of a triangle T (T is an input parameter of TTriangle type):
SABC = (p·(p − |AB|)·(p − |AC|)·(p − |BC|))1/2,
where p is the half-perimeter.
*/
require_once("../Prudivus_Param66.php");
class AreaTest extends PHPUnit_Framework_TestCase {
// is a correct date
    public function testDigitNTest1() {
        $A = new TPoint(0, 1);
        $B = new TPoint(1, 0);
        $C = new TPoint(1, 1);
        $T = new TTriangle($A, $B, $C);
        $this->assertEquals(Area($T), 0.5);

        $A = new TPoint(1, 0);
        $B = new TPoint(0, 1);
        $C = new TPoint(-1, -1);
        $T = new TTriangle($A, $B, $C);
        $this->assertEquals(Area($T), 1.5);
    }
}