<?php
/*
Using the TPoint and TTriangle types and the Area function (see Param64–Param66), write a real-valued function AreaN(P, N)
that returns the area of a convex polygon with N (> 2) vertices.
The polygon vertices have the TPoint type; an array P contains all vertices in order of walk.
*/
require_once("../Prudivus_Param70.php");
class AreaNTest extends PHPUnit_Framework_TestCase {
// N > 2 && = count($P)
    public function testDigitNTest1() {
        $A = new TPoint(0,0);
        $B = new TPoint(0,1);
        $C = new TPoint(1,1);
        $D = new TPoint(1,0);
        $E = new TPoint(0.5,-0.5);

        $P = array(1 => $A, $B, $C);
        $N = 3;
        $this->assertEquals(AreaN($P, $N), 0.5);

        $P = array(1 => $A, $B, $C, $D);
        $N = 4;
        $this->assertEquals(AreaN($P, $N), 1);

        $P = array(1 => $A, $B, $C, $D, $E);
        $N = 5;
        $this->assertEquals(AreaN($P, $N), 1.25);
    }
// N <= 2 || = count($P)
    public function testDigitNTest2() {
        $A = new TPoint(0,1);
        $B = new TPoint(1,0);
        $C = new TPoint(1,1);
        $D = new TPoint(-1,-1);
        $E = new TPoint(1,5);

        $P = array(1 => $A, $B, $C, $D, $E);
        $N = 50;
        $this->assertEquals(AreaN($P, $N), 'error');

        $N = 1;
        $this->assertEquals(AreaN($P, $N), 'error');
    }
}