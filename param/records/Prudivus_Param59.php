<?php
/*
Define a new type called TDate that is a record with three integer fields:
Day (a day number), Month (a month number), Year (a year number).
Also write a logical function LeapYear(D) with a parameter D of TDate type.
The function returns True if a year of date D is a leap year, and False otherwise.
Output the return values of this function for five given dates (all dates are assumed to be correct).
Note that a year is a leap year if it is divisible by 4 except for years that are divisible by 100 and are not divisible by 400.
*/

class TDate {
    function TDate ($day, $month, $year){
       $this->day = $day;
       $this->month = $month;
       $this->year = $year;
    }
}

function LeapYear(TDate $D) {
    if (($D->year % 100 == 0) && ($D->year % 400 != 0)) {
        return false;
    } else
        if ($D->year % 4 == 0) {
            return true;
        } else {
            return false;
        }
}

/*
$date = new TDate(31, 02, 2004);

echo LeapYear($date);
*/
?>