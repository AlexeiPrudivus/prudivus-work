<?php
/*
Using the TDate type and the DaysInMonth function (see Param59 and Param60),
write an integer function CheckDate(D) with a parameter D of TDate type.
If the date D is a correct date then the function returns 0;
if the date D contains an invalid month number or invalid day number for the correct month then the function returns 1 or 2 respectively.
Output the return values of this function for five given dates.
*/
require_once("Prudivus_Param60.php");

function  CheckDateD(TDate $D) {
    if ($D->month < 0 || $D->month > 12) {
        return 1;
    } else if ($D->day < 0 || $D->day > DaysInMonth($D)) {
        return 2;
    } else {
        return 0;
    }
}
/*
$date = new TDate(15, 02, 2005);

echo CheckDateD($date);
*/
?>