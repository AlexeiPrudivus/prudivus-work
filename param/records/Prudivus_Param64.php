<?php
/*
Define a new type called TPoint that is a record with two real-valued fields: X (an x-coordinate), Y (an y-coordinate).
Also write a real-valued function Leng(A, B) that returns the length of a segment AB (A and B are input parameters of TPoint type):
|AB| = ((A.X − B.X)2 + (A.Y − B.Y)2)1/2.
Using this function, output lengths of segments AB, AC, AD provided that the coordinates of points A, B, C, D are given.
*/

class TPoint {
    function TPoint ($X, $Y){
        $this->x = $X;
        $this->y = $Y;
    }
}

function Leng(TPoint $A, TPoint $B) {
    return round(abs(pow(pow(($A->x - $B->x), 2) + pow(($A->y - $B->y), 2), 1/2)), 2);
}
/*
$A = new TPoint(0, 3);
$B = new TPoint(2, 1);

echo Leng($A, $B);*/
?>