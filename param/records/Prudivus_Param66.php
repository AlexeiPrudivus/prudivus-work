<?php
/*
Using the TPoint and TTriangle types and the Leng and Perim functions (see Param64 and Param65),
write a real-valued function Area(T) that returns the area of a triangle T (T is an input parameter of TTriangle type):
SABC = (p·(p − |AB|)·(p − |AC|)·(p − |BC|))1/2,
where p is the half-perimeter.
Using this function, find the areas of triangles ABC, ABD, ACD provided that the coordinates of points A, B, C, D are given.
*/
require_once("Prudivus_Param65.php");

function Area(TTriangle $T) {
    return round(pow((Perim($T)/2 * (Perim($T)/2 - abs(Leng($T->a, $T->b))) * (Perim($T)/2 - abs(Leng($T->a, $T->c))) * (Perim($T)/2 - abs(Leng($T->b, $T->c)))), 1/2), 2);
}
/*
$A = new TPoint(0,1);
$B = new TPoint(1,0);
$C = new TPoint(1,1);

$T = new TTriangle($A, $B, $C);

echo Area($T);*/
?>