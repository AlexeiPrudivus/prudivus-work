<?php
/*
Using the TPoint and TTriangle types and the Area function (see Param64–Param66), write a real-valued function AreaN(P, N)
that returns the area of a convex polygon with N (> 2) vertices.
The polygon vertices have the TPoint type; an array P contains all vertices in order of walk.
Using this function, find the areas of three polygons provided that the amount of vertices and the coordinates of all vertices are given for each polygon.
*/
require_once("Prudivus_Param66.php");

function AreaN($P, $N) {
    if ($N <= 2 || $N != count($P)) {
        return 'error';
    }

    $s = 0;

    for ($i = 1; $i < $N; $i++) {
        $s += $P[$i]->x * $P[$i + 1]->y - $P[$i + 1]->x * $P[$i]->y;

        if ($i == $N - 1) {
            $s += $P[$i + 1]->x * $P[1]->y - $P[1]->x * $P[$i + 1]->y;
        }
    }

    return round(abs($s)/2, 2);
}
/*
$A = new TPoint(0,0);
$B = new TPoint(0,1);
$C = new TPoint(1,1);
$D = new TPoint(1,0);
$E = new TPoint(0.5,-0.5);

$P = array(1 => $A, $B, $C, $D, $E);
$N = 5;

echo AreaN($P, $N);*/
?>