<?php
/*
Using the TDate type and the DaysInMonth and CheckDate functions (see Param59−Param61),
write a procedure PrevDate(D) that changes a correct date D (of TDate type) to the previous one;
if D contains an invalid date then it remains unchanged.
The parameter D is an input and output parameter.
Apply this procedure to five given dates.
*/
require_once("Prudivus_Param61.php");

function PrevDate(TDate &$D) {
    if (CheckDateD($D) == 0) {
        if ($D->day == 1 && $D->month == 1) {
            $D->day = 31;
            $D->month = 12;
            $D->year -= 1;
        } else if ($D->day == 1) {
            $D->month -= 1;
            $D->day = DaysInMonth($D);
        } else {
            $D->day = $D->day - 1;
        }
    }
}
/*
$date = new TDate(1, 01, 2005);

PrevDate($date);

echo $date->day." ";
echo $date->month." ";
echo $date->year." ";
*/
?>