<?php
/*
Using the TPoint type and the Leng function (see Param64), write a real-valued function PerimN(P, N)
that returns the perimeter of a polygon with N (> 2) vertices.
The polygon vertices have the TPoint type; an array P contains all vertices in order of walk.
Using this function, find the perimeters of three polygons provided that the amount of vertices and the coordinates of all vertices are given for each polygon.
*/
require_once("Prudivus_Param64.php");

function PerimN($P, $N) {
    if ($N <= 2 || $N != count($P)) {
        return 'error';
    }

    $l = 0;

    for ($i = 1; $i < $N; $i++) {
        $l += leng($P[$i], $P[$i + 1]);

        if ($i == $N - 1) {
            $l += leng($P[$i + 1], $P[1]);
        }
    }

    return $l;
}

$A = new TPoint(0,1);
$B = new TPoint(1,0);
$C = new TPoint(1,1);
$D = new TPoint(-1,-1);
$E = new TPoint(1,5);

$P = array(1 => $A, $B, $C, $D, $E);
$N = 5;

echo PerimN($P, $N);
?>