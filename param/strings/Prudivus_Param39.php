<?php
/*
Write an integer function PosK(S0, S, K) that searches for K-th occurrence (K > 0) of a string S0 within a string S
and returns the order number of the first character of this occurrence.
The string S is assumed to contain no overlapping occurrences of the substring S0.
If the string S does not contain occurrences of S0 then the function returns 0.
Output return values of this function for five given triples (S0, S, K).
*/
function PosK($S0, $S, $K) {
    $S1 = "";
    $u = 1;
    $r = 0;
    $k = 0;

    for ($j = 0; $j <= strlen($S) - strlen($S0); $j++) {
        for ($i = $u - 1; $i < $u + strlen($S0) - 1; $i++) {
            $S1 = $S1 . $S[$i];
            if ($S1 == $S0) {
                $k++;
                $r = $u;
                if ($k == $K) {
                    return $r;
                }
            }
        }
        $S1 = "";
        $u++;
    }

    return 0;
}
/*
$S0 = "t";
$S = "test text";

echo PosK($S0, $S, 3);*/
?>