<?php
/*
Write a procedure UpCaseLat(S) that converts all Latin small letters of a string S to uppercase
(others characters of S must remain unchanged).
A string S is an input and output parameter.
Using this procedure, process five given strings.
*/

function UpCaseLat(&$S) {
        if(preg_match('/[^a-z]/', $S)) {
            $S = mb_strtoupper($S, 'utf-8');;
        }
    //echo $S;
}
/*
$S = "tрусскийf";

UpCaseLat($S);*/
?>