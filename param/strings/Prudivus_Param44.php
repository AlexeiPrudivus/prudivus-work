<?php
/*
Write a string function DecToBin(N) that returns a string containing the binary representation of a nonnegative integer N.
The string consists of characters "0", "1" and does not contain leading zeros (except for the representation of zero number).
Using this function, output binary representations of five given integers.
*/

/*
Делим десятичное число А на 2. Частное Q запоминаем для следующего шага, а остаток a записываем как младший бит двоичного числа.
Если частное q не равно 0, принимаем его за новое делимое и повторяем процедуру, описанную в шаге 1.
Каждый новый остаток (0 или 1) записывается в разряды двоичного числа в направлении от младшего бита к старшему.
Алгоритм продолжается до тех пор, пока в результате выполнения шагов 1 и 2 не получится частное Q = 0 и остаток a = 1.
*/
function DecToBin($N) {
    $Q = (int)($N / 2);
    $a = $N % 2;
    $A = (string)$a;

    do {
        $a = $Q % 2;
        $Q = (int)($Q / 2);
        $A = $a.$A;
    }
    while ($Q != 0 && $a != 1);

    return $A;
}

$N = 31;

echo DecToBin($N);
?>