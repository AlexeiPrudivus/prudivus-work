<?php
/*
Write a string function DecToHex(N) that returns a string containing the hexadecimal representation of a nonnegative integer N.
The string consists of characters "0"–"9", "A"–"F" and does not contain leading zeros (except for the representation of zero number).
Using this function, output hexadecimal representations of five given integers.
*/
/*
Делим десятичное число А на 16. Частное Q запоминаем для следующего шага, а остаток a записываем как младший бит шестнадцатеричного числа.
Если частное q не равно 0, принимаем его за новое делимое и повторяем процедуру, описанную в шаге 1.
Каждый новый остаток записывается в разряды шестнадцатеричного числа в направлении от младшего бита к старшему.
Алгоритм продолжается до тех пор, пока в результате выполнения шагов 1 и 2 не получится частное Q = 0 и остаток a меньше 16.
*/

function DecToHex($N) {
    $Q = $N;
    $a = 0;
    $A = '';

    do {
        $a = $Q % 16;
        $Q = (int)($Q / 16);

        switch ($a) {
            case 10: {
                $A = 'A'.$A;
                break;
            }
            case 11: {
                $A = 'B'.$A;
                break;
            }
            case 12: {
                $A = 'C'.$A;
                break;
            }
            case 13: {
                $A = 'D'.$A;
                break;
            }
            case 14: {
                $A = 'E'.$A;
                break;
            }
            case 15: {
                $A = 'F'.$A;
                break;
            }

            default : {
                $A = $a.$A;
                break;
            }
        }
    }
    while ($Q != 0 && $a < 16);

    return $A;
}

$N = 10;

echo DecToHex($N);
?>