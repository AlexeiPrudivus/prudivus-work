<?php
/*
Write a string function FillStr(S, N) that returns a string of length N containing repeating copies of the template string S
(the last copy of S may be contained partially in the resulting string).
Having input an integer N and five template strings and using this function,
create five resulting strings of length N.
*/

function FillStr($S, $N) {
$u = 0;
$A = "s";

    for($i = 0; $i < $N; $i++) {
        if ($u == strlen($S)) {
            $u = 0;
            $A[$i] = $S[$u];
            $u++;
        } else {
            $A[$i] = $S[$u];
            $u++;
        }
    }
    return $A;
}

$S = 'la_t9in ';

FillStr($S, 50);
?>