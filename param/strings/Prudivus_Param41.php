<?php
/*
Write a procedure SplitStr(S, W, N) that creates an array W of all words being contained in a string S.
The array W of strings and its size N are output parameters.
A word is defined as a character sequence that does not contain blank characters and is bounded by blank characters or the string beginning/end;
the string S is assumed to contain no more than 10 words.
Using this function, output the amount N of words in the given string S and also all these words.
*/
function SplitStr($S, &$W, &$N) {
    $W = array(1 => '');
    $S = trim($S);
    $S = $S." ";
    $N = 1;

    for ($i = 0; $i < strlen($S); $i++) {
        if ($S[$i] !== ' ') {
            //echo $i;
            $W[$N] = $W[$N].$S[$i];
        }

        if ($i == strlen($S) - 1) {
           /* if ($S[$i] == ' ') {
                $N++;
                $W[$N] = '';
            }*/
        } else {
            if ($S[$i] == ' ' && $S[$i + 1] !== ' ') {
                $N++;
                $W[$N] = '';
                //echo $i." ";
            }
        }
    }
/*
    for ($i = 1; $i < count($W); $i++) {
            echo $i." ".$W[$i] . "\n";
    }*/
    //echo $N;
}

$S = "            s                word1 j gvyt tyrf 756r 75r ,,, 7";

echo SplitStr($S, $W, $N);
?>