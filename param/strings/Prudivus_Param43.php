<?php
/*
Write a string function DecompressStr(S) that restores a string, which was compressed by a function CompressStr (see Param42).
An input parameter S is a compressed string; the function returns the decompressed value of the string S.
Using this function, restore five given compressed strings.
*/
function DecompressStr($S) {
    $S2 = '';
    $N = '';
    $u = 2;

    for ($i = 0; $i <= strlen($S) - 1; $i++) {
        if ($i == strlen($S) - 1) {
            $S2 = $S2 . $S[$i];
        } else if ($S[$i + 1] == '{') {
            //узнаём сколько символов добавлять
            $j = $i + 2;
            $N = '';
            while($S[$j] != '}') {
                $N = $N.$S[$j];
                $j++;
                $u++;
            }

            //добавляем символы
            for ($k = 0; $k < $N; $k++) {
                $S2 = $S2.$S[$i];
            }

            $i += $u;
            $u = 2;
        } else {
            $S2 = $S2 . $S[$i];
        }
    }

    return $S2;
}

$S = "g{5} 9{12}6h";
//$S = "1{32} 55558888 {29}";

echo DecompressStr($S);
?>