<?php
/*
Write a procedure LowCaseLat(S) that converts all Latin capital letters of a string S to lowercase
(others characters of S must remain unchanged).
A string S is an input and output parameter.
Using this procedure, process five given strings.
*/

function LowCaseLat(&$S) {
        if(preg_match('/[^a-z]/', $S)) {
            $S = mb_strtolower($S, 'utf-8');
        }
    //echo $S;
}
/*
$S = "TTTT русский f";

LowCaseLat($S);*/
?>