<?php
/*
Write an integer function BinToDec(S) that returns a nonnegative integer whose binary representation is contained in a string parameter S.
The parameter S consists of characters "0", "1" and does not contain leading zeros (except for the representation of zero number).
Using this function, output five integers whose binary representations are given.
*/

/*
Для перевода целой части необходимо умножить разряд числа на соответствующую ему степень разряда.
*/
function BinToDec($S) {
    $A = 0;
    $u = 0;

    for($i = strlen($S) - 1; $i >= 0; $i--) {
        $A = $A + (int)($S[$i] * pow(2, $u));
        $u++;
    }

    return $A;
}
/*
$N = '1111110';

echo BinToDec($N);*/
?>