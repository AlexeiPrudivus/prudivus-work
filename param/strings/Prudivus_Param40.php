<?php
/*
Write a string function WordK(S, K) that returns K-th word of a string S
(a word is defined as a character sequence that does not contain blank characters and is bounded by blank characters or the string beginning/end).
If the amount of words in the string S is less than K then the function returns an empty string.
Having input a string S and three positive integers K1, K2, K3 and using this function,
extract words with the given order numbers from the given string.
*/
function WordK($S, $K) {
    $S1 = array(0 => '');
    $S = trim($S);
    $S = $S." ";
    $u = 0;
    $K--;

    for ($i = 0; $i < strlen($S); $i++) {
        if ($S[$i] !== ' ') {
            //echo $i;
            $S1[$u] = $S1[$u].$S[$i];
        }

        if ($i == strlen($S) - 1) {
           /* if ($S[$i] == ' ') {
                $u++;
                $S1[$u] = '';
            }*/
        } else  if ($S[$i] == ' ' && $S[$i + 1] !== ' ') {
            $u++;
            $S1[$u] = '';
        }
    }

    return $S1[$K];
/*
    for ($i = 1; $i <= count($S1); $i++) {
            echo $i." ".$S1[$i] . "\n";
    }*/
}
/*
$S = "            s                word1 j gvyt tyrf 756r 75r ,,, 7";
$K = 6;

echo WordK($S, $K);*/
?>