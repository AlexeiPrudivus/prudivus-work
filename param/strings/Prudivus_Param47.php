<?php
/*
Write an integer function HexToDec(S) that returns a nonnegative integer whose hexadecimal representation is contained in a string parameter S.
The parameter S consists of characters "0"–"9", "A"–"F" and does not contain leading zeros (except for the representation of zero number).
Using this function, output five integers whose hexadecimal representations are given.
*/

/*
Для перевода шестнадцатеричного числа в десятичное необходимо это число представить
в виде суммы произведений степеней основания шестнадцатеричной системы счисления на соответствующие цифры в разрядах шестнадцатеричного числа.
*/
function HexToDec($S) {
    $A = 0;
    $u = 0;

    for($i = strlen($S) - 1; $i >= 0; $i--) {
        switch ($S[$i]) {
            case 'A': {
                $B = 10;
                break;
            }
            case 'B': {
                $B = 11;
                break;
            }
            case 'C': {
                $B = 12;
                break;
            }
            case 'D': {
                $B = 13;
                break;
            }
            case 'E': {
                $B = 14;
                break;
            }
            case 'F': {
                $B = 15;
                break;
            }
            default: {
                $B = $S[$i];
                break;
            }
        }

        $A = $A + (int)($B * pow(16, $u));
        $u++;
    }

    return $A;
}
/*
$N = '7FFF';

echo HexToDec($N);*/
?>