<?php
/*
Write a procedure LowCaseLat(S) that converts all Latin capital letters of a string S to lowercase
(others characters of S must remain unchanged).
A string S is an input and output parameter.
 */
require_once("../Prudivus_Param33.php");
class LowCaseLatTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $S = 'SOME LATIN TEXT';
        LowCaseLat($S);
        $this->assertEquals($S, 'some latin text');

        $S = 'TEXT9WI0TH8913NUMBERS';
        LowCaseLat($S);
        $this->assertEquals($S, 'text9wi0th8913numbers');
    }
//  is not latin
    public function testDigitNTest2() {
        $S = '64_';
        LowCaseLat($S);
        $this->assertEquals($S, '64_');

        $S = '709878';
        LowCaseLat($S);
        $this->assertEquals($S, '709878');

        $S = 'TEXT русский ENGLISH';
        LowCaseLat($S);
        $this->assertEquals($S, 'text русский english');
    }
}