<?php
/*
Write an integer function BinToDec(S) that returns a nonnegative integer whose binary representation is contained in a string parameter S.
The parameter S consists of characters "0", "1" and does not contain leading zeros (except for the representation of zero number).
Using this function, output five integers whose binary representations are given.
*/

require_once("../Prudivus_Param46.php");
class BinToDecTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1() {
        $this->assertEquals(BinToDec('101'), 5);
        $this->assertEquals(BinToDec('11111'), 31);
        $this->assertEquals(BinToDec('00'), 0);
        $this->assertEquals(BinToDec('10'), 2);
    }
}