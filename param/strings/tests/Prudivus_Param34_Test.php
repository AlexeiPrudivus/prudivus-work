<?php
/*
Write a procedure TrimLeftC(S, C) that removes all leading characters equal a character C from a string S.
A string S is an input and output parameter.
 */
require_once("../Prudivus_Param34.php");
class TrimLeftCTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $S = 'SSSSSSSSSSSSSSSSSSSSOME LATIN TEXT';
        TrimLeftC($S,'S');
        $this->assertEquals($S, 'OME LATIN TEXT');

        $S = 'ttttttttttttext';
        TrimLeftC($S, 't');
        $this->assertEquals($S, 'ext');
    }
}