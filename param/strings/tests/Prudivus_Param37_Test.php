<?php
/*
Write an integer function PosSub(S0, S, K, N) that searches for the first occurrence of a string S0 within a substring of a string S
(the substring contains N characters of S starting at a character with the order number K).
The function returns the order number of the first character of this occurrence within S.
If K is greater than the length of S then the function returns 0;
if the length of S is less than K + N then all characters of S starting at its K-th character must be analyzed.
If the required substring of S does not contain occurrences of S0 then the function returns 0.
 */
require_once("../Prudivus_Param37.php");
class PosSubTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $this->assertEquals(PosSub("te", "test text", 1, 500), 1);
        $this->assertEquals(PosSub('LAT', 'SOME LATIN TEXT', 4, 5), 6);
        $this->assertEquals(PosSub('EX', 'SOME LATIN TEXT', -7, 20), 13);
        $this->assertEquals(PosSub(' LATIN ', 'SOME LATIN TEXT', 600, 5), 0);
    }
}