<?php
/*
Write a string function DecompressStr(S) that restores a string, which was compressed by a function CompressStr (see Param42).
An input parameter S is a compressed string; the function returns the decompressed value of the string S.
 */
require_once("../Prudivus_Param43.php");
class DecompressStrTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $S = "g{5} 9{12}6h";
        $this->assertEquals(DecompressStr($S), 'ggggg 9999999999996h');

        $S = "551{6} 55558888";
        $this->assertEquals(DecompressStr($S), '55111111 55558888');
    }
}