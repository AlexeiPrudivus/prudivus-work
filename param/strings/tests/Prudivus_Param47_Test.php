<?php
/*
Write an integer function HexToDec(S) that returns a nonnegative integer whose hexadecimal representation is contained in a string parameter S.
The parameter S consists of characters "0"–"9", "A"–"F" and does not contain leading zeros (except for the representation of zero number).
 */
require_once("../Prudivus_Param47.php");
class HexToDecTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1() {
        $this->assertEquals(HexToDec('7FFF'), 32767);
        $this->assertEquals(HexToDec('F'), 15);
        $this->assertEquals(HexToDec('10'), 16);
    }
}