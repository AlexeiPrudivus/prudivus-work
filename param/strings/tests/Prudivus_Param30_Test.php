<?php
/*
Write an integer function IsIdent(S) that indicates whether a string S is a valid identifier,
that is, a nonempty string that does not begin with a digit and contains Latin letters, digits, and a character "_" only.
If S is a valid identifier then the function returns 0.
If S is an empty string or begins with a digit then the function returns −1 or −2 respectively.
If S contains invalid characters then the function returns the order number of the first invalid character.
 */
require_once("../Prudivus_Param30.php");
class IsIdentTest extends PHPUnit_Framework_TestCase {
//  S is a valid identifier
    public function testDigitNTest1() {
        $this->assertEquals(IsIdent('text4_'), 0);
        $this->assertEquals(IsIdent('te_x9t'), 0);
        $this->assertEquals(IsIdent('_t3xt_'), 0);
    }
//  S starts with a digit
    public function testDigitNTest2() {
        $this->assertEquals(IsIdent('6text4_'), -2);
        $this->assertEquals(IsIdent('7te_x9t'), -2);
        $this->assertEquals(IsIdent('8_t3xt_'), -2);
    }
//  S is empty
    public function testDigitNTest3() {
        $this->assertEquals(IsIdent(null), -1);
    }
}