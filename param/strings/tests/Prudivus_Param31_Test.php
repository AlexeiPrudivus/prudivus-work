<?php
/*
Write a string function FillStr(S, N) that returns a string of length N containing repeating copies of the template string S
(the last copy of S may be contained partially in the resulting string).
 */
require_once("../Prudivus_Param31.php");
class FillStrTest extends PHPUnit_Framework_TestCase {
//  N > S
    public function testDigitNTest1() {
        $this->assertEquals(FillStr('text4_', 20), 'text4_text4_text4_te');
        $this->assertEquals(FillStr('te_x9t', 10), 'te_x9tte_x');
        $this->assertEquals(FillStr('_t3xt ', 11), '_t3xt _t3xt');
    }
//  N < S
    public function testDigitNTest2() {
        $this->assertEquals(FillStr('6text4_', 5), '6text');
        $this->assertEquals(FillStr('7te_x9t', 2), '7t');
        $this->assertEquals(FillStr('8_t3xt_', 1), '8');
    }
}