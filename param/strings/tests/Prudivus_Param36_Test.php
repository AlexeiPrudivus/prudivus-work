<?php
/*
Write a string function InvStr(S, K, N) that returns an inverted substring of a string S.
The substring contains N characters of S (starting at a character with the order number K) in inverse order.
If K is greater than the length of S then the function returns an empty string;
if the length of S is less than K + N then all characters of S starting at its K-th character must be inverted.
 */
require_once("../Prudivus_Param36.php");
class InvStrTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $this->assertEquals(InvStr('SOME LATIN TEXT', 50, 500), null);
        $this->assertEquals(InvStr('SOME LATIN TEXT', 1, 4), 'EMOS');
        $this->assertEquals(InvStr('SOME LATIN TEXT', 6, 5), 'NITAL');
        $this->assertEquals(InvStr('SOME LATIN TEXT', 3, 5), 'AL EM');
        $this->assertEquals(InvStr('SOME LATIN TEXT', -15, 5), ' EMOS');
    }
}