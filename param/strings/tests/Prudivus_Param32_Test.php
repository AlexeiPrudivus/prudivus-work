<?php
/*
Write a procedure UpCaseLat(S) that converts all Latin small letters of a string S to uppercase
(others characters of S must remain unchanged).
A string S is an input and output parameter.
 */
require_once("../Prudivus_Param32.php");
class UpCaseLatTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $S = 'some latin text';
        UpCaseLat($S);
        $this->assertEquals($S, 'SOME LATIN TEXT');

        $S = 'text9wi0th8913numbers';
        UpCaseLat($S);
        $this->assertEquals($S, 'TEXT9WI0TH8913NUMBERS');
    }
//  is not latin
    public function testDigitNTest2() {
        $S = '64_';
        UpCaseLat($S);
        $this->assertEquals($S, '64_');

        $S = '709878';
        UpCaseLat($S);
        $this->assertEquals($S, '709878');

        $S = 'gh русский you';
        UpCaseLat($S);
        $this->assertEquals($S, 'GH русский YOU');
    }
}