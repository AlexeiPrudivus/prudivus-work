<?php
/*
Write a string function WordK(S, K) that returns K-th word of a string S
(a word is defined as a character sequence that does not contain blank characters and is bounded by blank characters or the string beginning/end).
If the amount of words in the string S is less than K then the function returns an empty string.
 */
require_once("../Prudivus_Param40.php");
class WordKTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $this->assertEquals(WordK(" several words ", 2), "words");
        $this->assertEquals(WordK("Write a string function WordK(S, K) that returns K-th word of a string S", 3), "string");
        $this->assertEquals(WordK("If the amount of words in the string S is less than K then the function returns an empty string.", 15), "the");
    }
}