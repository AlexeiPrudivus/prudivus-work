<?php
/*
Write a string function DecToBin(N) that returns a string containing the binary representation of a nonnegative integer N.
The string consists of characters "0", "1" and does not contain leading zeros (except for the representation of zero number).
 */
require_once("../Prudivus_Param44.php");
class DecToBinTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $this->assertEquals(DecToBin(17), 10001);
        $this->assertEquals(DecToBin(129), 10000001);
        $this->assertEquals(DecToBin(256), 100000000);
    }
}