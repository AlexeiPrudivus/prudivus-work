<?php
/*
Write an integer function PosK(S0, S, K) that searches for K-th occurrence (K > 0) of a string S0 within a string S
and returns the order number of the first character of this occurrence.
The string S is assumed to contain no overlapping occurrences of the substring S0.
If the string S does not contain occurrences of S0 then the function returns 0.
 */
require_once("../Prudivus_Param39.php");
class PosKTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $this->assertEquals(PosK("te", "test text", 2), 6);
        $this->assertEquals(PosK("t", "test text",1 ), 1);
        $this->assertEquals(PosK("t", "test text",2 ), 4);
        $this->assertEquals(PosK("t", "test text",3 ), 6);
        $this->assertEquals(PosK("t", "test text",4 ), 9);
        $this->assertEquals(PosK('E', 'SOME LATIN TEXT', 1), 4);
        $this->assertEquals(PosK(' LATIN ', 'LATIN LATIN LATIN', 500), 0);
    }
}