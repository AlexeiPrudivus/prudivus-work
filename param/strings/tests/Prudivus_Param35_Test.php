<?php
/*
Write a procedure TrimRightC(S, C) that removes all trailing characters equal a character C from a string S.
A string S is an input and output parameter.
 */
require_once("../Prudivus_Param35.php");
class TrimRightCTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $S = 'SOME LATIN TEXTTTTTTTTTTTTTTTTTTTTTTTTTTT';
        TrimRightC($S,'T');
        $this->assertEquals($S, 'SOME LATIN TEX');

        $S = 'letter ZZZzzzZZZZzzzZZZZ';
        TrimRightC($S, 'Z');
        $this->assertEquals($S, 'letter ZZZzzzZZZZzzz');
    }
}