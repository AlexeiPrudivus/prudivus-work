<?php
/*
Write an integer function PosLast(S0, S) that searches for the last occurrence of a string S0 within a string S
and returns the order number of the first character of this occurrence.
If the string S does not contain occurrences of S0 then the function returns 0.
 */
require_once("../Prudivus_Param38.php");
class PosLastTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $this->assertEquals(PosLast("te", "test text"), 6);
        $this->assertEquals(PosLast("t", "test text"), 9);
        $this->assertEquals(PosLast('E', 'SOME LATIN TEXT'), 13);
        $this->assertEquals(PosLast(' LATIN ', 'LATIN LATIN LATIN'), 6);
    }
}