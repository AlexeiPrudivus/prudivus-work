<?php
/*
Write a string function DecToHex(N) that returns a string containing the hexadecimal representation of a nonnegative integer N.
The string consists of characters "0"–"9", "A"–"F" and does not contain leading zeros (except for the representation of zero number).
 */
require_once("../Prudivus_Param45.php");
class DecToHexTest extends PHPUnit_Framework_TestCase {
//
    public function testDigitNTest1() {
        $this->assertEquals(DecToHex(32767), '7FFF');
        $this->assertEquals(DecToHex(15), 'F');
        $this->assertEquals(DecToHex(16), '10');
    }
}