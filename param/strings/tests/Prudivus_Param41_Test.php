<?php
/*
Write a procedure SplitStr(S, W, N) that creates an array W of all words being contained in a string S.
The array W of strings and its size N are output parameters.
A word is defined as a character sequence that does not contain blank characters and is bounded by blank characters or the string beginning/end;
the string S is assumed to contain no more than 10 words.
 */
require_once("../Prudivus_Param41.php");
class SplitStrTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $S = "            s                word1 j gvyt tyrf 756r 75r ,,, 7";
        SplitStr($S, $W, $N);
        $this->assertEquals($W, array(1 => 's','word1', 'j', 'gvyt', 'tyrf', '756r', '75r', ',,,', '7'));
        $this->assertEquals($N, 9);

        $S = " text text2 text3            ";
        SplitStr($S, $W, $N);
        $this->assertEquals($W, array(1 => 'text','text2','text3'));
        $this->assertEquals($N, 3);
    }
}