<?php
/*
Write a string function CompressStr(S) that compresses a string S and returns the compressed string.
The string compression must be carried out as follows: each substring consisting of 4 or more equal characters C is replaced by the string "C{K}",
where K is the amount of characters C (the string being compressed is assumed to contain no braces "{}").
For example, the string "bbbccccce" must be compressed to "bbbc{5}e".
 */
require_once("../Prudivus_Param42.php");
class CompressStrTest extends PHPUnit_Framework_TestCase {
//  is latin
    public function testDigitNTest1() {
        $S = "            s                         wwwww wwwwwwwwwwword1 jjj gvyyyyyyyyyyt tttyrf 7556r 75r ,,, 77777777777";
        $this->assertEquals(CompressStr($S), ' {11}s {24}wwwww w{10}ord1 jjj gvy{9}t tttyrf 7556r 75r ,,, 7{10}');

        $S = "55111111111111111111111111111111111 55558888                              ";
        $this->assertEquals(CompressStr($S), '551{32} 55558888 {29}');
    }
}