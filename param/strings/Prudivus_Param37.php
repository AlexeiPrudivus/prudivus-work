<?php
/*
Write an integer function PosSub(S0, S, K, N) that searches for the first occurrence of a string S0 within a substring of a string S
(the substring contains N characters of S starting at a character with the order number K).
The function returns the order number of the first character of this occurrence within S.
If K is greater than the length of S then the function returns 0;
if the length of S is less than K + N then all characters of S starting at its K-th character must be analyzed.
If the required substring of S does not contain occurrences of S0 then the function returns 0.
Output return values of this function for given strings S0, S and each of three pairs of positive integers (K1, N1), (K2, N2), (K3, N3).
*/
function PosSub($S0, $S, $K, $N) {
    $S1 = "";
    $A = " ";
    $u = 1;
    $d = 0;
    $K--;

    if ($K >= strlen($S)) {
        return 0;
    }
    if ($K < 0) {
        $K = 0;
    }
//------------------------------------------------------------ вырезаем из большого массива нужный нам кусок
    if (($K + $N) >= strlen($S)) {
        for ($i = $K; $i < strlen($S); $i++) {
            $A[$d] = $S[$i];
            $d++;
        }
    } else {
        for ($i = $K; $i < $K + $N; $i++) {
            $A[$d] = $S[$i];
            $d++;
        }
    }
//------------------------------------------------------------ ищем в вырезанном куске совпадения
    for ($j = 0; $j <= strlen($A) - strlen($S0); $j++) {
        for ($i = $u - 1; $i < $u + strlen($S0) - 1; $i++) {
            $S1 = $S1 . $A[$i];
            if ($S1 == $S0) {
                return $u + $K;
            }
        }
        $S1 = "";
        $u++;
    }

    return 0;
}
$S0 = "te";
$S = "test text";
$K = 1;
$N = 500;

echo PosSub($S0, $S, $K, $N);
?>