<?php
/*
Write an integer function PosLast(S0, S) that searches for the last occurrence of a string S0 within a string S
and returns the order number of the first character of this occurrence.
If the string S does not contain occurrences of S0 then the function returns 0.
Output return values of this function for five given pairs of strings (S0, S).
*/
function PosLast($S0, $S) {
    $S1 = "";
    $u = 1;
    $r = 0;

    for ($j = 0; $j <= strlen($S) - strlen($S0); $j++) {
        for ($i = $u - 1; $i < $u + strlen($S0) - 1; $i++) {
            $S1 = $S1 . $S[$i];
            if ($S1 == $S0) {
                $r = $u;
            }
        }
        $S1 = "";
        $u++;
    }

    return $r;
}
$S0 = "t";
$S = "test text";


echo PosLast($S0, $S);
?>