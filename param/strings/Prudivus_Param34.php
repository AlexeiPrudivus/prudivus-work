<?php
/*
Write a procedure TrimLeftC(S, C) that removes all leading characters equal a character C from a string S.
A string S is an input and output parameter.
Having input a character C and five strings and using this procedure, process the given strings.
*/
function TrimLeftC(&$S, $C) {

    $S= ltrim ($S, $C);
    echo $S;
}
/*
$S = "TTTT русский f";
$C = 'T';

TrimLeftC($S, $C);*/
?>