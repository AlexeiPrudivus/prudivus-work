<?php
/*
Write a string function CompressStr(S) that compresses a string S and returns the compressed string.
The string compression must be carried out as follows: each substring consisting of 4 or more equal characters C is replaced by the string "C{K}",
where K is the amount of characters C (the string being compressed is assumed to contain no braces "{}").
For example, the string "bbbccccce" must be compressed to "bbbc{5}e".
Using this function, compress five given strings.
*/
function CompressStr($S) {
    $A = array();
    $S2 = '';
    $u = 0;

    for ($i = 0; $i <= strlen($S) - 1; $i++) {
        if ($i == strlen($S) - 1) {
            if ($u > 5) {
                $S2 = $S2 . $S[$i] . '{' . $u . '}';
                $u = 0;
            } else {
                for ($j = 0; $j <= $u; $j++) {
                    $S2 = $S2 . $S[$i];
                }
                $u = 0;
            }
        } else {
            if ($S[$i] == $S[$i + 1]) {
                $u++;
            } else if ($u > 5) {
                $S2 = $S2 . $S[$i] . '{' . $u . '}';
                $u = 0;
            } else {
                for ($j = 0; $j <= $u; $j++) {
                    $S2 = $S2 . $S[$i];
                }
                $u = 0;
            }
        }
    }

    return $S2;
}
/*
$S = "111111111111111111111111111111111 55558888                              ";

echo CompressStr($S);*/
?>