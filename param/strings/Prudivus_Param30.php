<?php
/*
Write an integer function IsIdent(S) that indicates whether a string S is a valid identifier,
that is, a nonempty string that does not begin with a digit and contains Latin letters, digits, and a character "_" only.
If S is a valid identifier then the function returns 0.
If S is an empty string or begins with a digit then the function returns −1 or −2 respectively.
If S contains invalid characters then the function returns the order number of the first invalid character.
Using this function, check five given strings.
*/

function IsIdent($S) {
    if ($S == null) {
        return -1;
    }

    if (is_numeric($S[0])) {
        return -2;
    }

    for($i = 0; $i < strlen($S); $i++) {
        if(preg_match('/[^a-zA-Z0-9_]/', $S[$i])) {
            return $S[$i];
        }
    }
    return 0;
}
/*
$S = 'la_t9in';

IsIdent($S);*/
?>