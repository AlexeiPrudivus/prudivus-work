/*
 Using the Leng and Area functions from the tasks Func56 and Func58, write a real-valued function Dist(xP, yP, xA, yA, xB, yB)
 that returns the distance D(P, AB) between a point P and a line AB:
 D(P, AB) = 2·SPAB/|AB|,
 where SPAB is the area of the triangle PAB. Using this function,
 find the distance between a point P and each of lines AB, AC, BC provided that coordinates of points P, A, B, C are given.
 */
function Leng(xA, yA, xB, yB) {
    return Math.abs(Math.pow((Math.pow((xA - xB),2) + Math.pow((yA - yB),2)), 1/2)).toFixed(2);
}

function Perim(xA, yA, xB, yB, xC, yC) {
    return ((+Leng(xA, yA, xB, yB)) + (+Leng(xA, yA, xC, yC)) + (+Leng(xB, yB, xC, yC))).toFixed(2);
}

function Area(xA, yA, xB, yB, xC, yC) {
    var p = (Perim(xA, yA, xB, yB, xC, yC)/2).toFixed(2);
    return (Math.pow((p * (p - (+Leng(xA, yA, xB, yB))) * (p - (+Leng(xA, yA, xC, yC))) * (p - (+Leng(xB, yB, xC, yC)))), 1/2)).toFixed(2);
}

function Dist(xC, yC, xA, yA, xB, yB) {
    return (2 * Area(xC, yC, xA, yA, xB, yB) / Math.abs(Leng(xA, yA, xB, yB))).toFixed(2);
}

var xA = Math.floor((Math.random() * 20) - 10);
var yA = Math.floor((Math.random() * 20) - 10);
var xB = Math.floor((Math.random() * 20) - 10);
var yB = Math.floor((Math.random() * 20) - 10);
var xC = Math.floor((Math.random() * 20) - 10);
var yC = Math.floor((Math.random() * 20) - 10);

document.write(xC +' '+ yC +' '+ xA +' '+ yA +' '+ xB +' '+ yB +' '+ '<br>');
document.write(Dist(xC, yC, xA, yA, xB, yB));