/*
 Write a real-valued function Leng(xA, yA, xB, yB) that returns the length of a segment AB with given coordinates of its endpoints:
 |AB| = ((xA − xB)2 + (yA − yB)2)1/2
 (xA, yA, xB, yB are real-valued parameters).
 Using this function, find the lengths of segments AB, AC, AD provided that coordinates of points A, B, C, D are given.
 */
function Leng(xA, yA, xB, yB) {
    return Math.abs(Math.pow((Math.pow((xA - xB),2) + Math.pow((yA - yB),2)), 1/2)).toFixed(2);
}

var xA = Math.floor((Math.random() * 20) - 10);
var yA = Math.floor((Math.random() * 20) - 10);
var xB = Math.floor((Math.random() * 20) - 10);
var yB = Math.floor((Math.random() * 20) - 10);

document.write(xA +' '+ yA +' '+ xB +' '+ yB +' '+ '<br>');
document.write(Leng(xA, yA, xB, yB));