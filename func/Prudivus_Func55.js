/*
 Using the MonthDays function from the task Func53, write a function NextDate(D, M, Y) that changes a correct date,
 represented at the "day D, month number M, year Y" format, to a next one and returns new values of day, month, and year
 (all numbers are integers). Apply this function to three given dates and output resulting next ones.
 */
function IsLeapYear(Y) {
    if ((Y % 100 == 0) && (Y % 400 != 0)) {
        return false;
    } else
        return (Y % 4 == 0);
}

function MonthDays(M, Y) {
    switch (M) {

        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12: {
            M = 31;
            break;
        }

        case 4:
        case 6:
        case 9:
        case 11: {
            M = 30;
            break;
        }

        case 2: {
            if (IsLeapYear(Y) == true) {
                M = 29;
                break;
            } else {
                M = 28;
                break;
            }
        }
        default : {
            M = 'error';
            break;
        }
    }
    return M;
}

function NextDate(D, M, Y) {

    if (D == MonthDays(M,Y) && M == 12) {
        D = 1;
        M = 1;
        Y += 1;
    } else if (D == MonthDays(M,Y)) {
        M += 1;
        D = 1;
    } else {
        D = D + 1;
    }

    return [D, M, Y];
}

var D = Math.floor((Math.random() * 28) + 1);
var M = Math.floor((Math.random() * 12) + 1);
var Y = Math.floor((Math.random() * 1999) + 1000);

document.write(D +' '+ M +' '+ Y +' '+ '<br>');
document.write(NextDate(D, M, Y));