/*
 Write a real-valued function Fact(N) that returns a factorial of a positive integer N: N! = 1·2·…·N
 (the real return type allows to avoid the integer overflow during the calculation of the factorials for large values of the parameter N).
 Using this function, find the factorials of five given integers.
 */
function Fact(N) {
    var f = 1;
    for (var i = 1; i <= N; i++) {
        f *= i;
    }
    return f;
}

var N = Math.floor((Math.random() * 10) + 1);

document.write(N +'<br>');
document.write(Fact(N));