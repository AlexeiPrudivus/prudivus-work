/*
 Taking into account that the least common multiple of two positive integers A and B equals A·(B/GCD(A, B)),
 where GCD(A, B) is the greatest common divisor of A and B, and using the GCD2 function from the task Func46,
 write an integer function LCM2(A, B) that returns the least common multiple of A and B.
 Using this function, find the least common multiple for each of pairs (A, B), (A, C), (A, D) provided that integers A, B, C, D are given.
 */
function GCD2(A, B) {

    while (A != 0 && B != 0) {
        if (A > B) {
            A = A % B;
            if (A == 0) {
                return B;
            }
        } else {
            B = B % A;
            if (B == 0) {
                return A;
            }
        }
    }
}

function LCM2(A, B) {
    return A * (B / GCD2(A, B));
}

var A = Math.floor((Math.random() * 100) + 1);
var B = Math.floor((Math.random() * 100) + 1);

document.write(A +' '+ B +' '+ '<br>');
document.write(LCM2(A, B));