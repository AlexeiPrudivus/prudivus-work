/*
 Write a function AddRightDigit(D, K) that adds a digit D to the right side of the decimal representation of a positive integer K
 and returns the obtained number (D and K are input integer parameters, the value of D is in the range 0 to 9).
 Having input an integer K and two one-digit numbers D1, D2 and using two calls of this function,
 sequentially add the given digits D1, D2 to the right side of the given K and output the result of each adding.
 */
function AddRightDigit(D, K) {
    return K * 10 + D;
}

var K = Math.floor((Math.random() * 9999999) + 1);
var D = Math.floor((Math.random() * 9) + 1);

document.write(K +' '+ D +' '+ '<br>');
document.write(AddRightDigit(D, K));