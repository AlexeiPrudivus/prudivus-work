/*
 Write a function ShiftLeft3(X) that performs a left cyclic shift of a list X of three real-valued items:
 the value ot each item should be assigned to the previous item and the value of the first item should be assigned to the last item
 (the function returns the None value). Using this function, perform the left cyclic shift for each of two given lists X and Y.
 */
function ShiftLeft3(X) {
    var Z;

    Z = X[0];
    X[0] = X[1];
    X[1] = X[2];
    X[2] = Z;
}

var X = [Math.floor((Math.random() * 10) + 1),Math.floor((Math.random() * 10) + 1),Math.floor((Math.random() * 10) + 1)];

document.write(X +' '+ '<br>');
ShiftLeft3(X);
document.write(X +' '+ '<br>');