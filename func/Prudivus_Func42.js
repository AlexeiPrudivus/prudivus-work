/*
 Write a real-valued function Cos1(x, ε) (x and ε are real numbers, ε > 0)
 that returns an approximate value of the function cos(x) defined as follows:
 cos(x) = 1 − x2/(2!) + x4/(4!) − … + (−1)n·x2·n/((2·n)!) + … .
 Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function cos(x) at a given point x for six given ε.
 */
function Cos1(x, e) {
    var n = 1.0;
    var sum = 0.0;
    var i = 1;

    do
    {
        sum += (+n);
        n *= -1 * x * x / ((2 * i - 1) * (2 * i));
        i++;
    }
    while (Math.abs(n) > e);

    return sum.toFixed(7);
}

var x = 3.3383540;
var e = 0.0287580;

document.write(x +' '+ e +' '+'<br>');
document.write(Cos1(x, e));