/*
 Using the Power1 and Power2 functions (see Func37 and Func38), write a real-valued function Power3(A, B)
 that returns the power AB calculated as follows (A and B are real-valued parameters):
 if B has a zero-valued fractional part then the function Power2(A, N) is called (an integer variable N is equal to B),
 otherwise the function Power1(A, B) is called.
 Having input real numbers P, A, B, C and using the Power3 function, find the powers AP, BP, CP.
 */
function Power1(A, B) {
    if (A <= 0) {
        return 0;
    } else {
        return (Math.exp(B*Math.log(A))).toFixed(2);
    }
}

function Power2(A, N) {
    if (N == 0) {
        return 1;
    }

    var AN = 1;

    if (A <= 0) {
        return 0;
    } else {
        for (var i=1; i <= N; i++) {
            AN *= A;
        }
        return AN;
    }
}

function Power3(A, B) {
    if (B % 1 === 0) {
        return Power2(A, B);
    } else {
        return Power1(A, B);
    }
}

var A = Math.floor((Math.random() * 10) + 1);
var B = Math.floor((Math.random() * 10) + 1);

document.write(A +'^'+ B +' '+'<br>');
document.write(Power3(A, B));