/*
 Write a logical function IsPrime(N) that returns True, if an integer parameter N (> 1) is a prime number,
 and False otherwise. Using this function, find the amount of prime numbers in a given sequence of 10 integers greater than 1.
 Note that an integer (> 1) is called a prime number if it has not positive divisors except 1 and itself.
 */
function IsPrime(N) {
    if (N == 2 || N == 3 || N == 5 || N == 7) {
        return true;
    }

    if(N % 2 == 0) {
        return false;
    }

    for (var i = 3; i < N; i = i + 2) {
        if (N % i == 0) {
            return false;
        }
    }

    return true;
}
var N = Math.floor((Math.random() * 100) + 1);

document.write(N + '<br>');
document.write(IsPrime(N));