/*
 Write a logical function IsPalindrome(K) that returns True, if the decimal representation of a positive parameter K is a
 palindrome (i. e., it is read equally both from left to right and from right to left), and False otherwise.
 Using this function, find the amount of palindromes in a given sequence of 10 positive integers.
 */
function IsPalindrome(K) {
    var R1 = K;
    var R = 0;

    while(K != 0){
        R = R*10 + K%10;
        K = Math.floor(K / 10);
    }

    return (R == R1);
}
var K = Math.floor((Math.random() * 9999) + 1001);

document.write(K +'<br>');
document.write(IsPalindrome(K));