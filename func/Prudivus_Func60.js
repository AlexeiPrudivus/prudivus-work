/*
 Using the Dist function from the task Func59, write a function Alts(xA, yA, xB, yB, xC, yC)
 that evaluates and returns the altitudes hA, hB, hC drawn from the vertices A, B, C of a triangle ABC
 (the coordinates of vertices are input real-valued parameters).
 Using this function, evaluate the altitudes of each of triangles ABC, ABD, ACD provided that the coordinates of points A, B, C, D are given.
 */
function Leng(xA, yA, xB, yB) {
    return Math.abs(Math.pow((Math.pow((xA - xB),2) + Math.pow((yA - yB),2)), 1/2)).toFixed(2);
}

function Perim(xA, yA, xB, yB, xC, yC) {
    return ((+Leng(xA, yA, xB, yB)) + (+Leng(xA, yA, xC, yC)) + (+Leng(xB, yB, xC, yC))).toFixed(2);
}

function Area(xA, yA, xB, yB, xC, yC) {
    var p = (Perim(xA, yA, xB, yB, xC, yC)/2).toFixed(2);
    return (Math.pow((p * (p - (+Leng(xA, yA, xB, yB))) * (p - (+Leng(xA, yA, xC, yC))) * (p - (+Leng(xB, yB, xC, yC)))), 1/2)).toFixed(2);
}

function Dist(xC, yC, xA, yA, xB, yB) {
    return (2 * Area(xC, yC, xA, yA, xB, yB) / Math.abs(Leng(xA, yA, xB, yB))).toFixed(2);
}

function Alts(xA, yA, xB, yB, xC, yC) {
    var hA = Dist(xA, yA, xB, yB, xC, yC);
    var hB = Dist(xB, yB, xC, yC, xA, yA);
    var hC = Dist(xC, yC, xA, yA, xB, yB);

    return [hA, hB, hC];
}

var xA = Math.floor((Math.random() * 20) - 10);
var yA = Math.floor((Math.random() * 20) - 10);
var xB = Math.floor((Math.random() * 20) - 10);
var yB = Math.floor((Math.random() * 20) - 10);
var xC = Math.floor((Math.random() * 20) - 10);
var yC = Math.floor((Math.random() * 20) - 10);

document.write(xA +' '+ yA +' '+ xB +' '+ yB +' '+ xC +' '+ yC +' '+ '<br>');
document.write(Alts(xA, yA, xB, yB, xC, yC));