/*
 Write a real-valued function TriangleP(a, h) that returns the perimeter of an isosceles triangle
 with given base a and altitude h (a and h are real numbers).
 Using this function, find the perimeters of three triangles with given bases and altitudes.
 Note that the leg b of an isosceles triangle can be found by the Pythagorean theorem:
 b2 = (a/2)2 + h2.
 */
function TriangleP(a, h) {
    return Math.pow((Math.pow((a/2),2) + Math.pow(h,2)), 1/2).toFixed(2);
}
var a = Math.floor((Math.random() * 10) + 1);
var h = Math.floor((Math.random() * 10) + 1);

document.write(a +' '+ h + '<br>');
document.write(TriangleP(a, h));