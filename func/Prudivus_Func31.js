/*
 Write a function Swap(X, I, J) that exchanges the values of items XI and XJ of a list X of real numbers
 (I and J are input integer parameters, the function returns the None value).
 Having input a list of four real numbers and using three calls of this function,
 sequentially exchange the values of the two first, two last, and two middle items of the given list.
 Output the new values of the list.
 */
function Swap(X, I, J) {
    var Z = X[I - 1];
    X[I - 1] = X[J - 1];
    X[J - 1] = Z;
}

var X = [1,2,3,4,5,6,7];
var I = 1;
var J = 3;

document.write(X +' '+ '<br>');
Swap(X, I, J);
document.write(X +' '+ '<br>');