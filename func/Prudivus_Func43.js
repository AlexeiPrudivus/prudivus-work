/*
 Write a real-valued function Ln1(x, ε) (x and ε are real numbers, |x| < 1, ε > 0)
 that returns an approximate value of the function ln(1 + x) defined as follows:
 ln(1 + x) = x − x2/2 + x3/3 − … + (−1)n·xn+1/(n+1) + … .
 Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function ln(1 + x) at a given point x for six given ε.
 */
function Ln1(x, e) {
    var Ln = [];

    var S = 0;
    var stop = 2;
    var f = 1;

    for (var i = 0; i < stop; i++) {

        Ln[i] = Math.pow(-1,i) * Math.pow(x, i+1) / (i+1); //function

        if (Math.abs(Ln[i]) > e) { //stop condition
            stop++;
            //echo $Ln[$i]." ";
            S = (+S) + (+Ln[i]); //sum
        }
    }
    //echo "\n".$S;
    return S.toFixed(7);
}

var x = 0.8544101;
var e = 0.0656913;

document.write(x +' '+ e +' '+'<br>');
document.write(Ln1(x, e));