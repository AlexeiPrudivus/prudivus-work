/*
 Write a real-valued function Power1(A, B) that returns the power AB calculated by the formula AB = exp(B·ln(A))
 (A and B are real-valued parameters).
 In the case of zero-valued or negative parameter A the function returns 0.
 Having input real numbers P, A, B, C and using this function, find the powers AP, BP, CP.
 */
function Power1(A, B) {
    if (A <= 0) {
        return 0;
    } else {
        return (Math.exp(B*Math.log(A))).toFixed(2);
    }
}

var A = Math.floor((Math.random() * 10) + 1);
var B = Math.floor((Math.random() * 10) + 1);

document.write(A +'^'+ B +' '+'<br>');
document.write(Power1(A, B));