/*
 Write a function SortInc3(X) that sorts the list X of three real-valued items in ascending order (the function returns the None value).
 Using this function, sort each of two given lists X and Y.
 */
function SortInc3(X) {
    var Z;
    for (var i = 0; i < 4; i++) {
        if (X[0] > X[1]) {
            Z = X[0];
            X[0] = X[1];
            X[1] = Z;
        }
        if (X[1] > X[2]) {
            Z = X[1];
            X[1] = X[2];
            X[2] = Z;
        }
    }
}

var X = [Math.floor((Math.random() * 10) + 1),Math.floor((Math.random() * 10) + 1),Math.floor((Math.random() * 10) + 1)];

document.write(X +' '+ '<br>');
SortInc3(X);
document.write(X +' '+ '<br>');