/*
 Write an integer function Sign(X) that returns the following value:
 −1,    if X < 0;        0,    if X = 0;        1,    if X > 0
 (X is a real-valued parameter). Using this function, evaluate an expression Sign(A) + Sign(B) for given real numbers A and B.
 */
var X = Math.floor((Math.random() * 20) - 10);
document.write(X);

function Sign(X) {
    if (X < 0) {
        return -1;
    }
    else if (X > 0) {
        return 1;
    } else {
        return 0;
    }
}

document.write("\n");
document.write(Sign(X));