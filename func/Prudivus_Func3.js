/*
 Write a real-valued function CircleS(R) that returns the area of a circle of radius R (R is a real number).
 Using this function, find the areas of three circles of given radiuses.
 Note that the area of a circle of radius R can be found by formula S = π·R2.
 Use 3.14 for a value of π.
 */
function CircleS(R) {
    return 3.14*R*R;
}
var R = Math.floor((Math.random() * 10) + 1);


document.write(R + '<br>');
document.write(CircleS(R));