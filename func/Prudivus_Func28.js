/*
 Write a function InvDigits(K) that inverts the order of digits of a positive integer K
 and returns the obtained integer (K is an input parameter).
 Using this function, invert the order of digits for each of five given integers.
 */
function InvDigits(K) {
    var TK = K;
    var C = 0;
    var S = 0;
    var u1 = 0;

    while (K != 0) {
        C++;
        K = Math.floor(K / 10);
    }

    var u2 = C-1;

    for (var i = 1; i <= C; i++)
    {
        T = Math.floor(TK % Math.pow(10, i) / Math.pow(10, u1));
        S += T * Math.pow(10, u2);

        u1++;
        u2--;
    }

    K = S;
    return K;
}

var K = Math.floor((Math.random() * 9999999) + 1);

document.write(K +' '+ '<br>');
document.write(InvDigits(K));