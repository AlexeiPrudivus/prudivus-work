/*
 Write an integer function Quarter(x, y) that returns the number of a coordinate quarter containing a point with nonzero real-valued coordinates (x, y).
 Using this function, find the numbers of coordinate quarters containing each of three points with given nonzero coordinates.
 */
function Quarter(x, y) {
    if (x > 0 && y > 0) {
        return 1;
    } else if (x < 0 && y > 0) {
        return 2;
    } else if (x < 0 && y < 0) {
        return 3
    } else {
        return 4;
    }
}
var x = Math.floor((Math.random() * 20) - 10);
var y = Math.floor((Math.random() * 20) - 10);

document.write(x +' '+ y + '<br>');
document.write(Quarter(x, y));