/*
 Write a real-valued function RingS(R1, R2) that returns the area of a ring bounded by a concentric circles of radiuses R1 and R2
 (R1 and R2 are real numbers, R1 > R2). Using this function, find the areas of three rings with given outer and inner radiuses.
 Note that the area of a circle of radius R can be found by formula S = π·R2. Use 3.14 for a value of π.
 */
function RingS(R1, R2) {
    if(R1 > R2) {
        return (3.14*R1*R1 - 3.14*R2*R2).toFixed(2);
    } else {
        return (3.14*R2*R2 - 3.14*R1*R1).toFixed(2);
    }
}
var R1 = Math.floor((Math.random() * 10) + 1);
var R2 = Math.floor((Math.random() * 10) + 1);

document.write(R1 +' '+ R2 + '<br>');
document.write(RingS(R1, R2));