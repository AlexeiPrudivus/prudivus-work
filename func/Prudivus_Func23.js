/*
 Write a function PowerA234(A) that computes the second, the third, and the fourth degrees of a real number A
 and returns these degrees as three real numbers (A is an input parameter).
 Using this function, find the second, the third, and the fourth degrees of five given real numbers.
 */
function PowerA234(A) {
    return [A*A, A*A*A, A*A*A*A];
}

var A = Math.floor((Math.random() * 10) + 1);

document.write(A +'<br>');
document.write(PowerA234(A));