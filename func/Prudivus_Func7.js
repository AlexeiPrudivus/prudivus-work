function Calc(A, B, Op) {
    switch (Op) {
        case 1: {
            return A - B;
        }
        case 2: {
            return A * B;
        }
        case 3: {
            return (A / B).toFixed(2);
        }
        default: {
            return A + B;
        }
    }
}
var A = Math.floor((Math.random() * 10) + 1);
var B = Math.floor((Math.random() * 10) + 1);
var Op = Math.floor((Math.random() * 4) + 1);

document.write(A +' '+ B +' '+ Op + '<br>');
document.write(Calc(A, B, Op));