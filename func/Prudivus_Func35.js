/*
 Write a function ShiftRight3(X) that performs a right cyclic shift of a list X of three real-valued items:
 the value ot each item should be assigned to the next item and the value of the last item should be assigned to the first item
 (the function returns the None value). Using this function, perform the right cyclic shift for each of two given lists X and Y.
 */
function ShiftRight3(X) {
    var Z;

    Z = X[2];
    X[2] = X[1];
    X[1] = X[0];
    X[0] = Z;
}

var X = [Math.floor((Math.random() * 10) + 1),Math.floor((Math.random() * 10) + 1),Math.floor((Math.random() * 10) + 1)];

document.write(X +' '+ '<br>');
ShiftRight3(X);
document.write(X +' '+ '<br>');