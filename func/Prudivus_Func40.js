/*
 Write a real-valued function Exp1(x, ε) (x and ε are real numbers, ε > 0)
 that returns an approximate value of the function exp(x) defined as follows:
 exp(x) = 1 + x + x2/(2!) + x3/(3!) + … + xn/(n!) + …
 (n! = 1·2·…·n). Stop adding new terms to the sum when the value of the next term will be less than ε.
 Using this function, find the approximate values of the function exp(x) at a given point x for six given ε.
 */
function Exp1(x, e) {
    var Exp = [];

    var S = 0;
    var stop = 1;
    var f = 1;

    for (var i = 0; i < stop; i++) {

        if (i == 0) { //factorial
            f = 1;
        } else {
            f *= i;
        }

        Exp[i] = (Math.pow(x, i) / f); //function

        if (Math.abs(Exp[i]) > e) { //stop condition
            stop++;
            //echo $Exp[$i]." ";
            S = (+S) + (+Exp[i]); //sum
        }
    }

    //echo "\n".$S;
    return S;
}

var x = 0.1034538;
var e = 0.0000206;

document.write(x +' '+ e +' '+'<br>');
document.write(Exp1(x, e));