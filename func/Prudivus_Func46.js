/*
 Write an integer function GCD2(A, B) that returns the greatest common divisor (GCD) of two positive integers A and B. Use the Euclidean algorithm:
 GCD(A, B) = GCD(B, A mod B),    if B ≠ 0;        GCD(A, 0) = A,
 where "mod" denotes the operator of taking the remainder after integer division.
 Using this function, find the greatest common divisor for each of pairs (A, B), (A, C), (A, D) provided that integers A, B, C, D are given.
 */
function GCD2(A, B) {

    while (A != 0 && B != 0) {
        if (A > B) {
            A = A % B;
            if (A == 0) {
                return B;
            }
        } else {
            B = B % A;
            if (B == 0) {
                return A;
            }
        }
    }
}

var A = Math.floor((Math.random() * 100) + 1);
var B = Math.floor((Math.random() * 100) + 1);

document.write(A +' '+ B +' '+ '<br>');
document.write(GCD2(A, B));