/*
 Write an integer function Fib(N) that returns the value of N-th term of the sequence of the Fibonacci numbers.
 The Fibonacci numbers FK are defined as follows:
 F1 = 1,        F2 = 1,        FK = FK−2 + FK−1,    K = 3, 4, … .
 Using this function, find five Fibonacci numbers with given order numbers N1, N2, …, N5.
 */
function Fib(N) {
    if (N == 1) {
        return 1;
    }
    if (N == 2) {
        return 2
    }
    var f1 = 1;
    var f2 = 1;
    var f3 = 2;

    for (var i = 3; i <= N; i++) {
        f3 = f1 + f2;
        f1 = f2;
        f2 = f3;
    }
    return f3;
}

var N = Math.floor((Math.random() * 10) + 1);

document.write(N +'<br>');
document.write(Fib(N));