/*
 Write a logical function IsPower5(K) that returns True, if an positive integer parameter K is equal to 5 raised to some integer power,
 and False otherwise. Using this function, find the amount of powers of base 5 in a given sequence of 10 positive integers.
 */
function IsPower5(K) {
    if (K < 0) {
        return false;
    }

    return (Math.log(K) / Math.log(5) % 1 === 0);
}
//var K = Math.floor((Math.random() * 100) + 1);
var K = 25;

document.write(K + '<br>');
IsPower5(K);
document.write(IsPower5(K));