/*
 Write a function IncTime(H, M, S, T) that increases a time interval in hours H, minutes M, seconds S on T seconds
 and returns new values of hours, minutes, and seconds (all numbers are positive integers).
 Having input hours H, minutes M, seconds S (as integers) and an integer T and using the IncTime function,
 increase the given time interval on T seconds and output new values of H, M, S.
 */
function IncTime(H, M, S, T) {
    T += H*3600 + M*60 + S;

    H = Math.floor(T / 3600);
    M = Math.floor((T % 3600) / 60);
    S = Math.floor((T % 3600) % 60 % 60);

    return [H, M, S];
}

var H = Math.floor((Math.random() * 10) + 1);
var M = Math.floor((Math.random() * 59) + 1);
var S = Math.floor((Math.random() * 59) + 1);
var T = Math.floor((Math.random() * 10000) + 1);

document.write(H +' '+ M +' '+ S +' + '+ T +' '+ '<br>');
document.write(IncTime(H, M, S, T));