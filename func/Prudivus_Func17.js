/*
 Write a real-valued function DegToRad(D) that converts the angle value D in degrees into the one in radians (D is a real number, 0 ≤ D < 360).
 Note that 180° = π radians and use 3.14 for a value of π. Using this function, convert five given angles from degrees into radians.
 */
function DegToRad(D) {
    if (D < 0 || D > 360) {
        return false;
    } else {
        var R = (D / 180 * 3.14).toFixed(2);
        return R;
    }
}
var D = Math.floor((Math.random() * 360) + 1);

document.write(D +'<br>');
document.write(DegToRad(D));