/*
 Write a function PowerA3(A) that returns the third degree of a real number A (A is an input parameter).
 Using this function, find the third degree of five given real numbers.
 */
function PowerA3(A) {
    return A*A*A;
}

var A = Math.floor((Math.random() * 10) + 1);

document.write(A +'<br>');
document.write(PowerA3(A));